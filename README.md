### What is this repository for? ###

CAMBerVis - visualization software to support comparative analysis of multiple bacterial strains

Project website: http://bioputer.mimuw.edu.pl/ecamber/

Please don’t hesitate to contact us with any comments and suggestion or if you are interested in co-developing this software

### How do I get set up? ###

This software is written in Java, using Netbeans platform. 
User's manual is available at the project website.

### Contribution guidelines ###


### Who do I talk to? ###

m.wozniak@mimuw.edu.pl