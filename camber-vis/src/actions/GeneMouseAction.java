/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package actions;

import org.netbeans.api.visual.action.WidgetAction;
import org.netbeans.api.visual.widget.Widget;
import org.openide.awt.StatusDisplayer;
import structs.Multigene;
import ui.VisualizationManager;
import widgets.GeneWidget;

public class GeneMouseAction extends WidgetAction.Adapter {

    // private Multigene multigene;
    @Override
    public State mouseClicked(Widget widget, WidgetMouseEvent event) {
        if (widget instanceof GeneWidget) {
            GeneWidget geneWidget = (GeneWidget) widget;
            geneWidget.clicked();
            Multigene multigene = geneWidget.getMultigene();
            VisualizationManager.getDefault().multigeneClicked(multigene);
            StatusDisplayer.getDefault().setStatusText("gene id: " + multigene.getGeneID());
        }
        return super.mouseClicked(widget, event);
    }

    @Override
    public State mouseEntered(Widget widget, WidgetMouseEvent event) {
        return super.mouseEntered(widget, event);
    }

    @Override
    public State mouseReleased(Widget widget, WidgetMouseEvent event) {
        return super.mouseReleased(widget, event);
    }

    @Override
    public State mouseMoved(Widget widget, WidgetMouseEvent event) {
        return super.mouseMoved(widget, event);
    }
}
