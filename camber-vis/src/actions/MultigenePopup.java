/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package actions;

import javax.swing.event.PopupMenuEvent;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.event.PopupMenuListener;
import org.netbeans.api.visual.action.PopupMenuProvider;
import org.netbeans.api.visual.widget.Widget;
import structs.Multigene;
import ui.VisualizationManager;
import utils.Messenger;
import vis.ClusterVisTopComponent;
import widgets.GeneWidget;

public final class MultigenePopup implements PopupMenuProvider, ActionListener {

    private static final String ACTION_SHOW_CLUSTER = "show_cluster"; // NOI18N
    private static final String ACTION_NUC_SEQUENCE = "nuc_sequence"; // NOI18N
    private static final String ACTION_AA_SEQUENCE = "aa_sequence"; // NOI18N
    private static final String ACTION_BLASTN = "blastn"; // NOI18N
    private static final String ACTION_BLASTP = "blastp"; // NOI18N
    private static final String ACTION_BLAST_API = "blast_api"; // NOI18N
    private static final String ACTION_CLUSTAL_NUC = "clustal_nuc"; // NOI18N
    private static final String ACTION_CLUSTAL_AA = "clustal_aa"; // NOI18N
    private JPopupMenu menu;
    private Multigene multigene;
    private GeneWidget geneWidget;

    public MultigenePopup(GeneWidget geneWidget, final Multigene multigene) {
        this.multigene = multigene;
        this.geneWidget = geneWidget;
        menu = new JPopupMenu("Popup menu");
        menu.addPopupMenuListener(new PopupMenuListener() {

            @Override
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                VisualizationManager.getDefault().multigeneClicked(multigene);
            }

            @Override
            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
            }

            @Override
            public void popupMenuCanceled(PopupMenuEvent e) {
            }
        });
        JMenuItem item;

        item = new JMenuItem("Multigene: " + multigene.getGeneID());
        menu.add(item);

        item = new JMenuItem("Cluster zoom");
        item.setActionCommand(ACTION_SHOW_CLUSTER);
        item.addActionListener(this);
        menu.add(item);
//        MultigeneVisualizationManager.getDefault().multigeneClicked(multigene);
//        item = new JMenuItem("Nucleotide sequence");
//        item.setActionCommand(ACTION_NUC_SEQUENCE);
//        item.addActionListener(this);
//        menu.add(item);
//
//        item = new JMenuItem("Amino acids sequence");
//        item.setActionCommand(ACTION_AA_SEQUENCE);
//        item.addActionListener(this);
//        menu.add(item);
//
//        item = new JMenuItem("NCBI BLASTP API");
//        item.setActionCommand(ACTION_BLAST_API);
//        item.addActionListener(this);
//        menu.add(item);
//
//        item = new JMenuItem("BLASTN");
//        item.setActionCommand(ACTION_BLASTN);
//        item.addActionListener(this);
//        menu.add(item);
//
//        item = new JMenuItem("BLASTP");
//        item.setActionCommand(ACTION_BLASTP);
//        item.addActionListener(this);
//        menu.add(item);
//
//        item = new JMenuItem("CLUSTALW2 (Nuclotides)");
//        item.setActionCommand(ACTION_CLUSTAL_NUC);
//        item.addActionListener(this);
//        menu.add(item);
//
//        item = new JMenuItem("CLUSTALW2 (Amino acids)");
//        item.setActionCommand(ACTION_CLUSTAL_AA);
//        item.addActionListener(this);
//        menu.add(item);
    }

    @Override
    public JPopupMenu getPopupMenu(Widget widget, Point localLocation) {
        return menu;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(ACTION_SHOW_CLUSTER)) {
            ClusterVisTopComponent cv = ClusterVisTopComponent.getDefault();
            cv.setSelectedCluster(multigene.getCluster());
            cv.showInfo(multigene.getCluster());
            cv.showLists(multigene.getCluster());

            cv.zoomCluster();
        } else if (e.getActionCommand().equals(ACTION_NUC_SEQUENCE)) {
            String msg = ">" + multigene.getGeneID();
            msg += multigene.sequence();

            Messenger.getDefault().message(msg, "Multigene sequence");
        } else if (e.getActionCommand().equals(ACTION_AA_SEQUENCE)) {
            String msg = ">" + multigene.getGeneID();
            msg += multigene.sequence();

            Messenger.getDefault().message(msg, "Multigene sequence");
        } else if (e.getActionCommand().equals(ACTION_BLASTN)) {
            String msg = ">" + multigene.getGeneID();
            msg += multigene.sequence();

            Messenger.getDefault().message(msg, "Multigene sequence");
        } else if (e.getActionCommand().equals(ACTION_BLAST_API)) {
        }

    }
}
