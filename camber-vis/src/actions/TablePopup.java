/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package actions;

import java.awt.event.MouseEvent;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import org.netbeans.api.visual.action.PopupMenuProvider;
import org.netbeans.api.visual.widget.Widget;
import ui.ExcelTableAdapter;

public final class TablePopup extends MouseAdapter implements PopupMenuProvider, ActionListener {

    private static final String COPY = "Copy"; // NOI18N
    private static final String REMOVE = "Remove"; // NOI18N
    private static final String REMOVE_ALL = "Remove all"; // NOI18N
    private JPopupMenu menu;
    private ExcelTableAdapter adapter;
    private boolean is_copy = false;
    private boolean is_remove = false;
    private boolean is_remove_all = false;

    public TablePopup(ExcelTableAdapter adapter) {
        this.adapter = adapter;
        this.generate();
    }

    private void generate() {
        menu = new JPopupMenu("Menu");
        JMenuItem item;

        if (is_copy) {
            item = new JMenuItem("Copy (Ctrl+C)");
            item.addActionListener(this);
            item.setActionCommand(COPY);
            menu.add(item);
        }
        if (is_remove) {
            item = new JMenuItem("Remove");
            item.addActionListener(this);
            item.setActionCommand(REMOVE);
            menu.add(item);
        }
        if (is_remove_all) {
            item = new JMenuItem("Remove all");
            item.addActionListener(this);
            item.setActionCommand(REMOVE_ALL);
            menu.add(item);
        }
    }

    @Override
    public JPopupMenu getPopupMenu(Widget widget, Point localLocation) {
        return menu;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        showPopup(e);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        showPopup(e);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        showPopup(e);
    }

    private void showPopup(MouseEvent e) {
        if (e.isPopupTrigger()) {
            generate();
            menu.show(e.getComponent(), e.getX(), e.getY());
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        adapter.actionPerformed(e);
    }

    public boolean isIs_copy() {
        return is_copy;
    }

    public void setIs_copy(boolean is_copy) {
        this.is_copy = is_copy;
    }

    public boolean isIs_remove() {
        return is_remove;
    }

    public void setIs_remove(boolean is_remove) {
        this.is_remove = is_remove;
    }

    public boolean isIs_remove_all() {
        return is_remove_all;
    }

    public void setIs_remove_all(boolean is_remove_all) {
        this.is_remove_all = is_remove_all;
    }
}
