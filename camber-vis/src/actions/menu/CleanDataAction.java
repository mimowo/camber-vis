/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */

package actions.menu;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.openide.util.ImageUtilities;
import ui.VisualizationManager;
import init.CamberVisState;
import utils.DataHandle;
import utils.Messenger;
import utils.SettingsHandle;
import utils.VisSettingsHandle;
import vis.CamberVisTopComponent;
import vis.ClusterVisTopComponent;
import vis.ClustersStatsTopComponent;
import vis.MultigeneZoomTopComponent;

public final class CleanDataAction extends AbstractAction implements CamberStateChange {

    private static Icon icon = new ImageIcon(ImageUtilities.loadImage("icons/clean.png"));

    public CleanDataAction() {
        super("Clean data", icon);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        boolean ret = Messenger.getDefault().showConfirm("Are you sure?", "Clean up all loaded data.");
        if (ret) {
            ClustersStatsTopComponent.getDefault().cleanTable();
            ClusterVisTopComponent.getDefault().cleanMenu();
            MultigeneZoomTopComponent.getDefault().cleanTextMenu();
            VisualizationManager.getDefault().cleanGenomeBrowsers();
            DataHandle.getDefault().cleanData();
            VisSettingsHandle.getDefault().cleanColors();
            CamberVisState.getDefault().setData_loaded(false);
            CamberVisState.getDefault().setGenomes_shown(false);
            CamberVisState.getDefault().stateChanged();
            Messenger.getDefault().message("All data removed", "Data loading");
            ClusterVisTopComponent.getDefault().resetClustalWFrame();
            CamberVisTopComponent.getDefault().resetShowSequenceFrame();
        }
    }

    @Override
    public void camberStateUpdated(CamberVisState state) {
        if (state.isData_loaded()) {
            setEnabled(true);
        } else {
            setEnabled(false);
        }
    }
}
