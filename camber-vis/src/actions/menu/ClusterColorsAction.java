/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package actions.menu;

import init.CamberVisState;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.openide.util.ImageUtilities;
import ui.VisualizationManager;
import utils.Messenger;
import utils.SettingsHandle;

public final class ClusterColorsAction extends AbstractAction implements CamberStateChange {

    private static Icon icon = new ImageIcon(ImageUtilities.loadImage("icons/cc_id.png"));

    public ClusterColorsAction() {
        super("Color schema (by gene families)", icon);
    }

    @Override
    public void camberStateUpdated(CamberVisState state) {
        if (state.isGenomes_shown()) {
            setEnabled(true);
        } else {
            setEnabled(false);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        SettingsHandle sh = SettingsHandle.getDefault();
        if (sh.getSchemaColor().equals(SettingsHandle.SCHEMA_COLOR_CLUSTERS)) {
            Messenger.getDefault().message("Color scheme: types");
            putValue(Action.SHORT_DESCRIPTION, "Color schema (by gene families' types)");
            sh.setSchemaColor(SettingsHandle.SCHEMA_COLOR_TYPES);
           // VisualizationManager.getDefault().drawBrowserPanels();
            
        } else if (sh.getSchemaColor().equals(SettingsHandle.SCHEMA_COLOR_TYPES)) {
            Messenger.getDefault().message("Color scheme: conservation");
            putValue(Action.SHORT_DESCRIPTION, "Color schema (by gene families' conservation)");
            sh.setSchemaColor(SettingsHandle.SCHEMA_COLOR_CONS);
          //  VisualizationManager.getDefault().drawBrowserPanels();
        } else if (sh.getSchemaColor().equals(SettingsHandle.SCHEMA_COLOR_CONS)) {
            Messenger.getDefault().message("Color scheme: clusters");
            putValue(Action.SHORT_DESCRIPTION, "Color schema (by gene families)");
            sh.setSchemaColor(SettingsHandle.SCHEMA_COLOR_CLUSTERS);
        //    VisualizationManager.getDefault().drawBrowserPanels();
            
        } else {
            Messenger.getDefault().message("Color scheme: conservation");
            putValue(Action.SHORT_DESCRIPTION, "Color schema (by gene families' conservation)");
            sh.setSchemaColor(SettingsHandle.SCHEMA_COLOR_CONS);
            
            
        }
        VisualizationManager.getDefault().drawBrowserPanels();
        
    }
}