/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */

package actions.menu;

import java.awt.event.ActionEvent;
import java.io.IOException;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import ui.VisualizationManager;
import init.CamberVisState;
import java.awt.Image;
import java.io.File;
import java.lang.String;
import javax.swing.JFrame;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

public final class ExportBitmap extends AbstractAction implements CamberStateChange {

    private static Icon icon = new ImageIcon(ImageUtilities.loadImage("icons/save.png").getScaledInstance(16, 16, 0));

    public ExportBitmap() {
        super("Export to bitmap", icon);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        JFrame frame = new JFrame();

        final JFileChooser fc = new JFileChooser();
        Image icon_image = ImageUtilities.loadImage("icons/save.png");
        frame.setIconImage(icon_image);
        fc.setSelectedFile(new File("strains.jpg"));
        fc.setDialogTitle("Export as a jpeg bitmap.");
        FileNameExtensionFilter filter = new FileNameExtensionFilter("JPEG", "jpg", "jpeg");

        fc.setFileFilter(filter);
        int returnVal = fc.showSaveDialog(frame);

        

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            String filename = fc.getSelectedFile().getAbsolutePath();
            if (!filename.equals("")) {
                try {
                    VisualizationManager.getDefault().exportToBitmap(filename);
                } catch (IOException ex) {
                    Exceptions.printStackTrace(ex);
                }
            }
        }
    }

    @Override
    public void camberStateUpdated(CamberVisState state) {
        if (state.isGenomes_shown()) {
            setEnabled(true);
        } else {
            setEnabled(false);
        }
    }
}
