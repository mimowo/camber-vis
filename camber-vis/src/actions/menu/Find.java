/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */

package actions.menu;

import java.awt.Component;
import org.openide.util.HelpCtx;
import init.CamberVisState;
import org.openide.util.actions.CallableSystemAction;

public final class Find extends CallableSystemAction implements CamberStateChange {

    FindPanel findPanel = new FindPanel();

    @Override
    public void camberStateUpdated(CamberVisState state) {
        if (state.isGenomes_shown()) {
            setEnabled(true);
        } else {
            setEnabled(false);
        }
    }

    @Override
    public void setEnabled(boolean b) {
        if(CamberVisState.getDefault().isGenomes_shown()) {
            findPanel.setEnabled(true);
        } else {
            findPanel.setEnabled(false);
        }
    }

    @Override
    public Component getToolbarPresenter() {
        return findPanel;
    }

    @Override
    public void performAction() {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getName() {
        return "Find a multigene";
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }
}
