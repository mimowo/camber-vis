/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */

package actions.menu;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import org.openide.util.ImageUtilities;
import org.openide.windows.WindowManager;
import ui.LoadExampleForm;
import init.CamberVisState;
import java.awt.Image;

//@ActionID(id = "actions.LoadDataAction", category = "File")
//@ActionReferences(value = {
//    @ActionReference(path = "Menu/File", position = 400),
//    @ActionReference(path = "Toolbars/File", position = 500)})
public final class LoadDataAction extends AbstractAction implements CamberStateChange {

    private static Image icon_image = ImageUtilities.loadImage("icons/load.png");
    //private static Icon icon = ;
    private JFrame frame;

    public LoadDataAction() {
        super("Load input data", new ImageIcon(icon_image));
        this.frame = null;
    }

    @Override
    public void camberStateUpdated(CamberVisState state) {
        if (state.isData_loaded()) {
            setEnabled(false);
        } else {
            setEnabled(true);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (frame == null) {
            frame = new LoadExampleForm();
            frame.setIconImage(icon_image);
            frame.pack();
            frame.setLocationRelativeTo(WindowManager.getDefault().getMainWindow());
        }
        //  frame.setAlwaysOnTop(true);
        frame.setVisible(true);
    }
}
