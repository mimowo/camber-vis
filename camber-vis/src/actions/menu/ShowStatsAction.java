/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */

package actions.menu;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import org.openide.util.ImageUtilities;
import ui.StatsForm;
import init.CamberVisState;
import java.awt.Image;
import org.openide.windows.WindowManager;

public final class ShowStatsAction extends AbstractAction implements CamberStateChange {

    private static Image icon_image = ImageUtilities.loadImage("icons/stats.png");
    //private static Icon icon = ;

    public ShowStatsAction() {
        super("Show general statistics", new ImageIcon(icon_image));
    }

    @Override
    public void camberStateUpdated(CamberVisState state) {
        if (state.isData_loaded()) {
            setEnabled(true);
        } else {
            setEnabled(false);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        java.awt.EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                StatsForm frame = new StatsForm();
                frame.setIconImage(icon_image);
                //frame.showLabels();
                frame.pack();
                frame.setLocationRelativeTo(WindowManager.getDefault().getMainWindow());
                frame.setVisible(true);
            }
        });
    }
}
