/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package init;

import java.awt.Image;
import java.awt.Toolkit;
import java.lang.reflect.InvocationTargetException;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import vis.CamberVisTopComponent;

public class CamberVisInitializator {

    private static Image icon;
    private static Image scaled_icon;

    public static void initialize() {
        initializeCAMBerVisWindow();
        initializeLogo();
        initializeToolips();
    }

    public static void initializeLogo() {
        Toolkit kit = Toolkit.getDefaultToolkit();
        icon = ImageUtilities.loadImage("icons/load.png");
    }

    private static void initializeToolips() {
        ToolTipManager m = ToolTipManager.sharedInstance();

        m.setInitialDelay(2000);
        m.setDismissDelay(5000);
        m.setReshowDelay(1000);
    }

    public static Image getLogoIcon() {
        //Messenger.getDefault().showMessage("get");
        return scaled_icon;
    }

    private static void initializeCAMBerVisWindow() {
        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    CamberVisTopComponent.getDefault().open();

                }
            });
        } catch (InterruptedException ex) {
            Exceptions.printStackTrace(ex);
        } catch (InvocationTargetException ex) {
            Exceptions.printStackTrace(ex);
        }
    }
}
