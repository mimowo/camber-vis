/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package init;

import actions.menu.CamberStateChange;
import javax.swing.Action;
import org.openide.util.Utilities;

public class CamberVisState {

    private static CamberVisState state = new CamberVisState();
    private boolean data_loaded;
    private boolean genomes_shown;

    public static CamberVisState getDefault() {
        return state;
    }

    public boolean isData_loaded() {
        return data_loaded;
    }

    public void setData_loaded(boolean data_loaded) {
        this.data_loaded = data_loaded;
    }

    public boolean isGenomes_shown() {
        return genomes_shown;
    }

    public void setGenomes_shown(boolean genomes_shown) {
        this.genomes_shown = genomes_shown;
    }

    public void stateChanged() {
        updateActionsStates();
    }

    private void updateActionsStates() {
        for (Action action : Utilities.actionsForPath("Actions/")) {
            if (action instanceof CamberStateChange) {
                ((CamberStateChange) action).camberStateUpdated(state);
            }
        }
    }
}
