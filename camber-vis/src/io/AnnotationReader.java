/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package io;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import org.openide.util.Exceptions;
import structs.Contig;
import structs.Multigene;
import structs.MultigeneCluster;
import structs.MultigeneDataset;
import structs.Strain;
import tasks.NetbeansProgressListener;
import utils.DataHandle;
import utils.Messenger;
import utils.VisSettingsHandle;

public class AnnotationReader {

    private BufferedReader br = null;
    private FileInputStream fis = null;
    private BufferedInputStream bis = null;
    private DataInputStream dis = null;
    private String filename = null;
    private NetbeansProgressListener listener = null;

    public void setProgressListener(NetbeansProgressListener listener) {
        this.listener = listener;
    }

    public AnnotationReader(String filename) {
        this.filename = filename;
    }

    public Integer readLengthNonAnn(String length) {
        return Integer.valueOf(length);

    }

    public Integer readLengthSelOrAnn(String length) {
        return Integer.valueOf(length.substring(1, length.length()));
    }

    public Integer readLengthSelAndAnn(String length) {
        return Integer.valueOf(length.substring(2, length.length()));
    }
    
    public void readAnnotations() {
        MultigeneDataset data = DataHandle.getDefault().getData();
        Messenger.getDefault().message("Loading annotations");
        try {
            File file = new File(filename);
            fis = new FileInputStream(file);
            bis = new BufferedInputStream(fis);
            dis = new DataInputStream(bis);
            br = new BufferedReader(new InputStreamReader(dis));
            if (listener != null) {
                //listener.start();
                Long max = file.length();
                listener.switchToDeterminate(max.intValue());
                listener.progress("Loading unified annotations");
            }
            Integer count = 0;
            while (br.ready()) {
                count += 1;
                if (count % 100 == 0) {
                    Long current = fis.getChannel().position();
                    if (listener != null) {
                        listener.progress(current.intValue());
                    }
                }
                boolean removed = false;
                
                String line = br.readLine();
                if (line.startsWith("#")) {
                    removed = true;
                    line = line.substring(1);
                }
                String[] tokens = line.split("[\\t]");
                String cluster_id = tokens[0];
                Integer shift;
                if (tokens[1].replace(" ", ".").contains(".")) {
                    shift = 0;
                } else {
                    shift = 1;
                }
                String mg_id = tokens[1 + shift];
                String gene_id_full = tokens[2 + shift];
                String gene_id = gene_id_full;
                String gene_name = "";
                String[] gene_id_tokens = gene_id_full.split("[:]");
                if (gene_id_tokens.length > 1) { 
                    gene_id = gene_id_tokens[0];
                    gene_name = gene_id_tokens[1];
                }
                String[] mg_tokens = mg_id.split("[ .]");

                Integer end = Integer.valueOf(mg_tokens[mg_tokens.length - 3]);
                String strand = mg_tokens[mg_tokens.length - 2];
                String genome_id = mg_tokens[mg_tokens.length - 1];

                if (!data.hasContig(genome_id)) {
                    continue;
                }
                Contig contig = data.getContig(genome_id);
                Strain strain = data.getStrain(contig.getStrainName());

                Multigene multigene = new Multigene(gene_id, gene_name, end, strand, contig, strain);
                multigene.setRemoved(removed);
                multigene.setGeneName(gene_name);
                
                for (int i = 3 + shift; i < tokens.length; i++) {
                    int length;
                    if (tokens[i].startsWith("#")) {
                        if (tokens[i].contains("*")) {
                            length = readLengthSelAndAnn(tokens[i]);
                            multigene.addGene(length, true, true);
                        } else {
                            length = readLengthSelOrAnn(tokens[i]);
                            multigene.addGene(length, false, true);
                        }
                    } else {
                        if (tokens[i].contains("*")) {
                            length = readLengthSelOrAnn(tokens[i]);
                            multigene.addGene(length, true, false);
                        } else {
                            length = readLengthNonAnn(tokens[i]);
                            multigene.addGene(length, false, false);
                        }
                    }
                }
                
                data.addCluster(cluster_id);
                MultigeneCluster cluster = data.getCluster(cluster_id);
                multigene.setCluster(cluster);
                cluster.addMultigene(multigene);
                VisSettingsHandle.getDefault().checkColor(cluster_id);
                data.addMultigene(multigene);
            }
            
            for (MultigeneCluster cluster: data.getClusters()) {
                boolean removed = true;
                for (Multigene multigene:cluster.getMultigenes().getMultigenes()) {
                    if (!multigene.isRemoved()) {
                        removed = false;
                    }                
                }
                cluster.setRemoved(removed);
            }
            
            Messenger.getDefault().message("Unified annotations have been loaded.", "Data loading");
            br.close();
            dis.close();
            bis.close();
            fis.close();
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
    }
}
