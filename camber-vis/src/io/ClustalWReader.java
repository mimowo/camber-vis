/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */

package io;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.TreeMap;
import structs.ExtClustalWResult;
import utils.Messenger;

public class ClustalWReader {

    private String input_fn;
    private ExtClustalWResult result;

    public ClustalWReader(ExtClustalWResult result, String input_fn) {
        this.result = result;
        this.input_fn = input_fn;
    }

    public void read() {
        Map<Integer, String> line_nums_map = new TreeMap<Integer, String>();
        Map<String, String> seq_map = new TreeMap<String, String>();
        try {
            
            // Open the file that is the first
            // command line parameter
            FileInputStream fstream = new FileInputStream(this.input_fn);
            // Get the object of DataInputStream
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine = br.readLine();
            //   Messenger.getDefault().showMessage("reading: " + strLine);
            String[] tokens_line0 = strLine.split("\\s+");
            Integer line_count = Integer.valueOf(tokens_line0[1]);
            Integer line_nr = 0;
            //Read File Line By Line

            //Messenger.getDefault().showMessage("Line count: " + line_count.toString());
            while ((strLine = br.readLine()) != null) {
                if (line_nr < line_count) {
                    String[] index_str_tokens = strLine.split("[_]");
                    String index = String.valueOf(index_str_tokens[0]);
                    line_nums_map.put(line_nr, index);
                    seq_map.put(index, "");
                }
                String seq_line = "";
                String[] tokens = strLine.split("\\s+");
                if (line_nr < line_count) {
                    for (int i = 1; i < tokens.length; i++) {
                        seq_line += tokens[i];
                    }
                } else {
                    for (int i = 1; i < tokens.length; i++) {
                        seq_line += tokens[i];
                    }
                }
                String aln_index = line_nums_map.get(Integer.valueOf(line_nr % line_count));
                //Messenger.getDefault().showMessage("aln_index: " + aln_index.toString());
                String seq = seq_map.get(aln_index);
                seq += seq_line;
                seq_map.put(aln_index, seq);

                line_nr += 1;
                if (line_nr % line_count == 0) {
                    br.readLine();
                }
            }
            //Close the input stream
            in.close();
            result.setAln_seq_map(seq_map);
          //  Messenger.getDefault().showMessage("Reading... " + Integer.valueOf(seq_map.size()).toString());
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
    }
}
