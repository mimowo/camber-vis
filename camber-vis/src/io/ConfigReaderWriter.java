/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */

package io;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import org.openide.util.Exceptions;
import utils.StringUtils;

public class ConfigReaderWriter {

    private BufferedReader br = null;
    private FileInputStream fis = null;
    private BufferedInputStream bis = null;
    private DataInputStream dis = null;
    private String config_fn = null;

    public ConfigReaderWriter(String config_fn) {
        this.config_fn = config_fn;
    }

    public Map<String, String> readConfig() {
        Map<String, String> ret = new TreeMap<String, String>();
        try {

            File file = new File(config_fn);
            if (!file.exists()) {
                return ret;
            }
            fis = new FileInputStream(file);
            bis = new BufferedInputStream(fis);
            dis = new DataInputStream(bis);
            br = new BufferedReader(new InputStreamReader(dis));

            while (br.ready()) {

                String line = br.readLine();
                String[] tokens = line.split("[=]");
                if (tokens.length == 2) {
                    String key = tokens[0];
                    String value = StringUtils.chop(tokens[1]);
                    if (!ret.containsKey(key)) {
                        ret.put(key, value);
                    }
                } else if (tokens.length == 1) {
                    String key = tokens[0];
                    ret.put(key, "");
                }
            }
            br.close();
            dis.close();
            bis.close();
            fis.close();
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
        return ret;
    }

    public void saveConfig(Map<String, String> config) {
        FileWriter fstream = null;
        try {
            fstream = new FileWriter(config_fn, false);
            BufferedWriter out = new BufferedWriter(fstream);
            for (Entry<String, String> entry : config.entrySet()) {
                out.write(entry.getKey() + "=" + entry.getValue() + "\n");
            }
            out.close();
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        } finally {
            try {
                fstream.close();
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
    }
}
