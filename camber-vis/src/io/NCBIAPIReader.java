/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package io;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import structs.BLASTResultEntry;
import structs.ExtNCBIAPIResult;

public class NCBIAPIReader {

    private String input_fn;
    private ExtNCBIAPIResult result;

    public NCBIAPIReader(ExtNCBIAPIResult result, String input_fn) {
        this.result = result;
        this.input_fn = input_fn;
    }

    public void read() {
        try {
            FileInputStream fstream = new FileInputStream(this.input_fn);
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine = "xxx";
            Integer count = 0;
            boolean alignments = false;

            while (strLine != null) {

                if (strLine.contains("RID:")) {
                    String[] tokens = strLine.split("\\s+");
                    result.setRid(tokens[1]);
                    strLine = br.readLine();

                } else if (strLine.contains("ALIGNMENTS")) {
                    alignments = true;
                    strLine = br.readLine();
                } else if (alignments) {
                    boolean first = true;
                    Integer query_aln_start = 1;
                    Integer hit_aln_start = 1;
                    if (strLine.length() > 0 && strLine.charAt(0) == '>') {
                        String aln_query = "";
                        String aln_hit = "";
                        String expect = "";
                        count += 1;
                        // Messenger.getDefault().showMessage("reading: " + count.toString());
                        BLASTResultEntry entry = new BLASTResultEntry();
                        entry.setSpeciesName(strLine);
                        while ((strLine = br.readLine()) != null) {
                            String[] tokens = strLine.split("\\s+");
                            if (tokens.length >= 3) {
                                if (tokens[0].equals("Query")) {
                                    aln_query += tokens[2];
                                    if (first) {
                                        query_aln_start = Integer.valueOf(tokens[1]);
                                        entry.setQueryAlnStart(query_aln_start);
                                    }
                                } else if (tokens[0].equals("Sbjct")) {
                                    aln_hit += tokens[2];
                                    if (first) {
                                        hit_aln_start = Integer.valueOf(tokens[1]);
                                        entry.setHitAlnStart(hit_aln_start);
                                        first = false;
                                    }
                                }
                                if (strLine.contains("Expect")) {
                                    for (int i = 0; i < tokens.length; i++) {
                                        if (tokens[i].equals("Expect")) {
                                            String expect_str = tokens[i + 2];
                                            if (expect_str.charAt(expect_str.length() - 1) == ',') {
                                                expect = expect_str.substring(0, expect_str.length() - 1);
                                            } else {
                                                expect = expect_str;
                                            }
                                            break;
                                        }
                                    }

                                }
                            }
                            if (strLine.length() > 0 && strLine.charAt(0) == '>') {
                                entry.setAln_query(aln_query);
                                entry.setAln_hit(aln_hit);
                                entry.setEvalue(expect);
                                entry.setIndex(count);

                                if (result.isType()) {
                                    if (query_aln_start == 1) {
                                        entry.setAln_tiss(true);
                                    } else {
                                        entry.setAln_tiss(false);
                                    }
                                } else {
                                    if (query_aln_start == 1 && hit_aln_start == 1) {
                                        entry.setAln_tiss(true);
                                    } else {
                                        entry.setAln_tiss(false);
                                    }
                                }
                                result.addResultEntry(entry);
                                break;
                            }

                        }
                        if (strLine == null) {
                            entry.setAln_query(aln_query);
                            entry.setAln_hit(aln_hit);
                            entry.setEvalue(expect);
                            entry.setIndex(count);
                            if (result.isType()) {
                                if (query_aln_start == 1) {
                                    entry.setAln_tiss(true);
                                } else {
                                    entry.setAln_tiss(false);
                                }
                            } else {
                                if (query_aln_start == 1 && hit_aln_start == 1) {
                                    entry.setAln_tiss(true);
                                } else {
                                    entry.setAln_tiss(false);
                                }
                            }
                            result.addResultEntry(entry);
                        }
                    }
                } else {
                    strLine = br.readLine();
                }

            }
            //Close the input stream
            in.close();

        } catch (Exception e) {//Catch exception if any
            //     Messenger.getDefault().showMessage("error: " + e.getMessage());
            System.err.println("Error: " + e.getMessage());
        }
    }
}
