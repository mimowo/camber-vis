/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package io;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import org.openide.util.Exceptions;
import structs.Contig;
import structs.Strain;
import utils.DataHandle;
import tasks.NetbeansProgressListener;
import utils.FileChecker;
import utils.Messenger;

public class SequencesReader {

    private BufferedReader br = null;
    private FileInputStream fis = null;
    private BufferedInputStream bis = null;
    private DataInputStream dis = null;
    private String dirname;
    private NetbeansProgressListener listener = null;

    public SequencesReader(String dirname) {
        this.dirname = dirname;
    }

    public void setProgressListener(NetbeansProgressListener listener) {
        this.listener = listener;
    }

    public Map<String, String> readGenomeSequencesMap(String strain_filename) {

        try {
            Map<String, String> seqMap = new TreeMap<String, String>();
            File file = new File(strain_filename);
            fis = new FileInputStream(file);
            bis = new BufferedInputStream(fis);
            dis = new DataInputStream(bis);
            br = new BufferedReader(new InputStreamReader(dis));

            StringBuilder sequence = new StringBuilder("");
            String seq_id = "";
            while (br.ready()) {
                String line = br.readLine().trim();

                if (!line.contains(">")) {
                    String[] tokens = line.split("\\s+");
                    if (tokens.length == 1) {
                        sequence.append(tokens[0]);
                    }
                } else {
                    if (sequence.length() > 0 && !seq_id.equals("")) {
                        seqMap.put(seq_id, sequence.toString());
                    }
                    seq_id = line.substring(1);
                    sequence = new StringBuilder("");
                }
            }
            br.close();
            dis.close();
            bis.close();
            fis.close();
            if (sequence.length() > 0 && !seq_id.equals("")) {
                seqMap.put(seq_id, sequence.toString());
            }
            return seqMap; //sequence.toString();
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
        return null;
    }

//    public void loadStrainGenome(String strain_id, String seq_fn) {
//        DataHandle dh = DataHandle.getDefault();
//        String sequence = readGenomeSequence(seq_fn);
//        dh.getData().addStrain(strain_id);
//        dh.getData().getStrain(strain_id).setSequence(sequence);
//    }
    public void loadGenomeSequences() {

        DataHandle dh = DataHandle.getDefault();

        if (listener != null) {
            //listener.start();
            listener.switchToDeterminate(dh.getData().getStrains().size());
        }
        Integer count = 0;
        for (Strain strain : dh.getData().getStrains()) {
            String strain_id = strain.getName();
            if (listener != null) {
                listener.progress(count);
                listener.progress("Loading sequence for: " + strain_id);
            }

            String strain_fn = dirname + "/" + "seq-" + strain_id + ".fasta";
            Messenger.getDefault().message(strain_fn, "Before");

            if (FileChecker.fnExists(strain_fn)) {

                Map<String, String> strain_seqs = readGenomeSequencesMap(strain_fn);
                for (Entry<String, String> entry : strain_seqs.entrySet()) {
                    String contig_id = entry.getKey();

                    if (strain_seqs.size() == 1) {
                        contig_id = strain.getName();
                    }
                    String sequence = entry.getValue();
                    String desc = "contig";

                    if (contig_id.equals(strain_id)) {
                        desc = "main";
                    }

                    if (strain.getContigNames().contains(contig_id)) {
                        Contig contig = strain.getContig(contig_id);
                        contig.setSequence(sequence);
                    } else {
                        Contig contig = new Contig(strain, sequence, contig_id, desc);
                        dh.getData().addContig(contig);
                    }
                }
                Messenger.getDefault().message(strain_fn, "Plasmids loaded");
            } else {
                Messenger.getDefault().message("File not found: " + strain_fn);
            }

            for (Contig contig : strain.getContigs()) {
                if (contig.getSequence() == null || contig.getSequence().length() == 0) {
                    String contig_id = contig.getName();
                    String contig_fn = dirname + "/" + "seq-" + contig_id + ".fasta";
                    if (FileChecker.fnExists(contig_fn)) {
                        Map<String, String> seqs = readGenomeSequencesMap(contig_fn);
                        for (Entry<String, String> entry : seqs.entrySet()) {
                            if (contig_id.equals(entry.getKey()) || seqs.size() == 1) {
                                Messenger.getDefault().message(contig.getName() + ". Plasmid loaded: " + entry.getValue());
                                contig.setSequence(entry.getValue());
                            }
                        }
                    }
                }
            }
            count += 1;
        }
        Messenger.getDefault().message("Genome sequences have been loaded.", "Data loading");
    }
}
