/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package io;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import org.openide.util.Exceptions;
import structs.MultigeneDataset;
import tasks.NetbeansProgressListener;
import utils.DataHandle;
import utils.Messenger;

public class StrainsReader {

    private BufferedReader br = null;
    private FileInputStream fis = null;
    private BufferedInputStream bis = null;
    private DataInputStream dis = null;
    private String filename = null;
    private NetbeansProgressListener listener = null;

    public void setProgressListener(NetbeansProgressListener listener) {
        this.listener = listener;
    }

    public StrainsReader(String filename) {
        this.filename = filename;
    }

    public Integer readLengthNonAnn(String length) {
        return Integer.valueOf(length);

    }

    public Integer readLengthAnn(String length) {
        return Integer.valueOf(length.substring(1, length.length()));
    }

    public void readStrains() {
        try {
            File file = new File(filename);
            fis = new FileInputStream(file);
            bis = new BufferedInputStream(fis);
            dis = new DataInputStream(bis);
            br = new BufferedReader(new InputStreamReader(dis));
            if (listener != null) {
                listener.switchToIndeterminate();
                listener.progress("Loading strains data");
            }
            MultigeneDataset data = DataHandle.getDefault().getData();
            while (br.ready()) {

                String line = br.readLine();
                if (line.length() > 0 && line.charAt(0) == '#') {
                    continue;
                }
                String[] tokens = line.split("\\s+");
                if (tokens.length > 0 && !tokens[0].startsWith("#")) {
                    if (tokens.length == 1 || (tokens.length >= 2 && tokens[1].equals("-"))) {
                        String strain_id = tokens[0];
                        data.addStrain(strain_id);
                    } else if (tokens.length == 2) {
                        String contig_id = tokens[0];
                        String strain_id = tokens[1];
                        if (tokens[0].equals(tokens[1])) {
                            data.addContig(strain_id, contig_id, "main");
                        } else {
                            data.addContig(strain_id, contig_id, "contig");
                        }
                    } else if (tokens.length == 3) {
                        String contig_id = tokens[0];
                        String strain_id = tokens[1];
                        String desc = tokens[2];
                        if (desc.startsWith("m")) {
                            desc = "main";
                        } else if (desc.startsWith("p")) {
                            desc = "plasmid";
                        } else if (desc.startsWith("c")) {
                            desc = "contig";
                        } else {
                            desc = "contig";
                        }
                        data.addContig(strain_id, contig_id, desc);
                    }
                }
            }
            Messenger.getDefault().message("The file with info about strains has been loaded.", "Data loading");
            br.close();
            dis.close();
            bis.close();
            fis.close();

        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
    }
}
