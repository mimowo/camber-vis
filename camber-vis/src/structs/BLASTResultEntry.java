/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package structs;

public class BLASTResultEntry {

    private Integer hitAlnStart;
    private Integer queryAlnStart;
    private String speciesName;
    private String evalue;
    private String identities;
    private String aln_query;
    private String aln_hit;
    private Integer index;
    private boolean aln_tiss;

    public BLASTResultEntry() {
    }

    public boolean isAln_tiss() {
        return aln_tiss;
    }

    public void setAln_tiss(boolean aln_tiss) {
        this.aln_tiss = aln_tiss;
    }

    public String getAln_hit() {
        return aln_hit;
    }

    public void setAln_hit(String aln_hit) {
        this.aln_hit = aln_hit;
    }

    public String getAln_query() {
        return aln_query;
    }

    public void setAln_query(String aln_query) {
        this.aln_query = aln_query;
    }

    public String getEvalue() {
        return evalue;
    }

    public void setEvalue(String evalue) {
        this.evalue = evalue;
    }

    public String getIdentities() {
        return identities;
    }

    public void setIdentities(String identities) {
        this.identities = identities;
    }

    public String getSpeciesName() {
        return speciesName;
    }

    public void setSpeciesName(String speciesName) {
        this.speciesName = speciesName;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getAlnTISs() {
        if (aln_tiss) {
            return "YES";
        } else {
            return "NO";
        }
    }

    public void setHitAlnStart(Integer hit_aln_start) {
        this.hitAlnStart = hit_aln_start;
    }

    public void setQueryAlnStart(Integer query_aln_start) {
        this.queryAlnStart = query_aln_start;
    }

    public Integer getHitAlnStart() {
        return hitAlnStart;
    }

    public Integer getQueryAlnStart() {
        return queryAlnStart;
    }
}
