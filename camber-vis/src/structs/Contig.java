/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package structs;

import utils.SequenceUtils;

public class Contig {

    private String name;
    private String desc;
    private String sequence;
    private Strain strain;
    private MultigeneContigCollection annotation = new MultigeneContigCollection();

    public Contig(Strain strain, String name, String desc) {
        this.strain = strain;
        this.name = name;
        this.desc = desc;
    }

    public Contig(Strain strain, String sequence, String name, String desc) {
        this.strain = strain;
        this.sequence = sequence;
        this.name = name;
        this.desc = desc;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getName() {
        return name;
    }

    public Integer length() {
        if (sequence != null) {
            return sequence.length();
        } else {
            return 0;
        }
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Strain getStrain() {
        return strain;
    }

    public void setStrain(Strain strain) {
        this.strain = strain;
    }

    public MultigeneContigCollection getAnnotation() {
        return annotation;
    }

    public void setAnnotation(MultigeneContigCollection annotation) {
        this.annotation = annotation;
    }

    public String getStrainName() {
        return this.strain.getName();
    }

    public boolean isMain() {
        return desc.equals("main");
    }

    public boolean isPlasmid() {
        return desc.equals("plasmid");
    }

    public String getSubSequence(Integer start, Integer end, String strand, boolean rev) {
        if (sequence == null) {
            return "";
        }
        start = Math.min(Math.max(1, start), sequence.length());
        end = Math.min(Math.max(1, end), sequence.length());
        if (start > end) {
            Integer tmp = start;
            start = end;
            end = tmp;
        }

        String seq = sequence.substring(start - 1, end);
        if (strand.equals("+")) {
            if (rev) {
                return SequenceUtils.revSequence(seq);
            } else {
                return seq;
            }
        } else {
            if (rev) {
                return SequenceUtils.compRevSequence(seq);
            } else {
                return SequenceUtils.compSequence(seq);
            }
        }
    }
}
