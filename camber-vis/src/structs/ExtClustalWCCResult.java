/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */

package structs;

import java.util.Map;
import java.util.TreeMap;

public class ExtClustalWCCResult extends ExtToolResult {

    //private Map<Integer, String> strain_names_map = new TreeMap<Integer, String>();
    private Map<Integer, Multigene> mult_map = new TreeMap<Integer, Multigene>();
    private Map<Integer, Integer> mult_tis_map = new TreeMap<Integer, Integer>();
    private Map<Integer, String> aln_seq_map = new TreeMap<Integer, String>();
    private boolean type;
    private MultigeneCluster cluster;
    private Integer promotor_length;

    public ExtClustalWCCResult(boolean type, Integer promotor_length, String date, String jobID) {
        super(jobID, date, "ClustalW");
        this.type = type;
        this.promotor_length = promotor_length;
    }

    public MultigeneCluster getCluster() {
        return cluster;
    }

    public void setCluster(MultigeneCluster cluster) {
        this.cluster = cluster;
    }

    public Integer getPromotor_length() {
        return promotor_length;
    }

    public void setPromotor_length(Integer promotor_length) {
        this.promotor_length = promotor_length;
    }

    public boolean isType() {
        return type;
    }

    public void putSequenceID(Integer count, Multigene multigene) {
        mult_map.put(count, multigene);
    }

    public Map<Integer, String> getAln_seq_map() {
        return aln_seq_map;
    }

    public void setAln_seq_map(Map<Integer, String> aln_seq_map) {
        this.aln_seq_map = aln_seq_map;
    }

    public Map<Integer, Multigene> getMult_map() {
        return mult_map;
    }

    public void setMult_map(Map<Integer, Multigene> mult_map) {
        this.mult_map = mult_map;
    }

    public Map<Integer, Integer> getMult_tis_map() {
        return mult_tis_map;
    }

    public void setMult_tis_map(Map<Integer, Integer> mult_tis_map) {
        this.mult_tis_map = mult_tis_map;
    }
}
