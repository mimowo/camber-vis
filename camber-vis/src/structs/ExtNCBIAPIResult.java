/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */

package structs;

import java.util.ArrayList;
import utils.SequenceUtils;

public class ExtNCBIAPIResult extends ExtToolResult {

    private boolean type;
    private Multigene multigene;
    private Integer tis;
    private String database;
    private String expect;
    private String rid;
    private Integer rtoe;
    private String email;
    private String tool;
    private ArrayList<BLASTResultEntry> result_entries = new ArrayList<BLASTResultEntry>();

    public ExtNCBIAPIResult(Multigene multigene, boolean type, Integer tis, String date, String jobID) {
        super(jobID, date, "NCBI API");
        this.type = type;
        this.tis = tis;
        this.multigene = multigene;
        this.database = "nr";
        this.expect = "1e-6";
        this.email = "";
        this.tool = "CamberVis";
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTool() {
        return tool;
    }

    public void setTool(String tool) {
        this.tool = tool;
    }

    public Multigene getMultigene() {
        return multigene;
    }

    public void setMultigene(Multigene multigene) {
        this.multigene = multigene;
    }

    public Integer getTis() {
        return tis;
    }

    public void setTis(Integer tis) {
        this.tis = tis;
    }

    public boolean isType() {
        return type;
    }

    public void setType(boolean type) {
        this.type = type;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getExpect() {
        return expect;
    }

    public void setExpect(String expect) {
        this.expect = expect;
    }

    public void addResultEntry(BLASTResultEntry entry) {
        result_entries.add(entry);
    }

    public ArrayList<BLASTResultEntry> getResult_entries() {
        return result_entries;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String createGetURL() {
        String url = "http://www.ncbi.nlm.nih.gov/blast/Blast.cgi?";
        url += "CMD=Get&";
        url += "FORMAT_TYPE=Text&";
        url += "RID=" + rid.toString();

        return url;
    }

    public String createPutURL() {
        String url = "http://www.ncbi.nlm.nih.gov/blast/Blast.cgi?";
        url += "CMD=Put&";
        url += "DATABASE=" + this.getDatabase() + "&";
        url += "AUTO_FORMAT=Fullauto&";
        url += "EXPECT=" + this.getExpect() + "&";
        url += "TOOL=" + this.getTool() + "&";
        if (!this.getEmail().equals("")) {
            url += "EMAIL=" + this.getEmail() + "&";
        }
        if (this.isType()) {
            url += "PROGRAM=blastn&";
            url += "QUERY=" + this.getMultigene().sequence(this.getTis());
        } else {
            url += "PROGRAM=blastp&";
            url += "QUERY=" + SequenceUtils.translate(this.getMultigene().sequence(this.getTis()));
        }

        return url;
    }

    public Integer getRtoe() {
        return rtoe;
    }

    public void setRtoe(Integer rtoe) {
        this.rtoe = rtoe;
    }
}
