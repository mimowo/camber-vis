/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package structs;

import java.awt.Color;
import java.util.Collection;
import java.util.PriorityQueue;
import java.util.TreeSet;
import utils.ColorUtils;
import utils.DataHandle;
import utils.Messenger;
import utils.SequenceUtils;
import utils.VisSettingsHandle;

public class Multigene implements Comparable<Multigene> {

    private String gene_id;
    private String gene_name;
    private Integer end;
    private Integer start;
    private String strand;
    private Contig contig;
    private Strain strain;
    private MultigeneCluster cluster;
    private Integer left_bound;
    private Integer right_bound;
    private Integer length;
    private Integer int_strand;
    private Color color;
    private Color type_color;
    private Color conservation_color;
    private PriorityQueue<Integer> TISs = new PriorityQueue<Integer>();
    private Collection<Integer> allTISs = null;
    private Integer annTIS;
    private Integer selTIS;
    private boolean removed = false;

    public Multigene(String gene_id, String gene_name, Integer end, String strand, Contig contig, Strain strain) {
        this.gene_id = gene_id;
        this.gene_name = gene_name;
        this.end = end;
        this.strand = strand;
        if (this.strand.equals("+")) {
            this.int_strand = 1;
        } else {
            this.int_strand = -1;
        }
        this.contig = contig;
        this.strain = strain;
        this.length = 0;
    }

    public String getGeneName() {
        return gene_name;
    }

    public void setGeneName(String gene_name) {
        this.gene_name = gene_name;
    }

    public Color getClusterColor() {
        if (color == null) {
            color = VisSettingsHandle.getDefault().checkColor(this.getCluster().getCluster_id());
        }
        return color;
    }

    public Color getTypeColor() {
        if (type_color == null) {
            if (cluster.isAnchor()) {
                if (cluster.isInAllStrains()) {
                    type_color = Color.RED;
                } else if (cluster.countStrains() == 1) {
                    type_color = Color.GRAY;
                } else {
                    type_color = Color.ORANGE;
                }
            } else {
                if (cluster.isInAllStrains()) {
                    type_color = Color.GREEN;
                } else {
                    type_color = Color.BLUE;
                }
            }
        }
        return type_color;
    }

    public Color getConservationColor() {
        if (conservation_color == null) {
            if (cluster.isAnchor()) {
                Integer strains_count = DataHandle.getDefault().getData().getStrainCount();
                Integer strains_cluster_count = cluster.countStrains();
                float ratio = strains_cluster_count.floatValue() / strains_count.floatValue();
                conservation_color = ColorUtils.gradedColor(Color.YELLOW, Color.RED, ratio);
            } else {
                Integer strains_count = DataHandle.getDefault().getData().getStrainCount();
                Integer strains_cluster_count = cluster.countStrains();
                float ratio = strains_cluster_count.floatValue() / strains_count.floatValue();
                conservation_color = ColorUtils.gradedColor(Color.BLUE, Color.GREEN, ratio);
            }
        }
        return conservation_color;
    }

    public Integer getIntStrand() {
        return int_strand;
    }

    public void addGene(Integer gene_length, boolean annotated, boolean selected) {
        //Gene gene = new Gene(length, annotated);
        //genes.add(gene);
        TISs.add(gene_length);
        if (annotated) {
            this.annTIS = gene_length;
        }

        if (selected) {
            this.selTIS = gene_length;
        }

        if (gene_length > this.length) {
            if (int_strand.equals(1)) {
                this.start = this.end - gene_length + 1;
                this.left_bound = this.start;
                this.right_bound = this.end;
            } else {
                this.start = this.end + gene_length - 1;
                this.right_bound = this.start;
                this.left_bound = this.end;
            }
            this.length = this.right_bound - this.left_bound + 1;
        }
        //       if (this.length < gene.getLength()) {
        //}
    }

    public String promotor_sequence(Integer tis_length, Integer prom_length) {
        String strain_sequence = contig.getSequence();
        Integer genome_len = contig.length();
        if (strain_sequence == null) {
            return null;
        }
        Integer shift = this.length - tis_length;
        if (this.int_strand.equals(1)) {
            Integer left_b = Math.max(0, this.left_bound - prom_length + shift - 1);
            Integer right_b = Math.min(genome_len - 1, this.left_bound + shift - 1);

            String prom_sequence = strain_sequence.substring(left_b, right_b);
            return prom_sequence;
        } else {
            Integer left_b = Math.max(0, this.right_bound - shift);
            Integer right_b = Math.min(genome_len - 1, this.right_bound + prom_length - shift);

            String prom_sequence = strain_sequence.substring(left_b, right_b);
            return SequenceUtils.compRevSequence(prom_sequence);
        }
    }

    public String promotor_sequence(Integer prom_length) {
        return promotor_sequence(this.length, prom_length);
    }

    public String sequence() {
        return sequence(this.length);
    }

    public String sequence(Integer tis_length) {
        String strain_sequence = contig.getSequence();
        if (strain_sequence == null) {
            return null;
        }

        Integer genome_len = contig.length();
        Integer shift = this.length - tis_length;

        if (this.int_strand.equals(1)) {
            Integer left_b = Math.max(0, this.left_bound + shift - 1);
            Integer right_b = Math.min(genome_len - 1, this.right_bound);

            String gene_sequence = strain_sequence.substring(left_b, right_b);
            return gene_sequence;
        } else {
            Integer left_b = Math.max(0, this.left_bound - 1);
            Integer right_b = Math.min(genome_len - 1, this.right_bound - shift);
            String gene_sequence = strain_sequence.substring(left_b, right_b);
            return SequenceUtils.compRevSequence(gene_sequence);
        }
    }

    public MultigeneCluster getCluster() {
        return cluster;
    }

    public void setCluster(MultigeneCluster cluster) {
        this.cluster = cluster;
    }

    public String getGeneID() {
        return gene_id;
    }

    public Integer getEnd() {
        return end;
    }

    public Contig getContig() {
        return contig;
    }

    public Strain getStrain() {
        return strain;
    }

    public String getStrand() {
        return strand;
    }

    public Integer getLeft_bound() {
        return left_bound;
    }

    public void setLeft_bound(Integer left_bound) {
        this.left_bound = left_bound;
    }

    public Integer getRight_bound() {
        return right_bound;
    }

    public void setRight_bound(Integer right_bound) {
        this.right_bound = right_bound;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getLength() {
        return length;
    }

    public Integer getAnnTIS() {
        return annTIS;
    }

    public Integer getSelTIS() {
        return selTIS;
    }

    public PriorityQueue<Integer> getTISs() {
        return TISs;
    }

    public String getMultigeneID() {
        if (gene_name.length() > 1) {
            return this.gene_id + " " + gene_name + " " + this.strand + " " + this.contig.getName() + " " + this.length.toString();
        }
        return this.gene_id + " " + this.strand + " " + this.contig.getName() + " " + this.length.toString();
    }

    public String getMultigeneCCID() {
        if (gene_name.length() > 1) {
            return this.gene_id + " " + gene_name + " "+ this.strand + " " + this.end.toString() + "(cc: " + this.getCluster().getCluster_id() + ")";
        }
        return this.gene_id + " " + this.strand + " " + this.end.toString() + "(cc: " + this.getCluster().getCluster_id() + ")";
    }

    public String getGeneIDandName() {
        if (gene_name.length() > 1) {
            return this.gene_id + " " + gene_name;
        }
        return this.gene_id;
    }

    
    public String getMultigeneGenomeCCID() {
        if (gene_name.length() > 1) {
            return this.gene_id + " " + gene_name + "(" + this.getContig().getName() + ")" + " " + this.strand + " " + this.end.toString() + "(cc: " + this.getCluster().getCluster_id() + ")";
        }
        return this.gene_id + "(" + this.getContig().getName() + ")" + " " + this.strand + " " + this.end.toString() + "(cc: " + this.getCluster().getCluster_id() + ")";
    }

    public String getUniqueMultigeneID() {
        return this.getEnd().toString() + ";" + this.strand + ";" + this.contig.getName();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Multigene other = (Multigene) obj;

        return other.getUniqueMultigeneID().equals(this.getUniqueMultigeneID());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.gene_id != null ? this.gene_id.hashCode() : 0);
        hash = 59 * hash + (this.end != null ? this.end.hashCode() : 0);
        hash = 59 * hash + (this.strand != null ? this.strand.hashCode() : 0);
        hash = 59 * hash + (this.contig != null ? this.contig.hashCode() : 0);
        return hash;
    }

    @Override
    public int compareTo(Multigene other) {
        return other.getUniqueMultigeneID().compareTo(this.getUniqueMultigeneID());
    }

    boolean isAnnotated() {
        if (annTIS == null) {
            return false;
        } else {
            return true;
        }
    }

    public Collection<Integer> getAllTISs() {
        if (allTISs == null) {
            allTISs = new TreeSet<Integer>();
            String seq = this.sequence();
            for (int i = 0; i < seq.length() - 3; i += 3) {
                String codon = seq.substring(i, i + 3);
                if (codon.equals("ATG") || codon.equals("GTG") || codon.equals("TTG")) {
                    allTISs.add(seq.length() - i);
                }
            }
            for (Integer tis : TISs) {
                allTISs.add(tis);
            }
        }
        return allTISs;
    }

    boolean hasUntypicalStart() {
        String seq = this.sequence();
        if (seq != null && seq.length() >= 3) {
            for (Integer tis : TISs) {
                Integer i = seq.length() - tis;
                if (i >= 0) {
                    String codon = seq.substring(i, i + 3);
                    if (!(codon.equals("ATG") || codon.equals("GTG") || codon.equals("TTG"))) {
                        return Boolean.TRUE;
                    }
                }
            }
        }
        return Boolean.FALSE;
    }

    public void setRemoved(boolean removed) {
        this.removed = removed;
    }
    
    public boolean isRemoved() {
        return removed;
    }
}
