/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package structs;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import utils.DataHandle;
import utils.Messenger;

public class MultigeneCluster {

    private String cluster_id;
    private MultigeneCollection multigenes = new MultigeneCollection();
    private Boolean is_anchor;
    private Boolean in_all_strains;
    private Integer number_of_strains = 0;
    private Integer number_of_ann_strains = 0;
    private Integer max_length = 0;
    private Integer max_tis = 0;
    private Integer main_size;
    private Integer plasmids_size;
    private Double overlap;
    private Boolean untypical_start;
    private boolean removed = false;

    public void setRemoved(boolean removed) {
        this.removed = removed;
    }

    public boolean isRemoved() {
        return removed;
    }

    public Set<Multigene> getMultigenesByStrain(String strain_id) {
        Set<Multigene> strain_mults = new HashSet<Multigene>();
        for (Multigene multigene : multigenes.getMultigenes()) {
            if (multigene.getStrain().getName().equals(strain_id)) {
                strain_mults.add(multigene);
            }
        }
        return strain_mults;
    }

    public Set<Multigene> getMultigenesByContig(String contig_id) {
        Set<Multigene> strain_mults = new HashSet<Multigene>();
        for (Multigene multigene : multigenes.getMultigenes()) {
            if (multigene.getContig().getName().equals(contig_id)) {
                strain_mults.add(multigene);
            }
        }
        return strain_mults;
    }

    public boolean isAnchor() {
        if (is_anchor != null) {
            return is_anchor;
        } else {
            Set<String> strains = new TreeSet<String>();
            for (Multigene multigene : multigenes.getMultigenes()) {
                if (strains.contains(multigene.getStrain().getName())) {
                    is_anchor = Boolean.FALSE;
                    return false;
                } else {
                    strains.add(multigene.getStrain().getName());
                }
            }
            is_anchor = Boolean.TRUE;
            return true;
        }
    }

    public boolean isInAllStrains() {
        if (in_all_strains != null) {
            return in_all_strains;
        } else {
            DataHandle dh = DataHandle.getDefault();
            Set<String> strains = new TreeSet<String>();
            for (Multigene multigene : multigenes.getMultigenes()) {
                if (!strains.contains(multigene.getStrain().getName())) {
                    strains.add(multigene.getStrain().getName());
                }
            }

            if (strains.size() == dh.getData().getStrainKeys().size()) {
                in_all_strains = Boolean.TRUE;
                return true;
            } else {
                in_all_strains = Boolean.FALSE;
                return false;
            }

        }
    }

    public MultigeneCluster(String cluster_id) {
        this.cluster_id = cluster_id;
        this.is_anchor = null;
        this.in_all_strains = null;
    }

    public String getCluster_id() {
        return cluster_id;
    }

    public MultigeneCollection getMultigenes() {
        return multigenes;
    }

    public void addMultigene(Multigene mult) {
        multigenes.addMultigene(mult);
    }

    public void counting() {
        //DataHandle dh = DataHandle.getDefault();
        Set<String> strains = new TreeSet<String>();
        Set<String> ann_strains = new TreeSet<String>();
        for (Multigene multigene : multigenes.getMultigenes()) {
            if (multigene.getTISs().size() > max_tis) {
                max_tis = multigene.getTISs().size();
            }
            if (multigene.getLength() > max_length) {
                max_length = multigene.getLength();
            }
            if (!strains.contains(multigene.getStrain().getName())) {
                strains.add(multigene.getStrain().getName());
            }
            if (!ann_strains.contains(multigene.getStrain().getName())) {
                if (!multigene.getGeneID().equals("x")) {
                    ann_strains.add(multigene.getStrain().getName());
                }
            }
        }
        number_of_strains = strains.size();
        number_of_ann_strains = ann_strains.size();
    }

    public Integer countStrains() {
        if (number_of_strains != 0) {
            return number_of_strains;
        } else {
            counting();
            return number_of_strains;
        }
    }

    public Integer getSize() {
        return multigenes.size();
    }

    public Integer countAnnotatedStrains() {
        if (number_of_ann_strains != 0) {
            return number_of_ann_strains;
        } else {
            counting();
            return number_of_ann_strains;
        }
    }

    public Integer maxMultigeneLength() {
        if (max_length != 0) {
            return max_length;
        } else {
            counting();
            return max_length;
        }
    }

    public Integer maxNumberOfTIS() {
        if (max_tis != 0) {
            return max_tis;
        } else {
            counting();
            return max_tis;
        }
    }

    private void computeOverlap() {
        for (Multigene multigene : multigenes.getMultigenes()) {
            Iterable<Multigene> overlaps = multigene.getContig().getAnnotation().getMultigenesInRange(multigene.getEnd() - 20000, multigene.getEnd() + 20000);
            for (Multigene over_mult : overlaps) {
                if (over_mult != multigene) {
                    Integer over_left = Math.max(multigene.getLeft_bound(), over_mult.getLeft_bound());
                    Integer over_right = Math.min(multigene.getRight_bound(), over_mult.getRight_bound());
                    Integer overlap_bp = over_right - over_left + 1;
                    Double overlap_tmp = Double.valueOf((double) overlap_bp / (double) multigene.getLength());
                    if (overlap_tmp > overlap) {
                        overlap = overlap_tmp;
                    }
                }
            }
        }
    }

    public void computeSizes() {
        main_size = Integer.valueOf(0);
        plasmids_size = Integer.valueOf(0);
        for (Multigene multigene : multigenes.getMultigenes()) {
            if (multigene.getContig().isMain()) {
                main_size += 1;
            } else {
                plasmids_size += 1;
            }
        }
    }

    private void computeUntypicalStart() {
        for (Multigene multigene : multigenes.getMultigenes()) {
            if (multigene.hasUntypicalStart()) {
                untypical_start = Boolean.TRUE;
            }
        }
    }

    public Integer getMainSize() {
        if (main_size != null) {
            return main_size;
        } else {
            computeSizes();
            return main_size;
        }
    }

    public Integer getPlasmidSize() {
        if (plasmids_size != null) {
            return plasmids_size;
        } else {
            main_size = Integer.valueOf(0);
            computeSizes();
            return main_size;
        }
    }

    public Double getOverlap() {
        if (overlap != null) {
            return overlap;

        } else {
            overlap = Double.valueOf(0.0);
            computeOverlap();
            return overlap;
        }
    }

    public String getUntypical() {
        if (untypical_start != null) {
            if (untypical_start) {
                return "Y";
            } else {
                return "N";
            }
        } else {
            untypical_start = Boolean.FALSE;
            computeUntypicalStart();
            if (untypical_start) {
                return "Y";
            } else {
                return "N";
            }
        }
    }
}
