/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package structs;

import java.util.HashSet;
import java.util.Set;

public class MultigeneCollection {

    private Set<Multigene> multigenes = new HashSet<Multigene>();

    public MultigeneCollection() {
        multigenes = new HashSet<Multigene>();
    }

    public Set<Multigene> getMultigenes() {
        return multigenes;
    }

    public void setMultigenes(Set<Multigene> multigenes) {
        this.multigenes = multigenes;
    }

    public void addMultigene(Multigene mult) {
        multigenes.add(mult);
    }

    public Integer size() {
        return multigenes.size();
    }

    public Set<Multigene> getMultigenesByStrain(String strain_id) {
        Set<Multigene> strain_mults = new HashSet<Multigene>();
        for (Multigene multigene : multigenes) {
            if (multigene.getStrain().getName().equals(strain_id)) {
                strain_mults.add(multigene);
            }
        }
        return strain_mults;
    }

    public Set<Multigene> getMultigenesByContig(String contig_id) {
        Set<Multigene> strain_mults = new HashSet<Multigene>();
        for (Multigene multigene : multigenes) {
            if (multigene.getContig().getName().equals(contig_id)) {
                strain_mults.add(multigene);
            }
        }
        return strain_mults;
    }
}
