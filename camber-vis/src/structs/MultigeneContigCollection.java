/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package structs;

import java.util.Collection;
import java.util.Map.Entry;
import java.util.TreeMap;

public class MultigeneContigCollection {

    //  private Set<Multigene> multigenes = new HashSet<Multigene>();
    private TreeMap<Integer, Multigene> tree = new TreeMap<Integer, Multigene>();

    public Collection<Multigene> getMultigenes() {
        return tree.values();
    }

    public Collection<Multigene> getMultigenesSorted() {
        //return tree.descendingMap().values();
        return tree.values();
    }

//    public void setMultigenes(Set<Multigene> multigenes) {
//        this.multigenes = multigenes;
    //   }
    public void addMultigene(Multigene mult) {
        //    multigenes.add(mult);
        Integer pos = mult.getRight_bound();
        tree.put(pos, mult);
    }

    public Iterable<Multigene> getMultigenesInRange(Integer range_left, Integer range_right) {
        Collection<Multigene> multigenes_range = tree.subMap(range_left, range_right).values();
        return multigenes_range;
    }

    public Integer countMultigenes() {
        return tree.size();
    }

    public Integer countAnnMultigenes() {
        Integer count = Integer.valueOf(0);
        for (Multigene multigene : tree.values()) {
            if (multigene.isAnnotated()) {
                count += 1;
            }
        }
        return count;
    }

    public Multigene nextMultigene(Multigene multigene) {
        Entry<Integer, Multigene> entry = tree.higherEntry(multigene.getEnd());
        if (entry == null) {
            return null;
        } else {
            return entry.getValue();
        }
    }

    public Multigene prevMultigene(Multigene multigene) {
        Entry<Integer, Multigene> entry = tree.lowerEntry(multigene.getRight_bound());
        if (entry == null) {
            return null;
        } else {
            return entry.getValue();
        }
    }

    public Multigene nextAnchorInAllStrainsMultigene(Multigene multigene) {

        Multigene mult = multigene;
        for (int i = 0; i < 100; i++) {
            Entry<Integer, Multigene> entry = tree.higherEntry(mult.getRight_bound());
            if (entry == null) {
                return null;
            } else {
                mult = entry.getValue();
                if (mult.getCluster().isAnchor() && mult.getCluster().isInAllStrains()) {
                    return mult;
                }
            }
        }
        return null;
    }

    public Multigene prevAnchorInAllStrainsMultigene(Multigene multigene) {

        Multigene mult = multigene;
        for (int i = 0; i < 100; i++) {
            Entry<Integer, Multigene> entry = tree.lowerEntry(mult.getEnd());
            if (entry == null) {
                return null;
            } else {
                mult = entry.getValue();
                if (mult.getCluster().isAnchor() && mult.getCluster().isInAllStrains()) {
                    return mult;
                }
            }
        }
        return null;
    }

    public Integer countLongMultigenes() {
        Integer count = Integer.valueOf(0);
        for (Multigene multigene : tree.values()) {
            if (multigene.sequence().length() > 150) {
                count += 1;
            }
        }

        return count;
    }

    public Integer countLongAnnMultigenes() {
        Integer count = Integer.valueOf(0);
        for (Multigene multigene : tree.values()) {
            if (multigene.sequence().length() > 150 && multigene.isAnnotated()) {
                count += 1;
            }
        }

        return count;
    }
}
