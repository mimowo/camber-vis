/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package structs;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import utils.Messenger;

public class MultigeneDataset {

    private Map<String, MultigeneCluster> clusters = new TreeMap<String, MultigeneCluster>();
    private Map<String, MultigeneSuperCluster> superClusters = new TreeMap<String, MultigeneSuperCluster>();
    private Map<String, Strain> strains = new TreeMap<String, Strain>();
    private Map<String, Contig> contigs = new TreeMap<String, Contig>();

    public MultigeneDataset() {
    }

    public void addCluster(String cluster_id) {

        addCluster(cluster_id, false);
    }

    public void addCluster(String cluster_id, boolean removed) {
        if (!clusters.containsKey(cluster_id)) {
            MultigeneCluster cluster = new MultigeneCluster(cluster_id);
            cluster.setRemoved(removed);
            clusters.put(cluster_id, cluster);
            String[] tokens = cluster_id.split("[-:]");

            if (!superClusters.containsKey(tokens[0])) {
                MultigeneSuperCluster superCluster = new MultigeneSuperCluster(tokens[0]);
                superCluster.addCluster(cluster);
                superClusters.put(tokens[0], superCluster);
            } else {
                MultigeneSuperCluster superCluster = superClusters.get(tokens[0]);
                superCluster.addCluster(cluster);
            }
        }
    }

    public void addStrain(String strain_id) {
        if (!strains.containsKey(strain_id)) {
            Strain strain = new Strain(strain_id);
            strains.put(strain_id, strain);
        }
    }

    public void addMultigene(Multigene multigene) {
        Contig contig = multigene.getContig();
        MultigeneContigCollection annotation = contig.getAnnotation();
        annotation.addMultigene(multigene);
        //System.out.println(contig.getName() + " " + contig.getStrainName() + " " + multigene.getGeneID());
    }

    public MultigeneCluster getCluster(String cluster_id) {
        return clusters.get(cluster_id);
    }

    public Collection<MultigeneCluster> getClusters() {
        return clusters.values();
    }

    public Collection<Strain> getStrains() {
        return strains.values();
    }

    public void setStrains(Map<String, Strain> strains) {
        this.strains = strains;
    }

    public Set<String> getStrainKeys() {
        return strains.keySet();
    }

    public Strain getStrain(String strain_id) {
        return strains.get(strain_id);
    }

    public Integer getStrainCount() {
        return strains.size();
    }

    public void addContig(String strain_id, String contig_id, String desc) {
        Strain strain = strains.get(strain_id);
        if (strain != null) {
            Contig contig = new Contig(strain, contig_id, desc);
            strain.addContig(contig);
            contigs.put(contig_id, contig);
        }
    }

    public void addContig(Contig contig) {
        Strain strain = strains.get(contig.getStrainName());
        strain.addContig(contig);
        contigs.put(contig.getName(), contig);
    }

    public Collection<Multigene> findMultigenesByID(String gene_id) {
        Set<Multigene> ret = new TreeSet<Multigene>();
        for (Strain strain : strains.values()) {
            for (Contig contig : strain.getContigs()) {
                Collection<Multigene> multigenes = contig.getAnnotation().getMultigenes();
                for (Multigene multigene : multigenes) {
                    if (multigene.getGeneID().equals(gene_id) || multigene.getGeneName().equals(gene_id)) {
                        ret.add(multigene);
                    }
                }
            }
        }
        return ret;
    }

    public boolean hasContig(String genome_id) {
        if (genome_id == null) {
            return false;
        }
        return contigs.containsKey(genome_id);
    }

    public Contig getContig(String genome_id) {
        if (genome_id == null) {
            return null;
        } else if (!hasContig(genome_id)) {
            return null;
        }
        return contigs.get(genome_id);
    }

    public Collection<Contig> getContigList() {
        return contigs.values();
    }

    public Map<String, Contig> getContigs() {
        return contigs;
    }
}
