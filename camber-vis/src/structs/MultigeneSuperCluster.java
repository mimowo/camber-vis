/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */

package structs;

import java.util.Map;
import java.util.TreeMap;

public class MultigeneSuperCluster {

    private String cluster_id;
    private Map<String, MultigeneCluster> clusters = new TreeMap<String, MultigeneCluster>();

    public MultigeneSuperCluster(String cluster_id) {
        this.cluster_id = cluster_id;
    }

    public String getCluster_id() {
        return cluster_id;
    }

    public void addCluster(MultigeneCluster cluster) {
        clusters.put(cluster.getCluster_id(), cluster);
    }

    public Map<String, MultigeneCluster> getClusters() {
        return clusters;
    }
}
