/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package structs;

public class SequenceMultigene extends Sequence {

    private String name;
    private Multigene multigene;
    private Integer tis;
    private Integer promotor;
    private boolean nuc;

    public SequenceMultigene(String id, String name, Multigene multigene, Integer tis, boolean nuc) {
        super(id);
        this.multigene = multigene;
        this.tis = tis;
        this.nuc = nuc;
        this.name = name;
    }

    public SequenceMultigene(String id, String name, Multigene multigene, Integer tis, Integer promotor, boolean nuc) {
        super(id);
        this.multigene = multigene;
        this.tis = tis;
        this.promotor = promotor;
        this.nuc = nuc;
        this.name = name;
    }

    public SequenceMultigene(String id, Multigene multigene, Integer tis, Integer promotor, boolean nuc) {
        super(id);
        this.multigene = multigene;
        this.tis = tis;
        this.promotor = promotor;
        this.nuc = nuc;
    }

    public SequenceMultigene(String id, Multigene multigene, Integer promotor, boolean nuc) {
        super(id);
        this.multigene = multigene;
        this.promotor = promotor;
        this.nuc = nuc;
    }

    public SequenceMultigene(String id, Multigene multigene, boolean nuc) {
        super(id);
        this.multigene = multigene;
        this.nuc = nuc;
    }

    public SequenceMultigene(String id, String name, Multigene multigene, boolean nuc) {
        super(id);
        this.name = name;
        this.multigene = multigene;
        this.nuc = nuc;
    }

    @Override
    public String sequence() {
        String seq = "";
        if (nuc) {
            if (tis == null || tis == 0) {
                seq = multigene.sequence();
            } else if (promotor == null || promotor == 0) {
                seq = multigene.sequence(tis);
            } else {
                seq = multigene.promotor_sequence(tis, promotor) + multigene.sequence(tis);
            }
        } else {
            if (tis == null || tis == 0) {
                seq = utils.SequenceUtils.translate(multigene.sequence());
            } else {
                seq = utils.SequenceUtils.translate(multigene.sequence(tis));
            }
        }
        return seq;
    }

    @Override
    public String type() {
        if (nuc) {
            return "DNA";
        } else {
            return "AA";
        }
    }

    @Override
    public String getName() {
        if (this.name == null) {
            return multigene.getMultigeneCCID();
        } else {
            return name;
        }
    }
}
