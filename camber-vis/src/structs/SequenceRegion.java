/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package structs;

import utils.SequenceUtils;

public class SequenceRegion extends Sequence {

    private String name;
    private Integer start;
    private Integer end;
    private String strand;
    private boolean rev;
    private boolean nuc;
    private Contig contig;

    public SequenceRegion(String id, String name, Contig contig, Integer start, Integer end, String strand, boolean rev, boolean nuc) {
        super(id);
        this.name = name;
        this.contig = contig;
        this.start = start;
        this.end = end;
        this.strand = strand;
        this.rev = rev;
        this.nuc = nuc;
    }

    @Override
    public String sequence() {
        if (nuc) {
            return contig.getSubSequence(start, end, strand, rev);
        } else {
            return SequenceUtils.translate(contig.getSubSequence(start, end, strand, rev));
        }
    }

    @Override
    public String type() {
        if (nuc) {
            return "DNA";
        } else {
            return "AA";
        }
    }

    @Override
    public String getName() {
        return name;
    }
}
