/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package structs;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class Strain {

    private Map<String, Contig> contigs = new TreeMap<String, Contig>();
    private String name;
    private Contig main;

    public Strain(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Contig getMain() {
        return main;
    }

    public void setMain(Contig main) {
        this.main = main;
    }

    public void addContig(Contig contig) {
        contigs.put(contig.getName(), contig);
    }

    public Collection<Contig> getContigs() {
        return contigs.values();
    }

    public Collection<String> getContigNames() {
        return contigs.keySet();
    }

    public Contig getContig(String contig_id) {
        return contigs.get(contig_id);
    }

    public Contig getDefaultContig() {
        if (main != null) {
            return main;
        } else {
            Contig longest_contig = null;
            int longest = 0;
            for (Entry<String, Contig> entry : contigs.entrySet()) {
                if (entry.getValue().length() > longest) {
                    longest = entry.getValue().length();
                    longest_contig = entry.getValue();
                }
            }
            return longest_contig;
        }
    }

    public Integer length() {
        Integer length = Integer.valueOf(0);
        for (Contig contig : contigs.values()) {
            length += contig.length();
        }
        return length;
    }

    public Integer countMultigenes() {
        Integer count = Integer.valueOf(0);
        for (Contig contig : contigs.values()) {
            count += contig.getAnnotation().countMultigenes();
        }
        return count;
    }

    public Integer countLongMultigenes() {
        Integer count = Integer.valueOf(0);
        for (Contig contig : contigs.values()) {
            count += contig.getAnnotation().countLongMultigenes();
        }
        return count;
    }

    public Integer countAnnMultigenes() {
        Integer count = Integer.valueOf(0);
        for (Contig contig : contigs.values()) {
            count += contig.getAnnotation().countAnnMultigenes();
        }
        return count;
    }

    public Integer countLongAnnMultigenes() {
        Integer count = Integer.valueOf(0);
        for (Contig contig : contigs.values()) {
            count += contig.getAnnotation().countLongAnnMultigenes();
        }
        return count;
    }
}
