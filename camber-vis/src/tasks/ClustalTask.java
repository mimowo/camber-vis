/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package tasks;

import io.WriteGene;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JProgressBar;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.util.Cancellable;
import org.openide.util.RequestProcessor;
import org.openide.util.Task;
import org.openide.util.TaskListener;
import structs.ExtClustalWResult;
import structs.Multigene;
import io.ClustalWReader;
import java.io.File;
import java.util.Map.Entry;
import structs.ExtToolResult;
import structs.Sequence;
import utils.DataHandle;
import utils.Messenger;
import utils.SettingsHandle;
import vis.TasksTableTopComponent;

public class ClustalTask implements ActionListener {

    private final static RequestProcessor RP = DataHandle.getDefault().getRP();
    private final static Logger LOG = Logger.getLogger(ClustalTask.class.getName());
    private RequestProcessor.Task theTask = null;
    private String jobID;
    private ExtClustalWResult result;
    private JProgressBar progressBar;
    private JButton runButton;

    public ClustalTask(String jobID, ExtClustalWResult result) {
        this.result = result;
        this.jobID = jobID;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        final ProgressHandle ph = ProgressHandleFactory.createHandle("ClustalW2", new Cancellable() {

            @Override
            public boolean cancel() {
                return handleCancel();
            }
        });

        Runnable runnable = new Runnable() {

            @Override
            public void run() {

                ph.start(); //we must start the PH before we swith to determinate
                runButton.setEnabled(false);
                progressBar.setEnabled(true);
                progressBar.setIndeterminate(true);
                progressBar.setStringPainted(true);
                SettingsHandle sh = SettingsHandle.getDefault();
                String clustal_exe = sh.getClustalPath();
                String working_dir = sh.getTmpDirPath();

                ph.setDisplayName("Preparing Fasta files.");
                progressBar.setString("Preparing Fasta files.");

                if (!clustal_exe.equals("") && !working_dir.equals("")) {

                    String fasta_fn = working_dir + File.separator + jobID + ".fasta";
                    String output_fn = working_dir + File.separator + jobID + ".txt";

                    for (Entry<String, Sequence> entry : result.getSequences().entrySet()) {
                        String index = entry.getKey();
                        Sequence sequence = entry.getValue();
                        //Integer tis = result.getMult_tis_map().get(index);
                        String ann_id = index.toString() + "_" + sequence.getName();
                        String seq = sequence.sequence();
                        WriteGene.addGeneSequence(fasta_fn, seq, ann_id);
                    }

                    String[] params = new String[4];

                    params[0] = clustal_exe;
                    params[1] = "-INFILE=" + fasta_fn;
                    params[2] = "-OUTPUT=PHYLIP";
                    params[3] = "-OUTFILE=" + output_fn;

                    ph.setDisplayName("Multiple alignment");
                    progressBar.setString("Multiple alignment");
                    try {
                        Process p = Runtime.getRuntime().exec(params);

                        BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
                        String line;
                        while ((line = input.readLine()) != null) {
                            Messenger.getDefault().message(line, "ClustalW output");
                        }

                        input.close();
                    } catch (IOException ex) {
                        result.setStatus(ExtToolResult.STATUS_ERROR);
                        Messenger.getDefault().showError(ex.getMessage());
                    }
                    ph.setDisplayName("Output reading.");
                    progressBar.setString("Output reading.");
                    // Messenger.getDefault().showMessage("before read");
                    ClustalWReader reader = new ClustalWReader(result, output_fn);
                    reader.read();
                    //  Messenger.getDefault().showMessage("after read");
                }
            }
        };

        theTask = RP.create(runnable); //the task is not started yet

        theTask.addTaskListener(new TaskListener() {

            @Override
            public void taskFinished(Task task) {
                result.setStatus(ExtToolResult.STATUS_OK);
                TasksTableTopComponent.getDefault().updateStatus(result);
                if (progressBar != null) {
                    progressBar.setIndeterminate(false);
                    progressBar.setEnabled(false);
                }
                if (runButton != null) {
                    runButton.setEnabled(true);
                }
                ph.finish();
            }
        });

        theTask.schedule(0); //start the task


    }

    private boolean handleCancel() {
        LOG.info("handleCancel");
        if (null == theTask) {
            return false;
        }

        return theTask.cancel();
    }

    public void setProgressBar(JProgressBar jProgressBar1) {
        this.progressBar = jProgressBar1;
    }

    public void setRunButton(JButton jButton1) {
        this.runButton = jButton1;
    }
}
