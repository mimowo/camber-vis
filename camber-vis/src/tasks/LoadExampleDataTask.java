/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package tasks;

import actions.menu.DrawSceneAction;
import io.AnnotationReader;
import io.SequencesReader;
import io.StrainsReader;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.util.Cancellable;
import org.openide.util.RequestProcessor;
import utils.DataHandle;
import org.openide.util.Task;
import org.openide.util.TaskListener;
import utils.Messenger;
import vis.ClustersStatsTopComponent;

public class LoadExampleDataTask implements ActionListener {//extends Task {

    private final static Logger LOG = Logger.getLogger(LoadExampleDataTask.class.getName());
    private NetbeansProgressListener listener = null;
    private RequestProcessor.Task theTask = null;
    private String strains_fn;
    private String clusters_fn;
    private String dirname;

    public LoadExampleDataTask(String strains_fn, String clusters_fn, String dirname) {
        this.strains_fn = strains_fn;
        this.clusters_fn = clusters_fn;
        this.dirname = dirname;
    }

    public void loadExampleData() {
        final ProgressHandle ph = ProgressHandleFactory.createHandle("Load input data", new Cancellable() {
            @Override
            public boolean cancel() {
                return handleCancel();
            }

            private boolean handleCancel() {
                LOG.info("handleCancel");
                if (null == theTask) {
                    return false;
                }
                return theTask.cancel();
            }
        });

        listener.setProgressHandle(ph);

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                listener.start();

                StrainsReader strains_reader = new StrainsReader(strains_fn);
                strains_reader.setProgressListener(listener);
                strains_reader.readStrains();
                SequencesReader seq_reader = new SequencesReader(dirname);
                seq_reader.setProgressListener(listener);
                seq_reader.loadGenomeSequences();
                AnnotationReader clust_reader = new AnnotationReader(clusters_fn);
                clust_reader.setProgressListener(listener);
                clust_reader.readAnnotations();
                ClustersStatsTopComponent.getDefault().loadTable();
                DrawSceneAction.drawScene();
            }
        };
        RequestProcessor RP = DataHandle.getDefault().getRP();
        theTask = RP.create(runnable); //the task is not started yet
        theTask.addTaskListener(new TaskListener() {
            @Override
            public void taskFinished(Task task) {
                listener.finish();
            }
        });
        theTask.schedule(0);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        loadExampleData();
    }

    public void setProgressListener(final NetbeansProgressListener listener) {
        this.listener = listener;
    }
}
