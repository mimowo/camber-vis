/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package tasks;

import io.FileReaderWriter;
import io.NCBIAPIReader;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JProgressBar;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.util.Cancellable;
import org.openide.util.Exceptions;
import org.openide.util.RequestProcessor;
import org.openide.util.Task;
import org.openide.util.TaskListener;
import structs.ExtNCBIAPIResult;
import structs.ExtToolResult;
import utils.DataHandle;
import utils.Messenger;
import utils.SettingsHandle;
import vis.TasksTableTopComponent;

public class NCBIAPITask implements ActionListener {

    private final static RequestProcessor RP = DataHandle.getDefault().getRP();
    private final static Logger LOG = Logger.getLogger(NCBIAPITask.class.getName());
    private RequestProcessor.Task theTask = null;
    private JProgressBar progressBar;
    private JButton runButton;
    private ExtNCBIAPIResult result;
    private String qt;
    private String rid;
    private Integer rtoe;
    private String output = "";

    public NCBIAPITask(ExtNCBIAPIResult result) {
        this.result = result;
    }

    public JProgressBar getProgressBar() {
        return progressBar;
    }

    public void setProgressBar(JProgressBar progressBar) {
        this.progressBar = progressBar;
    }

    public JButton getRunButton() {
        return runButton;
    }

    public void setRunButton(JButton runButton) {
        this.runButton = runButton;
    }

    public void runNCBItask() throws MalformedURLException, IOException, UnknownHostException {
        String urlString = result.createPutURL();

        Messenger.getDefault().message(urlString, "Put query to NCBI");

        URL url = new URL(urlString);
        BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

        String msg = "";
        String inputLine;

        while ((inputLine = in.readLine()) != null) {
            String[] tokens = inputLine.split("\\s+");
            if (tokens.length == 4) {
                if (tokens[1].equals("RID")) {
                    this.rid = tokens[3];
                    result.setRid(rid);
                }
                if (tokens[1].equals("RTOE")) {
                    this.rtoe = Integer.valueOf(tokens[3]);
                    result.setRtoe(rtoe);
                }
            }
            msg += inputLine + "\n";
        }

        in.close();
        if (rid == null) {
            Messenger.getDefault().message("No RID defined.", "NCBI BLAST API");
        }
    }

    public String checkNCBItask() throws MalformedURLException, IOException {
        output = "";
        if (result.getRid() == null) {
            Messenger.getDefault().showMessage("Cannot get job ID from BLAST NCBI, check your internet connection.\nPossibly it is a temporal problem on the server site.");
            return "ËRROR";
        }
        String urlString = result.createGetURL();
        //   String urlLines = StringUtils.wrapText(urlString, 100);
        Messenger.getDefault().message(urlString, "Get query to NCBI");

        URL url = new URL(urlString);

        BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

        String inputLine;
        String status = "X";
        String newline = System.getProperty("line.separator");

        while ((inputLine = in.readLine()) != null) {
            String[] tokens = inputLine.split("[=]");
            if (!status.equals("")) {
                output += inputLine;
                output += newline;
            }
            if (tokens.length == 2) {
                if (tokens[0].contains("Status")) {
                    if (tokens[1].equals("WAITING")) {
                        status = "WAITING";
                    } else if (tokens[1].equals("READY")) {
                        status = "READY";
                    } else {
                        status = "OTHER";
                    }
                }
            }
        }

        in.close();
        return status;
    }

    public String getQt() {
        return qt;
    }

    public String getRid() {
        return rid;
    }

    public Integer getRtoe() {
        return rtoe;
    }

    public void decRtoe() {
        rtoe--;
    }

    public void setRtoe(Integer rtoe) {
        this.rtoe = rtoe;
    }

    public String getOutput() {
        return output;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        progressBar.setIndeterminate(true);
        progressBar.setEnabled(true);
        progressBar.setStringPainted(true);
        final ProgressHandle ph = ProgressHandleFactory.createHandle("NCBI BLAST API", new Cancellable() {

            @Override
            public boolean cancel() {
                return handleCancel();
            }
        });

        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                ph.start(); //we must start the PH before we swith to determinate
                runButton.setEnabled(false);
                progressBar.setEnabled(true);
                progressBar.setIndeterminate(true);
                progressBar.setStringPainted(true);
                ph.switchToIndeterminate();
                ph.setDisplayName("Status: Sending the request.");
                progressBar.setString("Status: Sending the request.");
                try {
                    //waitLoop();
                    runNCBItask();
                } catch (MalformedURLException ex) {
                    result.setStatus(ExtToolResult.STATUS_ERROR);
                    Exceptions.printStackTrace(ex);
                } catch (IOException ex) {
                    result.setStatus(ExtToolResult.STATUS_ERROR);
                    Exceptions.printStackTrace(ex);
                }
                ph.setDisplayName("Status: Waiting for results.");
                progressBar.setString("Status: Waiting for results.");
                String status = "WAITING";
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Exceptions.printStackTrace(ex);
                }

                try {
                    while (status.equals("WAITING")) {
                        status = checkNCBItask();
                        if (status.equals("WAITING")) {
                            setRtoe(4);
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException ex) {
                                Exceptions.printStackTrace(ex);
                            }
                        } else if (status.equals("READY")) {
                            progressBar.setString("Status: " + status);
                            String output = getOutput();
                            Messenger.getDefault().message(output, "NCBI API");
                            String output_fn = SettingsHandle.getDefault().getTmpDirPath() + File.separator + result.getJobID() + ".txt";

                            FileReaderWriter.writeToFile(output_fn, output);
                            NCBIAPIReader reader = new NCBIAPIReader(result, output_fn);
                            reader.read();
                        } else {
                            progressBar.setString("Status: " + status);
                            result.setStatus(ExtToolResult.STATUS_ERROR);
                            String output = getOutput();
                            Messenger.getDefault().message(output, "NCBI API Error msg");
                        }
                        // String output = getOutput();

                    }
                } catch (MalformedURLException ex) {
                    result.setStatus(ExtToolResult.STATUS_ERROR);
                    Exceptions.printStackTrace(ex);
                } catch (IOException ex) {
                    result.setStatus(ExtToolResult.STATUS_ERROR);
                    Exceptions.printStackTrace(ex);
                }
            }
        };
        theTask = RP.create(runnable); //the task is not started yet

        theTask.addTaskListener(new TaskListener() {

            @Override
            public void taskFinished(Task task) {
                result.setStatus(ExtToolResult.STATUS_OK);
                TasksTableTopComponent.getDefault().updateStatus(result);
                progressBar.setIndeterminate(false);
                progressBar.setEnabled(false);
                runButton.setEnabled(true);
                ph.finish();
            }
        });

        theTask.schedule(0); //start the task
    }

    private boolean handleCancel() {
        LOG.info("handleCancel");
        if (null == theTask) {
            return false;
        }

        return theTask.cancel();
    }
}
