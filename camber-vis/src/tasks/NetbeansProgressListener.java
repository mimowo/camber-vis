/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */

package tasks;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JProgressBar;
import org.netbeans.api.progress.ProgressHandle;

public class NetbeansProgressListener implements ActionListener {

    private ProgressHandle ph = null;
    private JProgressBar pb = null;

    public NetbeansProgressListener() {
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }

    public void finish() {
        if (ph != null) {
            ph.finish();
        }
        if (pb != null) {
            pb.setString("");
            pb.setIndeterminate(false);
            pb.setVisible(false);
        }
    }

    public void progress(String msg) {
        if (ph != null) {
            ph.progress(msg);
        }
        if (pb != null) {
            pb.setString(msg);
            pb.setStringPainted(true);
        }
    }

    public void progress(int workunit) {
        if (ph != null) {
            ph.progress(workunit);
        }
        if (pb != null) {
            pb.setValue(workunit);
        }
    }

    public void start() {
        if (ph != null) {
            ph.start();
        }
        if (pb != null) {
            pb.setVisible(true);
            pb.setStringPainted(true);
        }
    }

    public void start(String newDisplayName) {
        if (ph != null) {
            ph.setDisplayName(newDisplayName);
        }
        if (pb != null) {
            pb.setVisible(true);
            pb.setString(newDisplayName);
            pb.setStringPainted(true);
        }
    }

    public void switchToIndeterminate() {
        if (ph != null) {
            ph.switchToIndeterminate();
        }
        if (pb != null) {
            pb.setIndeterminate(true);
        }
    }

    public void switchToDeterminate(int workunit) {
        if (ph != null) {
            ph.switchToDeterminate(workunit);
        }
        if (pb != null) {
            pb.setIndeterminate(false);
            pb.setMaximum(workunit);
        }
    }

    void setProgressHandle(ProgressHandle ph) {
        this.ph = ph;

    }

    public void setProgressBar(JProgressBar pb) {
        this.pb = pb;
    }
}
