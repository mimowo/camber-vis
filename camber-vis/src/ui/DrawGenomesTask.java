/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.logging.Logger;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.util.Cancellable;
import org.openide.util.Exceptions;
import org.openide.util.RequestProcessor;
import utils.DataHandle;
import org.openide.util.Task;
import org.openide.util.TaskListener;

public class DrawGenomesTask implements ActionListener {

    private final static Logger LOG = Logger.getLogger(DrawGenomesTask.class.getName());
    private Integer delay;
    private RequestProcessor.Task theTask = null;

    public DrawGenomesTask(Integer delay) {
        this.delay = delay;
    }

    public void drawGenomeBrowsers() throws InterruptedException {
        final ProgressHandle ph = ProgressHandleFactory.createHandle("Load input data", new Cancellable() {

            @Override
            public boolean cancel() {
                return handleCancel();
            }

            private boolean handleCancel() {
                LOG.info("handleCancel");
                if (null == theTask) {
                    return false;
                }
                return theTask.cancel();
            }
        });
        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                ph.start();
                Map<String, GenomeBrowser> browsers = VisualizationManager.getDefault().getGenomeBrowsers();
                DataHandle dh = DataHandle.getDefault();

                for (String strain_id : dh.getData().getStrainKeys()) {
                    GenomeBrowser browser = browsers.get(strain_id);
                    browser.drawPanel();
                }
            }
        };
        RequestProcessor RP = DataHandle.getDefault().getRP();
        theTask = RP.create(runnable); //the task is not started yet
        theTask.addTaskListener(new TaskListener() {

            @Override
            public void taskFinished(Task task) {
                ph.finish();
            }
        });
        theTask.schedule(delay);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("prepare")) {
            prepareGenomeBrowsers();
        } else if (e.getActionCommand().equals("draw")) {
            try {
                drawGenomeBrowsers();
            } catch (InterruptedException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
    }

    private void prepareGenomeBrowsers() {
        final ProgressHandle ph = ProgressHandleFactory.createHandle("Load input data", new Cancellable() {

            @Override
            public boolean cancel() {
                return handleCancel();
            }

            private boolean handleCancel() {
                LOG.info("handleCancel");
                if (null == theTask) {
                    return false;
                }
                return theTask.cancel();
            }
        });
        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                ph.start();
                VisualizationManager vis = VisualizationManager.getDefault();
                vis.createBrowserPanels();
                vis.showBrowserPanels();
            }
        };
        RequestProcessor RP = DataHandle.getDefault().getRP();
        theTask = RP.create(runnable); //the task is not started yet
        theTask.addTaskListener(new TaskListener() {

            @Override
            public void taskFinished(Task task) {
                ph.finish();
            }
        });
        theTask.schedule(delay);
    }
}
