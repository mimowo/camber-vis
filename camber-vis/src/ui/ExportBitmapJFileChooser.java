/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */

package ui;

import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import org.openide.util.ImageUtilities;
import utils.Messenger;

public class ExportBitmapJFileChooser extends JFileChooser {

    public ExportBitmapJFileChooser(String title) {
        super();
        Messenger.getDefault().showMessage("run");
        Image icon_image = ImageUtilities.loadImage("icons/load.png");
        if(icon_image==null) {
            Messenger.getDefault().showMessage("aaa");
        }
        UIManager.put("FileView.fileIcon", new ImageIcon(icon_image));
        UIManager.put("FileView.directoryIcon", new ImageIcon(icon_image));
        UIManager.put("FileChooser.homeFolderIcon", new ImageIcon(icon_image));
        UIManager.put("FileView.computerIcon", new ImageIcon(icon_image));
        UIManager.put("FileChooser.detailsViewIcon", new ImageIcon(icon_image));
        UIManager.put("FileChooser.cancelButtonToolTipText","aaaa");
        SwingUtilities.updateComponentTreeUI(this);
    }
}
