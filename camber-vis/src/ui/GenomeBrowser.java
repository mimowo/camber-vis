/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package ui;

import actions.GeneMouseAction;
import actions.MultigenePopup;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import javax.swing.JComponent;
import org.netbeans.api.visual.action.ActionFactory;
import org.netbeans.api.visual.action.WidgetAction;
import org.netbeans.api.visual.export.SceneExporter;
import org.netbeans.api.visual.widget.LayerWidget;
import structs.Contig;
import structs.Multigene;
import structs.MultigeneCluster;
import structs.MultigeneContigCollection;
import structs.Strain;
import utils.Messenger;
import utils.SettingsHandle;
import vis.GraphSceneImpl;
import widgets.GeneWidget;
import widgets.MultigeneRangeWidget;
import widgets.RangeWidget;
import widgets.RulerWidget;
import widgets.SplitLineWidget;

public final class GenomeBrowser extends javax.swing.JPanel {

    private static class BrowserConfig {

        private Integer range_left;
        private Integer range_right;
        private Boolean flip;
        private GenomeListObject gen_list_obj;

        public BrowserConfig(GenomeListObject glo, Integer r_right) {
            if (r_right < 100000) {
                this.range_right = r_right;
            } else {
                this.range_right = 100000;
            }
            this.range_left = 0;
            this.flip = Boolean.FALSE;
            this.gen_list_obj = glo;
        }

        public GenomeListObject getGen_list_obj() {
            return gen_list_obj;
        }

        public Boolean getFlip() {
            return flip;
        }

        public void setFlip(Boolean flip) {
            this.flip = flip;
        }

        public Integer getRange_left() {
            return range_left;
        }

        public void setRange_left(Integer range_left) {
            this.range_left = range_left;
        }

        public Integer getRange_right() {
            return range_right;
        }

        public void setRange_right(Integer range_right) {
            this.range_right = range_right;
        }
    }
    private Integer range_left = 0;
    private Integer range_right = 100000;
    private Boolean flip = Boolean.FALSE;
    private Integer window_left = 0;
    private Integer window_right = 1000;
    private Integer shift = 0;
    private Strain strain;
    private Contig contig;
    Integer range_window_height = 20;
    private Integer range_window_right = 100;
    private GraphSceneImpl range_scene;// = scene = new GraphSceneImpl();
    private JComponent range_myView;
    private LayerWidget range_mainLayer;// = new LayerWidget(scene);
    private JComponent myView;
    private GraphSceneImpl scene;// = scene = new GraphSceneImpl();
    private LayerWidget mainLayer;// = new LayerWidget(scene);
    private LayerWidget rulerLayer;// = new LayerWidget(scene);
    private LayerWidget splitLayer;// = new LayerWidget(scene);
    private Map<String, BrowserConfig> configs = new TreeMap<String, BrowserConfig>();
    private boolean initialized;
    private Multigene mult_sel;
    private Set<Integer> positions = new TreeSet<Integer>();

    /**
     * Creates new form GenomeBrowser
     */
    public GenomeBrowser(Strain strain) {
        this.strain = strain;
        this.contig = strain.getDefaultContig();
        this.initialized = false;

        initComponents();
        initBrowserConfigs();
        initGenomeBrowser();

        //initSequencesList();
        //jCheckBox1.setText(strain.getName());
        jCheckBox1.setText("");
        //this.genome.length() = strain.length();

    }

    public void refreshRangeLabel() {
        Integer lr = Math.min(Math.max(range_left, 0), contig.length());
        Integer rr = Math.min(Math.max(range_right, 0), contig.length());

        //String text = "Range: (" + String.valueOf(lr) + "," + String.valueOf(rr) + ")";
        //String text = "Range: (" + String.valueOf(lr) + "," + String.valueOf(rr) + ")";
        //this.rangeLabel.setText(text);
        this.rangeLeftInput.setText(lr.toString());
        this.rangeRightInput.setText(rr.toString());
    }

    public void setWidth() {
        paneScene.updateUI();
        if (paneScene.getWidth() == 0) {
            //  this.window_right = paneScene.getParent().getParent().getParent().getWidth();
        } else {
            this.window_right = paneScene.getWidth();
        }
    }

    public void hideDistances() {
        rulerLayer.removeChildren();

        this.paneScene.setViewportView(myView);
        this.scene.createSatelliteView();
    }

    public void drawDistances() {
        PriorityQueue<Integer> pos_sorted = new PriorityQueue<Integer>();
        for (Integer pos : this.positions) {
            pos_sorted.add(pos);
        }
        rulerLayer.removeChildren();
        RulerWidget ruler = new RulerWidget(scene, pos_sorted, this.range_left, this.range_right, this.window_right);
        rulerLayer.addChild(ruler); // add the icon node into scene
        this.paneScene.setViewportView(myView);
        this.scene.createSatelliteView();
    }

    public void drawSplitLine() {
        splitLayer.removeChildren();
        SplitLineWidget line = new SplitLineWidget(scene, this.window_right, 26);
        splitLayer.addChild(line); // add the icon node into scene
    }

    public String multigeneTooptip(Multigene multigene) {
        MultigeneCluster cluster = multigene.getCluster();
        String tooltip = "<html><b>Gene id:</b> " + multigene.getGeneID();
        tooltip += "<br/>";
        tooltip += "<b>Strand:</b> " + multigene.getStrand();
        tooltip += "<br/>";
        tooltip += "<b>Locus:</b> (" + multigene.getStart().toString() + "," + multigene.getEnd().toString() + ")";
        tooltip += "<br/>";
        tooltip += "<b>TISs: </b>";
        for (Integer length_gene : multigene.getTISs()) {
            if (multigene.getAnnTIS() == length_gene) {
                tooltip += "<b>" + length_gene.toString() + "*</b>" + ",";
            } else {
                tooltip += length_gene.toString() + ",";
            }
        }
        tooltip += "<br/>";
        tooltip += "<b>Conn comp id:</b> " + cluster.getCluster_id();
        tooltip += "<br/>";
        tooltip += "The conn. comp. contains " + cluster.getSize().toString() + " multigenes in " + cluster.countStrains().toString() + " strains,";
        tooltip += "<br/>";
        tooltip += cluster.countAnnotatedStrains().toString() + " strains ";
        tooltip += "contain at least one annotated multigene.";
        tooltip += "</html>";
        return tooltip;
    }

    public void drawGenome() {
        SettingsHandle sh = SettingsHandle.getDefault();
        mainLayer.removeChildren();
        //mainLayer.getActions().addAction(new GenomeMouseMoveAction(this));

        this.positions = new TreeSet<Integer>();
        setWidth();
        MultigeneContigCollection multigenes = contig.getAnnotation();

        if (mult_sel != null) {
            if (flip) {
                this.shift = 3 - flipCoordinate(mult_sel.getEnd()) % 3;
            } else {
                this.shift = 3 - mult_sel.getEnd() % 3;
            }
        }

        Double a = Double.valueOf(window_right.doubleValue() / (range_right.doubleValue() - range_left.doubleValue()));
        Double b = -a * Double.valueOf(range_left.doubleValue());

        for (Multigene multigene : multigenes.getMultigenes()) {

            if (sh.getShowRemoved() == SettingsHandle.SHOW_REMOVED_N && multigene.isRemoved()) {
                continue;
            }
            Integer mult_right_bound;
            Integer mult_left_bound;
            if (flip == true) {
                mult_right_bound = flipCoordinate(multigene.getLeft_bound());
                mult_left_bound = flipCoordinate(multigene.getRight_bound());
            } else {
                mult_left_bound = multigene.getLeft_bound();
                mult_right_bound = multigene.getRight_bound();
            }

            if ((mult_left_bound <= range_right) && (mult_right_bound >= range_left)) {

                this.positions.add(mult_left_bound);
                this.positions.add(mult_right_bound);
                Integer ml = Math.max(0, Double.valueOf(a * mult_left_bound.doubleValue() + b).intValue());
                Integer mr = Math.min(window_right, Double.valueOf(a * mult_right_bound.doubleValue() + b).intValue());
                if (mr - ml < 1) {
                    continue;
                }

                GeneWidget gene = new GeneWidget(scene, multigene, mr - ml, 4, flip);

                if (multigene == mult_sel) {
                    gene.setSelected(true);
                }

                WidgetAction popup = ActionFactory.createPopupMenuAction(new MultigenePopup(gene, multigene));
                gene.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                gene.getActions().addAction(popup);
                gene.getActions().addAction(new GeneMouseAction());

                String tooltip = multigeneTooptip(multigene);
                gene.setToolTipText(tooltip);
                int center = (mr + ml) / 2;
                Integer h_pos = 6;
                if (multigene.getIntStrand() == 1 && flip == false) { //+
                    h_pos = 22 - ((multigene.getEnd() + shift) % 3) * 3;
                } else if (multigene.getIntStrand() != 1 && flip == true) { //+
                    h_pos = 22 - ((flipCoordinate(multigene.getEnd()) + shift) % 3) * 3;
                } else if (multigene.getIntStrand() == 1 && flip == true) { //-
                    h_pos = 33 + ((flipCoordinate(multigene.getEnd()) + shift) % 3) * 3;
                } else if (multigene.getIntStrand() != 1 && flip == false) { //-
                    h_pos = 33 + ((multigene.getEnd() + shift) % 3) * 3;
                }
                gene.setPreferredLocation(new Point(center, h_pos));
                mainLayer.addChild(gene);
            }
        }
    }

    public void refreshScene() {
        //    this.paneScene.setViewportView(myView);
        //this.scene.createSatelliteView();
        this.scene.validate();
    }

    public void drawPanel() {
        refreshScene();
        drawGenome();
        if (VisualizationManager.getDefault().getShow_distances()) {
            drawDistances();
        }
        drawSplitLine();
        refreshScene();
        drawRange();
        // paneScene.updateUI();
        refreshRangeLabel();
    }

    public GraphSceneImpl getScene() {
        return scene;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jToolBar1 = new javax.swing.JToolBar();
        jButton1 = new javax.swing.JButton();
        zoomIn = new javax.swing.JButton();
        zoomOut = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jCheckBox1 = new javax.swing.JCheckBox();
        jComboBox1 = new javax.swing.JComboBox();
        jToolBar4 = new javax.swing.JToolBar();
        rangeLeftLabel = new javax.swing.JLabel();
        rangeLeftInput = new javax.swing.JFormattedTextField();
        rangeRightLabel = new javax.swing.JLabel();
        rangeRightInput = new javax.swing.JFormattedTextField();
        jToolBar5 = new javax.swing.JToolBar();
        jScrollPane1 = new javax.swing.JScrollPane();
        jToolBar2 = new javax.swing.JToolBar();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        paneScene = new javax.swing.JScrollPane();

        setMaximumSize(new java.awt.Dimension(2147483647, 93));
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });
        setLayout(new java.awt.BorderLayout());

        jPanel1.setPreferredSize(new java.awt.Dimension(800, 23));

        jPanel2.setMaximumSize(new java.awt.Dimension(32767, 23));
        jPanel2.setPreferredSize(new java.awt.Dimension(800, 23));

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/arrow-left.png"))); // NOI18N
        jButton1.setToolTipText(org.openide.util.NbBundle.getMessage(GenomeBrowser.class, "GenomeBrowser.jButton1.toolTipText")); // NOI18N
        jButton1.setBorder(null);
        jButton1.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton1);

        zoomIn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/zoom-in.png"))); // NOI18N
        zoomIn.setToolTipText(org.openide.util.NbBundle.getMessage(GenomeBrowser.class, "GenomeBrowser.zoomIn.toolTipText")); // NOI18N
        zoomIn.setBorder(null);
        zoomIn.setIconTextGap(0);
        zoomIn.setMargin(new java.awt.Insets(0, 0, 0, 0));
        zoomIn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoomInActionPerformed(evt);
            }
        });
        jToolBar1.add(zoomIn);

        zoomOut.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/zoom-out.png"))); // NOI18N
        zoomOut.setToolTipText(org.openide.util.NbBundle.getMessage(GenomeBrowser.class, "GenomeBrowser.zoomOut.toolTipText")); // NOI18N
        zoomOut.setMargin(new java.awt.Insets(0, 0, 0, 0));
        zoomOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoomOutActionPerformed(evt);
            }
        });
        jToolBar1.add(zoomOut);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/arrow-right.png"))); // NOI18N
        jButton2.setToolTipText(org.openide.util.NbBundle.getMessage(GenomeBrowser.class, "GenomeBrowser.jButton2.toolTipText")); // NOI18N
        jButton2.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton2);

        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/flip.png"))); // NOI18N
        jButton6.setToolTipText(org.openide.util.NbBundle.getMessage(GenomeBrowser.class, "GenomeBrowser.jButton6.toolTipText")); // NOI18N
        jButton6.setFocusable(false);
        jButton6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton6.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton6);

        jCheckBox1.setText(org.openide.util.NbBundle.getMessage(GenomeBrowser.class, "GenomeBrowser.jCheckBox1.text")); // NOI18N
        jCheckBox1.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jCheckBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox1ActionPerformed(evt);
            }
        });
        jToolBar1.add(jCheckBox1);

        jComboBox1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox1ItemStateChanged(evt);
            }
        });
        jToolBar1.add(jComboBox1);

        jToolBar4.setFloatable(false);
        jToolBar4.setToolTipText(org.openide.util.NbBundle.getMessage(GenomeBrowser.class, "GenomeBrowser.jToolBar4.toolTipText")); // NOI18N
        jToolBar4.setPreferredSize(new java.awt.Dimension(160, 23));

        rangeLeftLabel.setText(org.openide.util.NbBundle.getMessage(GenomeBrowser.class, "GenomeBrowser.rangeLeftLabel.text")); // NOI18N
        jToolBar4.add(rangeLeftLabel);

        rangeLeftInput.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        rangeLeftInput.setText(org.openide.util.NbBundle.getMessage(GenomeBrowser.class, "GenomeBrowser.rangeLeftInput.text")); // NOI18N
        rangeLeftInput.setToolTipText(org.openide.util.NbBundle.getMessage(GenomeBrowser.class, "GenomeBrowser.rangeLeftInput.toolTipText")); // NOI18N
        rangeLeftInput.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rangeLeftInputMouseClicked(evt);
            }
        });
        rangeLeftInput.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                rangeLeftInputFocusLost(evt);
            }
        });
        rangeLeftInput.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                rangeLeftInputKeyPressed(evt);
            }
        });
        jToolBar4.add(rangeLeftInput);

        rangeRightLabel.setText(org.openide.util.NbBundle.getMessage(GenomeBrowser.class, "GenomeBrowser.rangeRightLabel.text")); // NOI18N
        jToolBar4.add(rangeRightLabel);

        rangeRightInput.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        rangeRightInput.setText(org.openide.util.NbBundle.getMessage(GenomeBrowser.class, "GenomeBrowser.rangeRightInput.text")); // NOI18N
        rangeRightInput.setToolTipText(org.openide.util.NbBundle.getMessage(GenomeBrowser.class, "GenomeBrowser.rangeRightInput.toolTipText")); // NOI18N
        rangeRightInput.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rangeRightInputMouseClicked(evt);
            }
        });
        rangeRightInput.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                rangeRightInputFocusLost(evt);
            }
        });
        rangeRightInput.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                rangeRightInputKeyPressed(evt);
            }
        });
        jToolBar4.add(rangeRightInput);

        jToolBar5.setFloatable(false);
        jToolBar5.setRollover(true);

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane1.setToolTipText(org.openide.util.NbBundle.getMessage(GenomeBrowser.class, "GenomeBrowser.jScrollPane1.toolTipText")); // NOI18N
        jScrollPane1.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        jScrollPane1.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                jScrollPane1ComponentResized(evt);
            }
            public void componentShown(java.awt.event.ComponentEvent evt) {
                shown(evt);
            }
        });
        jToolBar5.add(jScrollPane1);

        jToolBar2.setFloatable(false);
        jToolBar2.setRollover(true);

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/up.png"))); // NOI18N
        jButton4.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jToolBar2.add(jButton4);

        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/down.png"))); // NOI18N
        jButton5.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jToolBar2.add(jButton5);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jToolBar4, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jToolBar5, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                .addComponent(jToolBar2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, 0, 0, Short.MAX_VALUE)
            .addComponent(jToolBar2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jToolBar4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jToolBar5, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 728, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        add(jPanel1, java.awt.BorderLayout.PAGE_START);

        jPanel3.setMaximumSize(new java.awt.Dimension(32767, 70));
        jPanel3.setPreferredSize(new java.awt.Dimension(800, 70));

        paneScene.setBorder(null);
        paneScene.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        paneScene.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        paneScene.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        paneScene.setHorizontalScrollBar(null);
        paneScene.setMaximumSize(new java.awt.Dimension(32767, 70));
        paneScene.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                paneSceneComponentResized(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(paneScene, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 728, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(paneScene, javax.swing.GroupLayout.DEFAULT_SIZE, 71, Short.MAX_VALUE)
        );

        add(jPanel3, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        shiftRight();
        drawPanel();
    }//GEN-LAST:event_jButton2ActionPerformed

    public void shiftRight() {
        Integer range_shift = (range_right - range_left) / 4;
        //range_left = Math.min(genome.length(), range_left + range_shift);
        //range_right = Math.min(genome.length(), range_right + range_shift);
        range_left = range_left + range_shift;
        range_right = range_right + range_shift;
    }

    public void shiftLeft() {
        Integer range_shift = (range_right - range_left) / 4;
        //range_left = Math.max(0, range_left - range_shift);
        //range_right = Math.max(0, range_right - range_shift);
        range_left = range_left - range_shift;
        range_right = range_right - range_shift;
    }

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        shiftLeft();
        drawPanel();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void zoomInActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zoomInActionPerformed
        zoomIn();
        drawPanel();
    }//GEN-LAST:event_zoomInActionPerformed

    public void zoomOut() {
        Double scale = 0.333;
        Integer length = range_right - range_left;
        Integer change = Double.valueOf(scale * length.doubleValue()).intValue();
        //range_left = Math.max(0, range_left - change);
        range_left = range_left - change;
        //range_right = Math.min(genome.length(), range_right + change);
        range_right = range_right + change;
    }

    public void flipGenome() {
        if (flip) {
            flip = false;
        } else {
            flip = true;
        }
        Integer right_tmp = flipCoordinate(this.range_left);
        Integer left_tmp = flipCoordinate(this.range_right);
        this.range_left = left_tmp;
        this.range_right = right_tmp;
    }

    public void zoomIn() {
        Integer length = range_right - range_left;
        if (length > 50) {
            Double scale = 0.2;

            Integer change = Double.valueOf(scale * length.doubleValue()).intValue();
            //range_left = Math.min(genome.length(), range_left + change);
            //range_right = Math.max(0, range_right - change);
            range_left = range_left + change;
            range_right = range_right - change;
        }
    }

    void changeContig(Contig n_contig) {
        if (this.contig != n_contig) {
            BrowserConfig curr_conf = configs.get(contig.getName());

            saveConfig(curr_conf);
            BrowserConfig new_conf = configs.get(n_contig.getName());
            if (new_conf == null) {
                Messenger.getDefault().showMessage("NEW: " + n_contig.getName() + " " + Integer.toString(configs.size()) + " " + Integer.toString(n_contig.getStrain().getContigs().size()));
                for (String c : configs.keySet()) {
                    Messenger.getDefault().showMessage("BROWSER: " + c);
                }
                for (Contig c : n_contig.getStrain().getContigs()) {
                    Messenger.getDefault().showMessage("STRAIN: " + c.getName());
                }

            }
            loadConfig(new_conf);

            jComboBox1.setSelectedItem(new_conf.getGen_list_obj());
        }
    }

    public Integer flipCoordinate(Integer p) {
        return this.contig.length() - 1 - p;
    }

    private void zoomOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zoomOutActionPerformed
        zoomOut();
        drawPanel();
    }//GEN-LAST:event_zoomOutActionPerformed

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            shiftRight();
        } else if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            shiftLeft();
        } else if (evt.getKeyCode() == KeyEvent.VK_UP) {
            zoomOut();
        } else if (evt.getKeyCode() == KeyEvent.VK_DOWN) {
            zoomIn();
        }

    }//GEN-LAST:event_formKeyPressed

    private void jCheckBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox1ActionPerformed
        if (jCheckBox1.isSelected() == false) {
            scene.setBackground(Color.LIGHT_GRAY);
            jButton1.setEnabled(false);
            jButton2.setEnabled(false);
            zoomIn.setEnabled(false);
            zoomOut.setEnabled(false);
            jPanel1.updateUI();
            paneScene.updateUI();
        }
        if (jCheckBox1.isSelected() == true) {
            jButton1.setEnabled(true);
            jButton2.setEnabled(true);
            zoomIn.setEnabled(true);
            zoomOut.setEnabled(true);
            scene.setBackground(Color.WHITE);
            jPanel1.updateUI();
            paneScene.updateUI();
        }
    }//GEN-LAST:event_jCheckBox1ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        VisualizationManager.getDefault().moveUp(strain.getName());
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        VisualizationManager.getDefault().moveDown(strain.getName());
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        flipGenome();
        drawPanel();
    }//GEN-LAST:event_jButton6ActionPerformed

    private void shown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_shown
    }//GEN-LAST:event_shown

    private void jScrollPane1ComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_jScrollPane1ComponentResized
        drawRange();
    }//GEN-LAST:event_jScrollPane1ComponentResized

    private void paneSceneComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_paneSceneComponentResized
        setWidth();
        drawPanel();
    }//GEN-LAST:event_paneSceneComponentResized

    private void jComboBox1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox1ItemStateChanged
        if (initialized) {
            BrowserConfig curr_conf = configs.get(contig.getName());
            saveConfig(curr_conf);

            GenomeListObject gen_obj = (GenomeListObject) jComboBox1.getSelectedItem();
            this.contig = gen_obj.getContig();
            BrowserConfig new_conf = configs.get(contig.getName());
            loadConfig(new_conf);
            drawPanel();
        }
    }//GEN-LAST:event_jComboBox1ItemStateChanged

    private void rangeRightInputMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rangeRightInputMouseClicked
        //  rangeRightInput.setEditable(true);
    }//GEN-LAST:event_rangeRightInputMouseClicked

    private void rangeLeftInputMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rangeLeftInputMouseClicked
        //  rangeLeftInput.setEditable(true);
    }//GEN-LAST:event_rangeLeftInputMouseClicked

    private void rangeRightInputKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rangeRightInputKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            try {
                range_left = Integer.valueOf(rangeLeftInput.getText());
                range_right = Integer.valueOf(rangeRightInput.getText());
            } catch (NumberFormatException e) {
                rangeLeftInput.setText(range_left.toString());
                rangeRightInput.setText(range_right.toString());
            }
            drawPanel();
        }
    }//GEN-LAST:event_rangeRightInputKeyPressed

    private void rangeLeftInputKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rangeLeftInputKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            try {
                range_left = Integer.valueOf(rangeLeftInput.getText());
                range_right = Integer.valueOf(rangeRightInput.getText());
            } catch (NumberFormatException e) {
                rangeLeftInput.setText(range_left.toString());
                rangeRightInput.setText(range_right.toString());
            }
            drawPanel();
        }
    }//GEN-LAST:event_rangeLeftInputKeyPressed

    private void rangeRightInputFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_rangeRightInputFocusLost
        //rangeRightInput.setEditable(false);
    }//GEN-LAST:event_rangeRightInputFocusLost

    private void rangeLeftInputFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_rangeLeftInputFocusLost
        //rangeLeftInput.setEditable(false);
    }//GEN-LAST:event_rangeLeftInputFocusLost
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JToolBar jToolBar2;
    private javax.swing.JToolBar jToolBar4;
    private javax.swing.JToolBar jToolBar5;
    private javax.swing.JScrollPane paneScene;
    private javax.swing.JFormattedTextField rangeLeftInput;
    private javax.swing.JLabel rangeLeftLabel;
    private javax.swing.JFormattedTextField rangeRightInput;
    private javax.swing.JLabel rangeRightLabel;
    private javax.swing.JButton zoomIn;
    private javax.swing.JButton zoomOut;
    // End of variables declaration//GEN-END:variables

    public void zoomRegion(Integer range_center, Integer length) {
        this.mult_sel = null;
        Integer length_half = length / 2;
        range_left = range_center - length_half - 1000;
        range_right = range_center + length_half + 1000;
    }

    public void zoomMultigene(Multigene multigene, Integer length) {
        this.mult_sel = multigene;
        Integer mult_right_bound;
        Integer mult_left_bound;
        if (flip == true) {
            mult_right_bound = flipCoordinate(multigene.getLeft_bound());
            mult_left_bound = flipCoordinate(multigene.getRight_bound());
        } else {
            mult_left_bound = multigene.getLeft_bound();
            mult_right_bound = multigene.getRight_bound();
        }

        Integer range_center = Math.abs(mult_right_bound + mult_left_bound) / 2;
        Integer length_half = length / 2;
        range_left = range_center - length_half - 1000;
        range_right = range_center + length_half + 1000;
    }

    public void zoomMultigene(Multigene multigene) {
        zoomMultigene(multigene, multigene.getLength());
    }

    private void findGene(String gene_term) {
        Multigene multigene = findMultigeneByTerm(gene_term);
        if (multigene != null) {
            zoomMultigene(multigene);
        }
    }

    private Multigene findMultigeneByTerm(String gene_term) {
        for (Contig contig : strain.getContigs()) {
            Collection<Multigene> multigenes = contig.getAnnotation().getMultigenes();
            for (Multigene multigene : multigenes) {
                if (multigene.getGeneID().equals(gene_term)) {
                    return multigene;
                }
            }
        }
        return null;
    }

    public boolean isActive() {
        return jCheckBox1.isSelected();
    }

    public BufferedImage getImage() throws IOException {
        int width = Double.valueOf(scene.getBounds().getWidth()).intValue();
        int height = Double.valueOf(scene.getBounds().getHeight()).intValue();
        try {
            BufferedImage image = SceneExporter.createImage(scene, null, SceneExporter.ImageType.PNG, SceneExporter.ZoomType.CUSTOM_SIZE, false, false, 50, width, height);
            return image;
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    public void RangeWidth() {
        if (jScrollPane1.getWidth() == 0) {
            //this.range_window_right = jScrollPane1.getParent().getWidth() - 4;
            this.range_window_right = jScrollPane1.getWidth() - 4;
        } else {
            this.range_window_right = jScrollPane1.getWidth() - 4;
        }
        range_window_right = jScrollPane1.getWidth();
        range_window_height = jScrollPane1.getHeight();
    }

    private void drawRange() {
        jScrollPane1.updateUI();
        RangeWidth();
        range_mainLayer.removeChildren();

        Double a = Double.valueOf(range_window_right.doubleValue() / (contig.length().doubleValue()));

        Integer ml = Double.valueOf(a * range_left.doubleValue()).intValue();
        Integer mr = Double.valueOf(a * range_right.doubleValue()).intValue();

        SplitLineWidget line = new SplitLineWidget(range_scene, this.range_window_right, range_window_height / 2);
        RangeWidget rangeWidget = new RangeWidget(range_scene, ml, mr, range_window_height, range_window_right - 3);
        range_mainLayer.addChild(line); 
        range_mainLayer.addChild(rangeWidget);
        
        

        if (mult_sel != null) {
            MultigeneCluster cluster = mult_sel.getCluster();
            for (Multigene multigene : cluster.getMultigenesByContig(contig.getName())) {
                Integer m_center = (multigene.getLeft_bound() + multigene.getRight_bound()) / 2;
                Integer m;
                if (flip) {
                    m = Double.valueOf(a * flipCoordinate(m_center).doubleValue()).intValue();
                } else {
                    m = Double.valueOf(a * m_center.doubleValue()).intValue();
                }


                MultigeneRangeWidget m_dot = new MultigeneRangeWidget(range_scene, m, multigene.getIntStrand(), range_window_height, flip);
                range_mainLayer.addChild(m_dot);
            }
        }

        this.jScrollPane1.setViewportView(range_myView);
        this.range_scene.createSatelliteView();
        this.range_scene.validate();
        jScrollPane1.updateUI();
    }

//    public void refreshRangeLabelWithCaret(Point mouse_pos) {
//        Integer caret_pos = getCaretPosition(mouse_pos.x);
//        String text = "Range: (" + String.valueOf(range_left) + "," + String.valueOf(range_right) + ")," + caret_pos.toString();
//        this.rangeLabel.setText(text);
//    }
    public Integer getCaretPosition(Integer point_x) {
        Integer range_width = range_right - range_left;
        //Double a = Double.valueOf((range_width / window_right.doubleValue()));
        Integer window_pos = (point_x * range_width / window_right) + range_left;
        return window_pos;
    }

    private void initGenomeBrowser() {
        this.jCheckBox1.setSelected(true);

        this.scene = new GraphSceneImpl();
        this.mainLayer = new LayerWidget(scene);
        this.rulerLayer = new LayerWidget(scene);
        this.splitLayer = new LayerWidget(scene);
        this.scene.addChild(splitLayer);
        this.scene.addChild(mainLayer);
        this.scene.addChild(rulerLayer);
        this.myView = scene.createView();
        this.paneScene.setViewportView(myView);

        this.range_scene = new GraphSceneImpl();
        this.range_mainLayer = new LayerWidget(range_scene);
        this.range_myView = range_scene.createView();
        this.range_scene.addChild(range_mainLayer);

        initialized = true;
    }

    private void initBrowserConfigs() {
        for (Contig contig_i : strain.getContigs()) {
            GenomeListObject plas_obj = new GenomeListObject(contig_i, contig_i.getDesc());
            jComboBox1.addItem(plas_obj);
            configs.put(contig_i.getName(), new BrowserConfig(plas_obj, contig_i.length()));
            if (contig_i.isMain()) {
                contig = contig_i;
                jComboBox1.setSelectedItem(plas_obj);
            }
        }
        jComboBox1.setSelectedItem(range_left);
    }

    private void saveConfig(BrowserConfig config) {
        config.setFlip(flip);
        config.setRange_left(range_left);
        config.setRange_right(range_right);
    }

    private void loadConfig(BrowserConfig config) {
        range_left = config.getRange_left();
        range_right = config.getRange_right();
        flip = config.getFlip();
    }

    public Contig getSelectedContig() {
        return contig;
    }
}
