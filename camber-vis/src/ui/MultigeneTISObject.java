/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */

package ui;

import structs.Multigene;


public class MultigeneTISObject {

    private Multigene multigene;
    private Integer tis;

    public MultigeneTISObject(Multigene multigene, Integer tis) {
        this.multigene = multigene;
        this.tis = tis;
    }

    @Override
    public String toString() {
        String seq = multigene.sequence(tis).substring(0, 3).toUpperCase();
        //String prom = multigene.promotor_sequence(tis, 3).toLowerCase();
        if (tis == multigene.getAnnTIS()) {
            //return prom + seq + " " + tis.toString()+"*";
            return seq + " " + tis.toString() + "*";
        } else {
            return seq + " " + tis.toString();
            //return prom + seq + " " + tis.toString();
        }

    }

    public Multigene getMultigene() {
        return multigene;
    }

    public Integer getTis() {
        return tis;
    }
}
