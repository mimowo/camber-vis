/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package ui;

import java.awt.Cursor;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.metadata.IIOMetadataNode;
import javax.imageio.stream.ImageOutputStream;
import static javax.print.attribute.ResolutionSyntax.DPI;
import javax.swing.JPanel;
import org.openide.windows.WindowManager;
import structs.Multigene;
import structs.MultigeneCluster;
import structs.MultigeneDataset;
import structs.Strain;
import utils.DataHandle;
import utils.Messenger;
import vis.CamberVisTopComponent;
import vis.MultigeneZoomTopComponent;

public final class VisualizationManager {

    private static VisualizationManager vis = new VisualizationManager();
    private Map<String, GenomeBrowser> genomeBrowsers = new TreeMap<String, GenomeBrowser>();
    private Boolean show_distances = Boolean.FALSE;
    private Multigene sel_multigene = null;

    public static VisualizationManager getDefault() {
        return vis;
    }

    public Boolean getShow_distances() {
        return show_distances;
    }

    public void setShow_distances(Boolean show_distances) {
        this.show_distances = show_distances;
    }

    public void zoomCluster(MultigeneCluster cluster, Map<String, Multigene> selected) {
        Integer max_length = cluster.maxMultigeneLength();
        MultigeneDataset dh = DataHandle.getDefault().getData();

        Set<String> strainsTmp = new TreeSet<String>(dh.getStrainKeys());

        for (String genome_id : selected.keySet()) {
            Multigene mult = selected.get(genome_id);
            String strain_id = mult.getStrain().getName();
            GenomeBrowser browser = this.genomeBrowsers.get(strain_id);
            browser.changeContig(mult.getContig());
            browser.zoomMultigene(mult, max_length);
            strainsTmp.remove(strain_id);
        }

        Multigene ref_mult = null;
        Multigene ref_prev_mult = null;
        Multigene ref_next_mult = null;
        for (String contig_id : selected.keySet()) {
            ref_mult = selected.get(contig_id);
            if (ref_mult != null) {
                ref_prev_mult = dh.getContig(contig_id).getAnnotation().prevAnchorInAllStrainsMultigene(ref_mult);
                ref_next_mult = dh.getContig(contig_id).getAnnotation().nextAnchorInAllStrainsMultigene(ref_mult);
                break;
            }
        }

        if (ref_prev_mult != null && ref_next_mult != null) {
            for (String strain_id : strainsTmp) {
                GenomeBrowser browser = this.genomeBrowsers.get(strain_id);

                for (String contig_id : dh.getStrain(strain_id).getContigNames()) {
                    Set<Multigene> set_prev = ref_prev_mult.getCluster().getMultigenesByContig(contig_id);
                    Set<Multigene> set_next = ref_next_mult.getCluster().getMultigenesByContig(contig_id);

                    if (set_prev.size() == 1 && set_next.size() == 1) {
                        Multigene mult_left = null;
                        Multigene mult_right = null;
                        for (Multigene mg : set_prev) {
                            mult_left = mg;
                        }
                        for (Multigene mg : set_next) {
                            mult_right = mg;
                        }

                        if (mult_left == null) {
                            Messenger.getDefault().showMessage("BAD" + strain_id);
                        }

                        browser.changeContig(mult_left.getContig());

                        if (Math.abs(mult_left.getLeft_bound() - mult_right.getRight_bound()) > Math.abs(mult_left.getRight_bound() - mult_right.getLeft_bound())) {
                            Integer region_center = (mult_left.getRight_bound() + mult_right.getLeft_bound()) / 2;
                            browser.zoomRegion(region_center, max_length);
                        } else {
                            Integer region_center = (mult_left.getLeft_bound() + mult_right.getRight_bound()) / 2;
                            browser.zoomRegion(region_center, max_length);
                        }
                    }
                }
            }
        }
    }

    public void shiftRight() {
        //  Map<String, GenomeBrowser> browsers = VisualizationManager.getDefault().getGenomeBrowsers();
        DataHandle dh = DataHandle.getDefault();

        for (String strain_id : dh.getData().getStrainKeys()) {
            GenomeBrowser browser = this.genomeBrowsers.get(strain_id);
            browser.shiftRight();
        }
    }

    public void shiftLeft() {
        //      Map<String, GenomeBrowser> browsers = VisualizationManager.getDefault().getthis.genomeBrowsers();
        DataHandle dh = DataHandle.getDefault();

        for (String strain_id : dh.getData().getStrainKeys()) {
            GenomeBrowser browser = this.genomeBrowsers.get(strain_id);
            browser.shiftLeft();
        }
    }

    public void ZoomOut() {
        //       Map<String, GenomeBrowser> browsers = VisualizationManager.getDefault().getthis.genomeBrowsers();
        DataHandle dh = DataHandle.getDefault();
        for (String strain_id : dh.getData().getStrainKeys()) {
            GenomeBrowser browser = this.genomeBrowsers.get(strain_id);
            browser.zoomOut();
        }
    }

    public void ZoomIn() {
        //    Map<String, GenomeBrowser> browsers = VisualizationManager.getDefault().getthis.genomeBrowsers();
        DataHandle dh = DataHandle.getDefault();

        for (String strain_id : dh.getData().getStrainKeys()) {
            GenomeBrowser browser = this.genomeBrowsers.get(strain_id);
            browser.zoomIn();
        }
    }

    public void drawBrowserPanels() {
        final DataHandle dh = DataHandle.getDefault();
        Frame mainWindow = WindowManager.getDefault().getMainWindow();

        mainWindow.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

        for (final String strain_id : dh.getData().getStrainKeys()) {

            final GenomeBrowser browser = genomeBrowsers.get(strain_id);
            browser.drawPanel();
        }
        mainWindow.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

    }

    public void cleanGenomeBrowsers() {
        JPanel browsersPanel = CamberVisTopComponent.getDefault().getBrowsersPanel();
        browsersPanel.removeAll();
        this.genomeBrowsers.clear();
        browsersPanel.updateUI();
    }

    public void createBrowserPanels() {
        DataHandle dh = DataHandle.getDefault();
        for (Strain strain : dh.getData().getStrains()) {
            GenomeBrowser browserPanel = new GenomeBrowser(strain);
            this.genomeBrowsers.put(strain.getName(), browserPanel);
        }
    }

    public void showBrowserPanels() {
        DataHandle dh = DataHandle.getDefault();
        final JPanel browsersPanel = CamberVisTopComponent.getDefault().getBrowsersPanel();
        browsersPanel.removeAll();
        //browsersPanel.setLayout(new GridLayout(dh.getData().getStrainKeys().size(), 1));
        //CamberVisTopComponent.getDefault().setHeightSize();

        for (String strain_id : dh.getData().getStrainKeys()) {
            GenomeBrowser browserPanel = this.genomeBrowsers.get(strain_id);
            browsersPanel.add(browserPanel);
            browserPanel.setVisible(true);
        }
        browsersPanel.updateUI();
    }

    public Map<String, GenomeBrowser> getGenomeBrowsers() {
        return this.genomeBrowsers;
    }

    void moveDown(String strain_id) {
        JPanel browsersPanel = CamberVisTopComponent.getDefault().getBrowsersPanel();
        GenomeBrowser genomeBrowser = genomeBrowsers.get(strain_id);
        Integer index = browsersPanel.getComponentZOrder(genomeBrowser);
        GenomeBrowser subpanel = null;
        boolean find = false;
        for (String strain : DataHandle.getDefault().getData().getStrainKeys()) {
            subpanel = this.genomeBrowsers.get(strain);
            if (browsersPanel.getComponentZOrder(subpanel) == index + 1) {
                find = true;
                break;
            }
        }

        if (subpanel != null && find == true) {
            browsersPanel.setComponentZOrder(genomeBrowser, index + 1);
            browsersPanel.setComponentZOrder(subpanel, index);
            browsersPanel.updateUI();
        }
    }

    void moveUp(String strain_id) {
        JPanel browsersPanel = CamberVisTopComponent.getDefault().getBrowsersPanel();
        GenomeBrowser genomeBrowser = this.genomeBrowsers.get(strain_id);
        Integer index = browsersPanel.getComponentZOrder(genomeBrowser);
        GenomeBrowser subpanel = null;
        boolean find = false;
        for (String strain : DataHandle.getDefault().getData().getStrainKeys()) {
            subpanel = this.genomeBrowsers.get(strain);
            if (browsersPanel.getComponentZOrder(subpanel) == index - 1) {
                find = true;
                break;
            }
        }
        if (subpanel != null && find == true) {
            browsersPanel.setComponentZOrder(genomeBrowser, index - 1);
            browsersPanel.setComponentZOrder(subpanel, index);
            browsersPanel.updateUI();
        }
    }

    public void exportToBitmap(String filename) throws IOException {
        //   Map<String, GenomeBrowser> browsers = VisualizationManager.getDefault().getGenomeBrowsers();
        DataHandle dh = DataHandle.getDefault();
        File output = new File(filename);
        int iw = 1000;
        int ih = 500;
        Set<BufferedImage> images = new HashSet<BufferedImage>();
        int count = 0;

        for (String strain_id : dh.getData().getStrainKeys()) {
            GenomeBrowser browser = this.genomeBrowsers.get(strain_id);
            if (browser.isActive()) {

                try {
                    BufferedImage image = browser.getImage();
                    iw = image.getWidth();
                    ih = image.getHeight();
                    images.add(image);
                    count += 1;
                } catch (IOException ex) {
                    Messenger.getDefault().showError("Not showed: " + ex.getMessage());
                }

            }
        }

        BufferedImage out = new BufferedImage(iw, count * ih, BufferedImage.TYPE_INT_RGB);
        Graphics g = out.getGraphics();
        count = 0;
        for (BufferedImage image : images) {
            if (image != null) {
                g.drawImage(image, 0, ih * count, null);
                count += 1;
            }
        }
        ImageIO.write(out, "jpg", output);
        //saveGridImage(out, output);
    }

    private void saveGridImage(BufferedImage gridImage, File output) throws IOException {
        output.delete();

        final String formatName = "png";

        for (Iterator<ImageWriter> iw = ImageIO.getImageWritersByFormatName(formatName); iw.hasNext();) {
            ImageWriter writer = iw.next();
            ImageWriteParam writeParam = writer.getDefaultWriteParam();
            ImageTypeSpecifier typeSpecifier = ImageTypeSpecifier.createFromBufferedImageType(BufferedImage.TYPE_INT_RGB);
            IIOMetadata metadata = writer.getDefaultImageMetadata(typeSpecifier, writeParam);
            if (metadata.isReadOnly() || !metadata.isStandardMetadataFormatSupported()) {
                continue;
            }

            setDPI(metadata);

            final ImageOutputStream stream = ImageIO.createImageOutputStream(output);
            try {
                writer.setOutput(stream);
                writer.write(metadata, new IIOImage(gridImage, null, metadata), writeParam);
            } finally {
                stream.close();
            }
            break;
        }
    }

    private void setDPI(IIOMetadata metadata) throws IIOInvalidTreeException {
        final double INCH_2_CM = 2.54;
        // for PMG, it's dots per millimeter
        double dotsPerMilli = 1.0 * DPI / 10 / INCH_2_CM;

        IIOMetadataNode horiz = new IIOMetadataNode("HorizontalPixelSize");
        horiz.setAttribute("value", Double.toString(dotsPerMilli));

        IIOMetadataNode vert = new IIOMetadataNode("VerticalPixelSize");
        vert.setAttribute("value", Double.toString(dotsPerMilli));

        IIOMetadataNode dim = new IIOMetadataNode("Dimension");
        dim.appendChild(horiz);
        dim.appendChild(vert);

        IIOMetadataNode root = new IIOMetadataNode("javax_imageio_1.0");
        root.appendChild(dim);

        metadata.mergeTree("javax_imageio_1.0", root);
    }

    public void showDistances() {
        //     Map<String, GenomeBrowser> browsers = VisualizationManager.getDefault().getthis.genomeBrowsers();
        DataHandle dh = DataHandle.getDefault();
        for (String strain_id : dh.getData().getStrainKeys()) {
            GenomeBrowser browser = this.genomeBrowsers.get(strain_id);
            browser.drawDistances();
            browser.refreshScene();
        }
    }

    public void hideDistances() {
        //Map<String, GenomeBrowser> browsers = VisualizationManager.getDefault().getthis.genomeBrowsers();
        DataHandle dh = DataHandle.getDefault();
        for (String strain_id : dh.getData().getStrainKeys()) {
            GenomeBrowser browser = this.genomeBrowsers.get(strain_id);
            browser.hideDistances();
            browser.refreshScene();
        }
    }

    public void multigeneClicked(Multigene multigene) {
        this.sel_multigene = multigene;
        MultigeneZoomTopComponent.getDefault().multigeneClicked(multigene);
    }

    public Multigene getSelMultigene() {
        return sel_multigene;
    }
}
