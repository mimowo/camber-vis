/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package utils;

import java.lang.String;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import structs.Multigene;
import structs.MultigeneCluster;
import structs.MultigeneDataset;
import structs.Strain;

public class DataStatsUtils {

    private static DataStatsUtils instance = new DataStatsUtils();
  //  private Map<StringPair, Integer> dist_matrix = new TreeMap<StringPair, Integer>();
    
    public static DataStatsUtils getDefault() {
        return instance;
    }

    public void calcDistanceMatrix() {
        MultigeneDataset dh = DataHandle.getDefault().getData();

        Set<String> strains = dh.getStrainKeys();
        int n = strains.size();
        String[] strains_list = (String[])strains.toArray(new String[n]);
        Map<String, Integer> strains_rev = new TreeMap<String, Integer> ();
        
        Double[][] dist_matrix = new Double[n][n];
        
        for (int i=0; i<n; i++) {
            
            strains_rev.put(strains_list[i], Integer.valueOf(i));
            for (int j=0; j<n; j++) {
                dist_matrix[i][j] = Double.valueOf(0.0);
            }
        }
       
        for (MultigeneCluster cluster : dh.getClusters()) {
            Set<String> strains_pres = new TreeSet<String>();
            Set<String> strains_abs = new TreeSet<String>(dh.getStrainKeys());
            for (Multigene multigene : cluster.getMultigenes().getMultigenes()) {
                strains_pres.add(multigene.getStrain().getName());
                strains_abs.remove(multigene.getStrain().getName());
            }
            for (String strain1_id : strains_pres) {
                for (String strain2_id : strains_abs) {
                    int index1 = strains_rev.get(strain1_id);
                    int index2 = strains_rev.get(strain2_id);
                    dist_matrix[index1][index2] += 1;
                }
            }
        }
//        for (int i=0;i<n;i++) {
//            for (int j=0;j<n;j++) {
//                System.out.println(String.valueOf(i) + " " + String.valueOf(j) + " " + String.valueOf(dist_matrix[i][j]));
//            }
//        }
    }
}
