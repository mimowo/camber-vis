/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */

package utils;

import javax.help.HelpSet;
import javax.help.JHelp;
import javax.swing.JFrame;
import org.netbeans.api.javahelp.Help;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

public class HelpHandler {

    private static HelpHandler h = new HelpHandler();
    private HelpSet help;
    private JHelp helpViewer;
    private JFrame helpFrame;

    public static HelpHandler getDefault() {
        return h;
    }

    public HelpSet getHelp() {
        return help;
    }

    public JHelp getHelpViewer() {
        if (helpViewer == null) {
            help = Lookup.getDefault().lookup(HelpSet.class);
            helpViewer = new JHelp(help);
        }
        return helpViewer;
    }

    public void showHelpFrame() {
        
            Help helpn = Lookup.getDefault().lookup(Help.class);
            HelpCtx helpCtx = HelpCtx.DEFAULT_HELP;
            //helpViewer = getHelpViewer();
            //helpViewerTmp.setCurrentID("cambervis");
           
            //helpCtx.setHelpIDString(helpViewerTmp, "cambervis");
            //Messenger.getDefault().showMessage(helpn.isValidID("cambervis", true).toString());
            helpn.showHelp(helpCtx, true);
            
        
//        if (helpFrame == null) {
//            helpFrame = new JFrame("CAMBerVis help");
//            helpFrame.add(getHelpViewer());
//            helpFrame.pack();
//            helpFrame.setAlwaysOnTop(true);
//            helpFrame.setLocationRelativeTo(null);
//            helpFrame.setVisible(true);
//        } else {
//            helpFrame.setVisible(true);
//        }
    }
}
