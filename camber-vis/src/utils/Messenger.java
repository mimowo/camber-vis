/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package utils;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.openide.windows.IOProvider;

public class Messenger {

    private static Messenger m = new Messenger();

    public static Messenger getDefault() {
        return m;
    }

    public void showMessage(String message) {
        IOProvider.getDefault().getIO("Messages", false).getOut().println(message);
        JOptionPane.showMessageDialog(null, message);
    }

    public boolean showConfirm(String msg, String title) {
        int ret = JOptionPane.showConfirmDialog(null, msg, title, JOptionPane.YES_NO_OPTION);
        if (ret == JOptionPane.YES_OPTION) {
            return true;
        } else {
            return false;
        }
    }

    public String showInput(String msg) {
        String ret = JOptionPane.showInputDialog(msg);
        return ret;
    }

    public void showText(String text, String title) {
        JFrame msg_dialog = new MSGFrame(text, title);
        msg_dialog.pack();
        msg_dialog.setVisible(true);
    }

    public void message(String message) {
        IOProvider.getDefault().getIO("Messages", false).getOut().println(message);
    }

    public void message(String message, String title) {
        IOProvider.getDefault().getIO(title, false).getOut().println(message);
    }

    public void showError(String msg) {
        IOProvider.getDefault().getIO("Errors", false).getOut().println(msg);
        JOptionPane.showMessageDialog(null, msg, "Error", JOptionPane.ERROR_MESSAGE);
    }
}
