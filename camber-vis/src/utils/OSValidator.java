/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */

package utils;

public class OSValidator {

    public static String system = null;

    public static boolean isWindows() {
        if (system == null) {
            getSystem();
        }
        return (system.indexOf("win") >= 0);

    }

    public static boolean isMac() {
        if (system == null) {
            getSystem();
        }
        return (system.indexOf("mac") >= 0);
    }

    public static boolean isUnix() {
        if (system == null) {
            getSystem();
        }
        return (system.indexOf("nix") >= 0 || system.indexOf("nux") >= 0);
    }

    private static void getSystem() {
        system = System.getProperty("os.name").toLowerCase();
    }
}
