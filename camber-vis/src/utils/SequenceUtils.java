/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package utils;

public class SequenceUtils {

    public static String removeSpaces(String sequence) {
        StringBuilder clear = new StringBuilder("");
        for (int i = 0; i < sequence.length(); i++) {
            if (sequence.charAt(i) != '-') {
                clear.append(sequence.charAt(i));
            }
        }
        return clear.toString();
    }

    public static String revSequence(String sequence) {
        String reverse = new StringBuffer(sequence).reverse().toString();
        return reverse;
    }

    public static String compSequence(String sequence) {
        StringBuilder comp = new StringBuilder("");
        for (int i = 0; i < sequence.length(); i++) {
            if (sequence.charAt(i) == 'A') {
                comp.append('T');
            } else if (sequence.charAt(i) == 'T') {
                comp.append('A');
            } else if (sequence.charAt(i) == 'G') {
                comp.append('C');
            } else if (sequence.charAt(i) == 'C') {
                comp.append('G');
            } else {
                comp.append(sequence.charAt(i));
            }
        }
        return comp.toString();
    }

    public static String compRevSequence(String sequence) {
        String revSeq = revSequence(sequence);
        String compRevSeq =  compSequence(revSeq);
        return compRevSeq;
    }

    public static String translate(String nn_sequence) {
        StringBuilder aa_sequence = new StringBuilder("");
        String codon = "";
        for (int i = 0; i < nn_sequence.length(); i++) {
            if (codon.length() < 3) {
                codon += nn_sequence.charAt(i);
            }
            if (codon.length() == 3) {
                if (codon.equals("GCT") || codon.equals("GCC") || codon.equals("GCA") || codon.equals("GCG")) {
                    aa_sequence.append("A");
                } else if (codon.equals("TTA") || codon.equals("TTG") || codon.equals("CTT") || codon.equals("CTC") || codon.equals("CTA") || codon.equals("CTG")) {
                    aa_sequence.append("L");
                } else if (codon.equals("CGT") || codon.equals("CGC") || codon.equals("CGA") || codon.equals("CGG") || codon.equals("AGA") || codon.equals("AGG")) {
                    aa_sequence.append("R");
                } else if (codon.equals("AAA") || codon.equals("AAG")) {
                    aa_sequence.append("K");
                } else if (codon.equals("AAT") || codon.equals("AAC")) {
                    aa_sequence.append("N");
                } else if (codon.equals("ATG")) {
                    aa_sequence.append("M");
                } else if (codon.equals("GAT") || codon.equals("GAC")) {
                    aa_sequence.append("D");
                } else if (codon.equals("TTT") || codon.equals("TTC")) {
                    aa_sequence.append("F");
                } else if (codon.equals("TGT") || codon.equals("TGC")) {
                    aa_sequence.append("C");
                } else if (codon.equals("CCT") || codon.equals("CCC") || codon.equals("CCA") || codon.equals("CCG")) {
                    aa_sequence.append("P");
                } else if (codon.equals("CAA") || codon.equals("CAG")) {
                    aa_sequence.append("Q");
                } else if (codon.equals("TCT") || codon.equals("TCC") || codon.equals("TCA") || codon.equals("TCG") || codon.equals("AGT") || codon.equals("AGC")) {
                    aa_sequence.append("S");
                } else if (codon.equals("GAA") || codon.equals("GAG")) {
                    aa_sequence.append("E");
                } else if (codon.equals("ACT") || codon.equals("ACC") || codon.equals("ACA") || codon.equals("ACG")) {
                    aa_sequence.append("T");
                } else if (codon.equals("GGT") || codon.equals("GGC") || codon.equals("GGA") || codon.equals("GGG")) {
                    aa_sequence.append("G");
                } else if (codon.equals("TGG")) {
                    aa_sequence.append("W");
                } else if (codon.equals("CAT") || codon.equals("CAC")) {
                    aa_sequence.append("H");
                } else if (codon.equals("TAT") || codon.equals("TAC")) {
                    aa_sequence.append("Y");
                } else if (codon.equals("ATT") || codon.equals("ATC") || codon.equals("ATA")) {
                    aa_sequence.append("I");
                } else if (codon.equals("GTT") || codon.equals("GTC") || codon.equals("GTA") || codon.equals("GTG")) {
                    aa_sequence.append("V");
                } else if (codon.equals("TAA") || codon.equals("TGA") || codon.equals("TAG")) {
                    aa_sequence.append("*");
                } else {
                    aa_sequence.append("#");
                }

                codon = "";
            }
        }
        return aa_sequence.toString();
    }
}
