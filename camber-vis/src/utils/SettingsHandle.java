/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package utils;

import io.ConfigReaderWriter;
import java.io.File;
import java.util.Map;

public final class SettingsHandle {

    final public Integer CLUSTALW_NOT_EXISTS = -1;
    final public Integer CLUSTALW_NOT_EXECUTABLE = 0;
    final public Integer CLUSTALW_OK = 1;
    public static int SCHEMA_COLOR_CLUSTERS = 0;
    public static int SCHEMA_COLOR_TYPES = 1;
    public static int SCHEMA_COLOR_CONS = 2;
    public static int SHOW_REMOVED_Y = 0;
    public static int SHOW_REMOVED_N = 1;
    private static SettingsHandle sh = new SettingsHandle();
    //  private String curDir = "";
//    private String clustalPath = "";
//    private String blastPath = "";
//    private String formatdbPath = "";
//    private String workingDirPath = "";
    private String camberVisPath = "";
    private Integer lineCharCount = 60;
    //private String userEmail = "";
    private Integer schemaColor;
    private Integer showRemoved;
    private Map<String, String> config;
    private ConfigReaderWriter configReaderWriter;

    public static SettingsHandle getDefault() {
        return sh;
    }

    public SettingsHandle() {
        computeCamberVisPath();
        configReaderWriter = new ConfigReaderWriter(camberVisPath + File.separator + "config.txt");
        config = configReaderWriter.readConfig();
        schemaColor = Integer.valueOf(SettingsHandle.SCHEMA_COLOR_CONS);
        showRemoved = Integer.valueOf(SettingsHandle.SHOW_REMOVED_Y);
        loadDefault();

    }

    public String computeCamberVisPath() {

        String jarpath = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
        jarpath = jarpath.replace("%20", " ");
        jarpath = jarpath.substring(5);

        String currpath = jarpath;
        boolean found = false;

        while (!found) {
            String examplesPath = currpath + File.separator + "examples";
            Messenger.getDefault().message("CAMVerVis path: " + currpath);
            found = (new File(examplesPath)).exists();
            if (!found) {
                currpath = (new File(currpath)).getParent();
            }
        }
        this.camberVisPath = (new File(currpath)).getAbsolutePath();
        return camberVisPath;
    }

    public String computeCamberVisPathOld() {
        if (OSValidator.isMac()) {
            Messenger.getDefault().showMessage("MAC");
            this.camberVisPath = (new File(".")).getAbsolutePath();
        } else {
            Messenger.getDefault().showMessage("NOT MAC :(");
            this.camberVisPath = System.getProperty("user.dir");
        }
        return camberVisPath;
    }

    public void saveConfig() {
        configReaderWriter.saveConfig(config);
    }
//    public void loadDefault() {
//
//        blastPath = curDir + "\\ext\\blastall.exe";
//        clustalPath = curDir + "\\ext\\clustalw2.exe";
//        formatdbPath = curDir + "\\ext\\formatdb.exe";
//        workingDirPath = curDir + "\\tmp\\";
//        schemaColor = SCHEMA_COLOR_TYPES;
//    }

    public void loadDefault() {

        //Map<String, String> config = configReaderWriter.readConfig();
        if (!config.containsKey("BLAST")) {
            if (OSValidator.isWindows()) {
                config.put("BLAST", camberVisPath + File.separator + "ext" + File.separator + "blastall.exe");
            } else {
                config.put("BLAST", camberVisPath + File.separator + "ext" + File.separator + "blastall");
            }
        }
        if (!config.containsKey("CLUSTALW")) {
            if (OSValidator.isWindows()) {
                config.put("CLUSTALW", camberVisPath + File.separator + "ext" + File.separator + "clustalw2.exe");
            } else {
                config.put("CLUSTALW", camberVisPath + File.separator + "ext" + File.separator + "clustalw2");
            }
        }
        if (!config.containsKey("FORMATDB")) {
            if (OSValidator.isWindows()) {
                config.put("FORMATDB", camberVisPath + File.separator + "ext" + File.separator + "formatdb.exe");
            } else {
                config.put("FORMATDB", camberVisPath + File.separator + "ext" + File.separator + "formatdb");
            }
        }
        if (!config.containsKey("WORKDIR")) {
            config.put("WORKDIR", System.getProperty("java.io.tmpdir") + File.separator);
            //config.put("WORKDIR", camberVisPath + File.separator + "tmp" + File.separator);
        }
        if (!config.containsKey("EMAIL")) {
            config.put("EMAIL", "");
        }
    }

    public String getBlastPath() {
        return config.get("BLAST");
    }

    public String getClustalPath() {
        return config.get("CLUSTALW");
    }

    public String getFormatdbPath() {
        return config.get("FORMATDB");
    }

    public String getTmpDirPath() {
        return config.get("WORKDIR");
    }

    public String setBlastPath(String path) {
        return config.put("BLAST", path);
    }

    public String setClustalPath(String path) {
        return config.put("CLUSTALW", path);
    }

    public String setFormatdbPath(String path) {
        return config.put("FORMATDB", path);
    }

    public String setWorkingDirPath(String path) {
        return config.put("WORKDIR", path);
    }

    public String getCamberVisPath() {
        if (camberVisPath == null) {
            camberVisPath = computeCamberVisPath();
        }
        return camberVisPath;
    }

    public Integer getLineCharCount() {
        return lineCharCount;
    }

    public void setLineCharCount(Integer lineCharCount) {
        this.lineCharCount = lineCharCount;
    }

    public String getUserEmail() {
        return config.get("EMAIL");
    }

    public void setUserEmail(String userEmail) {
        config.put("EMAIL", userEmail);
    }

    public Integer getSchemaColor() {
        return schemaColor;
    }

    public void setSchemaColor(Integer schemaColor) {
        this.schemaColor = schemaColor;
    }

    public Integer getShowRemoved() {
        return showRemoved;
    }

    public void setShowRemoved(Integer showRemoved) {
        this.showRemoved = showRemoved;
    }

    public String getExamplesFolder() {
        getCamberVisPath();
        return camberVisPath + File.separator + "examples";
    }

    public Integer validateClustalW() {
        String path = getClustalPath();
        File tmp = new File(path);
        if (!tmp.exists()) {
            return CLUSTALW_NOT_EXISTS;
        } else if (!tmp.canExecute()) {
            return CLUSTALW_NOT_EXECUTABLE;
        } else {
            return CLUSTALW_OK;
        }
    }
}
