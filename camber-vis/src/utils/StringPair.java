/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package utils;

public class StringPair implements Comparable<StringPair> {

    protected String fFirst;
    protected String fSecond;

    public StringPair(String one, String two) {
        this.fFirst = one;
        this.fSecond = two;
    }

    public String getFirst() {
        return fFirst;
    }

    public void setFirst(String one) {
        this.fFirst = one;
    }

    public String getSecond() {
        return fSecond;
    }

    public void setSecond(String two) {
        this.fSecond = two;
    }

    @Override
    public boolean equals(Object arg0) {
        if (arg0 instanceof StringPair) {
            return ((StringPair) arg0).fFirst.equals(fFirst) && ((StringPair) arg0).fSecond.equals(fSecond);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return fFirst.hashCode();
    }

    @Override
    public String toString() {
        return "[" + fFirst + "], [" + fSecond + "]";
    }

    @Override
    public int compareTo(StringPair o) {
        Integer cmp = this.fFirst.compareTo(o.fFirst);
        if (cmp.equals(0)) {
            return this.fSecond.compareTo(o.fSecond);
        } else {
            return cmp.intValue();
        }
    }
}