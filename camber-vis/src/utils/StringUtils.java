/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */

package utils;

public class StringUtils {

    public static final char LF = '\n';
    public static final char CR = '\r';

    public static String chop(String str) {
        if (str == null) {
            return null;
        }
        int strLen = str.length();
        if (strLen < 2) {
            return "";
        }
        int lastIdx = strLen - 1;
        char last = str.charAt(lastIdx);
        if (last == LF) {
            if (str.charAt(lastIdx - 1) == CR) {
                return str.substring(0, lastIdx - 1);
            }
        }

        return str;
    }

    public static String wrapText(String text, int line_char_count) {
        String ret = "";
        Integer lines_count = (text.length() - 1) / line_char_count;
        for (int i = 0; i < lines_count + 1; i++) {
            String sub_mg_seq = text.substring(i * line_char_count, Math.min(text.length(), (i + 1) * line_char_count));
            ret += sub_mg_seq + "\n";
        }
        return ret;
    }
}
