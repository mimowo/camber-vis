/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */

package utils;

import java.awt.Color;
import java.util.Map;
import java.util.TreeMap;
import java.util.Random;

public class VisSettingsHandle {

    private Random random = new Random(12331223);
    private Map<String, Color> clusterColors = new TreeMap<String, Color>();
    private static VisSettingsHandle instance = new VisSettingsHandle();

    public static VisSettingsHandle getDefault() {
        return instance;
    }

    public Color checkColor(String cluster_id) {
        if (clusterColors.containsKey(cluster_id)) {
            return clusterColors.get(cluster_id);
        } else {
            int r = random.nextInt(255);
            int g = random.nextInt(255);
            int b = random.nextInt(255);
            Color color = new Color(r, g, b);
            clusterColors.put(cluster_id, color);
            return color;
        }
    }

    public void cleanColors() {
        clusterColors = new TreeMap<String, Color>();
    }
}
