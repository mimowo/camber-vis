/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package vis;

import init.CamberVisState;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Logger;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;
import org.netbeans.api.settings.ConvertAsProperties;
import structs.Multigene;
import structs.MultigeneCluster;
import structs.MultigeneCollection;
import ui.ClustalWGeneralFrame;
import ui.ShowSequenceFrame;
import ui.VisualizationManager;
import utils.DataHandle;
import utils.Messenger;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(dtd = "-//vis//ClusterVis//EN",
autostore = false)
public final class ClusterVisTopComponent extends TopComponent {
    
    private ClustalWGeneralFrame frame;
    private MultigeneCluster sel_cluster;
    private MultigeneCollection multigenes = new MultigeneCollection();
    private static ClusterVisTopComponent instance;
    private Set<ClusterStrainMembers> selectionPanels = new HashSet<ClusterStrainMembers>();
    /** path to the icon used by the component and its open action */
//    static final String ICON_PATH = "SET/PATH/TO/ICON/HERE";
    private static final String PREFERRED_ID = "ClusterVisTopComponent";

    public void cleanMenu() {
        sel_cluster = null;
        membersPanel.removeAll();
        conn_id_label.setText("Multigene cluster ID: ");
        conn_type_label.setText("Multigene cluster type: ");
        mult_count_label.setText("Number of multigenes: ");
        membersPanel.updateUI();
        updateUI();
    }

    public ClusterVisTopComponent() {
        initComponents();
        setName(NbBundle.getMessage(ClusterVisTopComponent.class, "CTL_ClusterVisTopComponent"));
        setToolTipText(NbBundle.getMessage(ClusterVisTopComponent.class, "HINT_ClusterVisTopComponent"));
//        setIcon(ImageUtilities.loadImage(ICON_PATH, true));
        this.jTextField1.addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {

                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    jButton2.requestFocus();
                    jButton2.doClick();
                }
            }
        });

    }

    public void showLists(MultigeneCluster cluster) {
        DataHandle dh = DataHandle.getDefault();
        Integer count = 0;

        for (String strain_id : dh.getData().getStrainKeys()) {
            Set<Multigene> mults = cluster.getMultigenesByStrain(strain_id);
            if (mults.size() > 0) {
                count += 1;
            }
        }
        jScrollPane1.setPreferredSize(new Dimension(302, 24 * count));
        jScrollPane1.setMaximumSize(new Dimension(302, 24 * count));
        jScrollPane1.setSize(302, 24 * count);
        membersPanel.setPreferredSize(new Dimension(302, 24 * count));
        membersPanel.setMaximumSize(new Dimension(302, 24 * count));
        membersPanel.setSize(302, 24 * count);

        for (String strain_id : dh.getData().getStrainKeys()) {
            Set<Multigene> mults = cluster.getMultigenesByStrain(strain_id);
            if (mults.size() > 0) {
                ClusterStrainMembers mem = new ClusterStrainMembers(mults, strain_id);
                membersPanel.add(mem);
                selectionPanels.add(mem);
            }
        }
        jScrollPane1.updateUI();

    }

    public void showInfo(MultigeneCluster cluster) {
        membersPanel.removeAll();
        DataHandle dh = DataHandle.getDefault();

        selectionPanels = new HashSet<ClusterStrainMembers>();
        membersPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
        conn_id_label.setText("Multigene cluster ID: " + cluster.getCluster_id());
        if (cluster.isAnchor()) {
            conn_type_label.setText("Multigene cluster type: ANCHOR");
        } else {
            conn_type_label.setText("Multigene cluster type: NON-ANCHOR");
        }
        mult_count_label.setText("Number of multigenes: " + cluster.getSize().toString() + " (in " + cluster.countStrains().toString() + " out of " + dh.getData().getStrainCount().toString() + " strains)");
        updateUI();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mult_count_label = new javax.swing.JLabel();
        conn_id_label = new javax.swing.JLabel();
        conn_type_label = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        membersPanel = new javax.swing.JPanel();
        jButton2 = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();

        setPreferredSize(new java.awt.Dimension(300, 300));

        org.openide.awt.Mnemonics.setLocalizedText(mult_count_label, org.openide.util.NbBundle.getMessage(ClusterVisTopComponent.class, "ClusterVisTopComponent.mult_count_label.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(conn_id_label, org.openide.util.NbBundle.getMessage(ClusterVisTopComponent.class, "ClusterVisTopComponent.conn_id_label.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(conn_type_label, org.openide.util.NbBundle.getMessage(ClusterVisTopComponent.class, "ClusterVisTopComponent.conn_type_label.text")); // NOI18N

        jScrollPane1.setPreferredSize(new java.awt.Dimension(116, 101));

        membersPanel.setBackground(new java.awt.Color(153, 255, 255));
        membersPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout membersPanelLayout = new javax.swing.GroupLayout(membersPanel);
        membersPanel.setLayout(membersPanelLayout);
        membersPanelLayout.setHorizontalGroup(
            membersPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 306, Short.MAX_VALUE)
        );
        membersPanelLayout.setVerticalGroup(
            membersPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 461, Short.MAX_VALUE)
        );

        jScrollPane1.setViewportView(membersPanel);

        org.openide.awt.Mnemonics.setLocalizedText(jButton2, org.openide.util.NbBundle.getMessage(ClusterVisTopComponent.class, "ClusterVisTopComponent.jButton2.text")); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jTextField1.setText(org.openide.util.NbBundle.getMessage(ClusterVisTopComponent.class, "ClusterVisTopComponent.jTextField1.text")); // NOI18N

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/alignment.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(jButton1, org.openide.util.NbBundle.getMessage(ClusterVisTopComponent.class, "ClusterVisTopComponent.jButton1.text")); // NOI18N
        jButton1.setToolTipText(org.openide.util.NbBundle.getMessage(ClusterVisTopComponent.class, "ClusterVisTopComponent.jButton1.toolTipText")); // NOI18N
        jButton1.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 304, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(mult_count_label, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 284, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(conn_id_label, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 284, Short.MAX_VALUE)
                    .addComponent(conn_type_label, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 280, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(1, 1, 1)
                .addComponent(conn_id_label)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(conn_type_label)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mult_count_label, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 217, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    //public void zoomCluster(MultigeneCluster cluster) {
    public void selectionChanged() {
        multigenes = new MultigeneCollection();
        for (ClusterStrainMembers mem : selectionPanels) {
            Multigene mult = mem.getSelectedMultigene();
            if (mult != null) {
                multigenes.addMultigene(mult);
            }
        }
    }

    public void zoomCluster() {
        if (CamberVisState.getDefault().isGenomes_shown()) {
            if (sel_cluster != null) {
                selectionChanged();
                Map<String, Multigene> selected = new TreeMap<String, Multigene>();
                for (Multigene mult : multigenes.getMultigenes()) {
                    selected.put(mult.getContig().getName(), mult);
                }
                VisualizationManager.getDefault().zoomCluster(sel_cluster, selected);
                VisualizationManager.getDefault().drawBrowserPanels();
            }
        } else {
            Messenger.getDefault().showMessage("First, visualize genomes.");
        }
    }

    public void setSelectedCluster(MultigeneCluster sel_cluster) {
        this.sel_cluster = sel_cluster;
    }

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        DataHandle dh = DataHandle.getDefault();
        String cluster_id = jTextField1.getText();
        MultigeneCluster cluster = dh.getData().getCluster(cluster_id);
        if (cluster != null) {
            sel_cluster = cluster;
            ClusterVisTopComponent.getDefault().showInfo(cluster);
            ClusterVisTopComponent.getDefault().showLists(cluster);
            ClusterVisTopComponent.getDefault().zoomCluster();
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        showClustalWFrame();
    }//GEN-LAST:event_jButton1ActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel conn_id_label;
    private javax.swing.JLabel conn_type_label;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JPanel membersPanel;
    private javax.swing.JLabel mult_count_label;
    // End of variables declaration//GEN-END:variables

    /**
     * Gets default instance. Do not use directly: reserved for *.settings files only,
     * i.e. deserialization routines; otherwise you could get a non-deserialized instance.
     * To obtain the singleton instance, use {@link #findInstance}.
     */
    public static synchronized ClusterVisTopComponent getDefault() {
        if (instance == null) {
            instance = new ClusterVisTopComponent();
        }
        return instance;
    }

    /**
     * Obtain the ClusterVisTopComponent instance. Never call {@link #getDefault} directly!
     */
    public static synchronized ClusterVisTopComponent findInstance() {
        TopComponent win = WindowManager.getDefault().findTopComponent(PREFERRED_ID);
        if (win == null) {
            Logger.getLogger(ClusterVisTopComponent.class.getName()).warning(
                    "Cannot find " + PREFERRED_ID + " component. It will not be located properly in the window system.");
            return getDefault();
        }
        if (win instanceof ClusterVisTopComponent) {
            return (ClusterVisTopComponent) win;
        }
        Logger.getLogger(ClusterVisTopComponent.class.getName()).warning(
                "There seem to be multiple components with the '" + PREFERRED_ID
                + "' ID. That is a potential source of errors and unexpected behavior.");
        return getDefault();
    }

    @Override
    public int getPersistenceType() {
        return TopComponent.PERSISTENCE_ALWAYS;
    }

    @Override
    public void componentOpened() {
        // TODO add custom code on component opening
    }

    @Override
    public void componentClosed() {
        // TODO add custom code on component closing
    }

    void writeProperties(java.util.Properties p) {
        // better to version settings since initial version as advocated at
        // http://wiki.apidesign.org/wiki/PropertyFiles
        p.setProperty("version", "1.0");
        // TODO store your settings
    }

    Object readProperties(java.util.Properties p) {
        if (instance == null) {
            instance = this;
        }
        instance.readPropertiesImpl(p);
        return instance;
    }

    private void readPropertiesImpl(java.util.Properties p) {
        String version = p.getProperty("version");
        // TODO read your settings according to their version
    }

    @Override
    protected String preferredID() {
        return PREFERRED_ID;
    }

    public void showClustalWFrame() {
        if (frame == null) {
            frame = new ClustalWGeneralFrame();
            frame.pack();
            frame.setLocationRelativeTo(WindowManager.getDefault().getMainWindow());
        }
        frame.setSelectedCluster(sel_cluster, multigenes);
        frame.loadMultigenePanels0();
        frame.setVisible(true);
        //} else {
        //  Messenger.getDefault().showMessage("First, select a Multigene cluster.");
        // }
    }

    public void resetClustalWFrame() {
        if(frame != null) {
            frame.dispose();
            frame = null;
        }
        multigenes = new MultigeneCollection();
        sel_cluster = null;
    }
    
}
