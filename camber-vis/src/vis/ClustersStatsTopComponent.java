/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */

package vis;

import actions.TablePopup;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;
import org.netbeans.api.settings.ConvertAsProperties;
import structs.MultigeneCluster;
import ui.ExcelTableAdapter;
import utils.DataHandle;
import utils.Messenger;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(dtd = "-//vis//ClustersStats//EN", autostore = false)
public final class ClustersStatsTopComponent extends TopComponent {

    public ClustersStatsTopComponent() {
        initComponents();
        setName(NbBundle.getMessage(ClustersStatsTopComponent.class, "CTL_ClustersStatsTopComponent"));
        setToolTipText(NbBundle.getMessage(ClustersStatsTopComponent.class, "HINT_ClustersStatsTopComponent"));
        prepareTableAdapter();

    }

    public void cleanTable() {
        DefaultTableModel model = (DefaultTableModel) jTable3.getModel();
        int n = model.getRowCount();
        for (int i = 0; i < n; i++) {

            model.removeRow(n - i - 1);
        }
        jTable3.updateUI();
    }

    public void loadTable() {
        jTable3.removeAll();
        DefaultTableModel model = (DefaultTableModel) jTable3.getModel();
        jTable3.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        for (MultigeneCluster cluster : DataHandle.getDefault().getData().getClusters()) {
            String id = cluster.getCluster_id();
            if (cluster.isRemoved()) {
                id = "#" + id;
            }
            Integer mc = cluster.getSize();
            Integer ccc = cluster.countStrains();
            Integer acc = cluster.countAnnotatedStrains();
            Integer max_len = cluster.maxMultigeneLength();
            Integer max_tis = cluster.maxNumberOfTIS();
            Integer mc_main = cluster.getMainSize();
            Integer mc_plasmid = cluster.getPlasmidSize();
            Double overlap = cluster.getOverlap()*100.0; 
            String crem = "N";
            String untypical_str = cluster.getUntypical();
            String type = "";
            if (cluster.isAnchor()) {
                type = "ANCHOR";
            } else {
                type = "NON_ANCHOR";
            }
            if (cluster.isRemoved()) {
                crem = "Y";
            }
            Object[] row = {id, mc, mc_main, mc_plasmid, ccc, acc, max_len, max_tis, overlap, crem, untypical_str, type};
            model.addRow(row);
        }
        jTable3.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {

                if (SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 2) {
                    DataHandle dh = DataHandle.getDefault();
                    //} else if (SwingUtilities.isRightMouseButton(e)) {
                    Point p = e.getPoint();
                    int rowNumber = jTable3.rowAtPoint(p);
                    ListSelectionModel model = jTable3.getSelectionModel();
                    model.setSelectionInterval(rowNumber, rowNumber);
                    Integer row = jTable3.getSelectedRow();
                    Integer col = 0;
                    while (!jTable3.getColumnName(col).equals("cluster id")) {
                        col++;
                    }
                    String cluster_id = jTable3.getValueAt(row, col).toString();
                    if (cluster_id.startsWith("#")) {
                        cluster_id = cluster_id.substring(2);
                    }
                    
                    MultigeneCluster cluster = dh.getData().getCluster(cluster_id);

                    ClusterVisTopComponent.getDefault().setSelectedCluster(cluster);
                    ClusterVisTopComponent.getDefault().showInfo(cluster);
                    ClusterVisTopComponent.getDefault().showLists(cluster);
                    ClusterVisTopComponent.getDefault().zoomCluster();
                }
            }
        });
    }

    private void prepareTableAdapter() {
        KeyStroke copy = KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK, false);
        ExcelTableAdapter adapter = new ExcelTableAdapter(jTable3);
        TablePopup popup = new TablePopup(adapter);
        popup.setIs_copy(true);
        jTable3.registerKeyboardAction(adapter, "Copy", copy, JComponent.WHEN_FOCUSED);
        jTable3.addMouseListener(popup);
    }
    
    private static ClustersStatsTopComponent instance;
    /** path to the icon used by the component and its open action */
//    static final String ICON_PATH = "SET/PATH/TO/ICON/HERE";
    private static final String PREFERRED_ID = "ClustersStatsTopComponent";

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTable4 = new javax.swing.JTable();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(jTable1);

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(jTable2);

        jTable4.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane5.setViewportView(jTable4);

        setToolTipText(org.openide.util.NbBundle.getMessage(ClustersStatsTopComponent.class, "ClustersStatsTopComponent.jTable3.toolTipText")); // NOI18N

        jScrollPane4.setToolTipText(org.openide.util.NbBundle.getMessage(ClustersStatsTopComponent.class, "ClustersStatsTopComponent.jTable3.toolTipText")); // NOI18N

        jTable3.setAutoCreateRowSorter(true);
        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "cluster id", "# of mgs", "# of mgs on main", "# of mg on plasmids", "# of strains", "# of ann strains", "max length", "max # of TIS", "max overlap", "contains removed", "untypical start", "cluster type"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Double.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable3.setToolTipText(org.openide.util.NbBundle.getMessage(ClustersStatsTopComponent.class, "ClustersStatsTopComponent.jTable3.toolTipText")); // NOI18N
        jTable3.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jScrollPane4.setViewportView(jTable3);
        jTable3.getAccessibleContext().setAccessibleDescription(org.openide.util.NbBundle.getMessage(ClustersStatsTopComponent.class, "ClustersStatsTopComponent.jTable3.AccessibleContext.accessibleDescription")); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 414, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 641, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents
//    private void loadItems() {
//        DataHandle dh = DataHandle.getDefault();
//
//        MultigeneGraph graph = dh.getGraph();
//
//        jComboBox1.removeAllItems();
//
//        for (MultigeneCluster cluster : graph.getClusters()) {
//            if (!cluster.isAnchor()) {
//                ClusterItemObject cio = new ClusterItemObject(cluster);
//                jComboBox1.addItem(cio);
//            }
//        }
//        jComboBox1.updateUI();
//    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable3;
    private javax.swing.JTable jTable4;
    // End of variables declaration//GEN-END:variables

    /**
     * Gets default instance. Do not use directly: reserved for *.settings files only,
     * i.e. deserialization routines; otherwise you could get a non-deserialized instance.
     * To obtain the singleton instance, use {@link #findInstance}.
     */
    public static synchronized ClustersStatsTopComponent getDefault() {
        if (instance == null) {
            instance = new ClustersStatsTopComponent();
        }
        return instance;
    }

    /**
     * Obtain the ClustersStatsTopComponent instance. Never call {@link #getDefault} directly!
     */
    public static synchronized ClustersStatsTopComponent findInstance() {
        TopComponent win = WindowManager.getDefault().findTopComponent(PREFERRED_ID);
        if (win == null) {
            Logger.getLogger(ClustersStatsTopComponent.class.getName()).warning(
                    "Cannot find " + PREFERRED_ID + " cluster. It will not be located properly in the window system.");
            return getDefault();
        }
        if (win instanceof ClustersStatsTopComponent) {
            return (ClustersStatsTopComponent) win;
        }
        Logger.getLogger(ClustersStatsTopComponent.class.getName()).warning(
                "There seem to be multiple clusters with the '" + PREFERRED_ID
                + "' ID. That is a potential source of errors and unexpected behavior.");
        return getDefault();
    }

    @Override
    public int getPersistenceType() {
        return TopComponent.PERSISTENCE_ALWAYS;
    }

    @Override
    public void componentOpened() {
        // TODO add custom code on component opening
    }

    @Override
    public void componentClosed() {
        // TODO add custom code on component closing
    }

    void writeProperties(java.util.Properties p) {
        // better to version settings since initial version as advocated at
        // http://wiki.apidesign.org/wiki/PropertyFiles
        p.setProperty("version", "1.0");
        // TODO store your settings
    }

    Object readProperties(java.util.Properties p) {
        if (instance == null) {
            instance = this;
        }
        instance.readPropertiesImpl(p);
        return instance;
    }

    private void readPropertiesImpl(java.util.Properties p) {
        String version = p.getProperty("version");
        // TODO read your settings according to their version
    }

    @Override
    protected String preferredID() {
        return PREFERRED_ID;
    }
}
