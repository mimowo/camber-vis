/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package vis;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.logging.Logger;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;
import org.openide.util.ImageUtilities;
import org.netbeans.api.settings.ConvertAsProperties;
import structs.Multigene;
import ui.NCBIAPIFrame;
import utils.Messenger;
import utils.SequenceUtils;
import utils.SettingsHandle;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(dtd = "-//vis//MultigeneZoom//EN",
autostore = false)
public final class MultigeneZoomTopComponent extends TopComponent {

    //private Integer line_char_count = 60;//SettingsHandle.getDefault().getLineCharCount();
    private static MultigeneZoomTopComponent instance;
    /** path to the icon used by the component and its open action */
    static final String ICON_PATH = "vis/arrow-in.png";
    private static final String PREFERRED_ID = "MultigeneZoomTopComponent";
    private Multigene multigene;

    public MultigeneZoomTopComponent() {
        initComponents();
        setName(NbBundle.getMessage(MultigeneZoomTopComponent.class, "CTL_MultigeneZoomTopComponent"));
        setToolTipText(NbBundle.getMessage(MultigeneZoomTopComponent.class, "HINT_MultigeneZoomTopComponent"));
        setIcon(ImageUtilities.loadImage(ICON_PATH, true));
        this.jTextPane1.setEditable(false);
        prepareTextPane();
        cleanTextMenu();
    }

    public void cleanTextMenu() {
        multigene = null;
        String text = "";
        //text += "Multigene ID: \n";
        //text += "Lengths: \n";
        //text += "Locus: \n";
        text += "Select a multigene \n";
        jTextAreaDetails.setText(text);
    }

    private void updateTextMenu() {
        String text = "";
        jTextAreaDetails.setText("");
        text += "Multigene ID: " + multigene.getGeneID() + "\n";
        text += "Gene name: " + multigene.getGeneName() + "\n";
        text += "Strand: " + multigene.getStrand() + "\n";
        String lengths_text = "";
        Integer count = 0;
        for (Integer length_gene : multigene.getTISs()) {
            count += 1;
            if (multigene.getAnnTIS() == length_gene) {
                lengths_text += length_gene.toString() + "*";
            } else {
                lengths_text += length_gene.toString();
            }
            if (count < multigene.getTISs().size()) {
                lengths_text += ",";
            }
        }
        text += "TISs: " + lengths_text + "\n";
        text += "Locus: (" + multigene.getStart().toString() + "," + multigene.getEnd().toString() + ")" + "\n";
        jTextAreaDetails.setText(text);
    }

    public void multigeneClicked(Multigene multigene) {
        this.multigene = multigene;
        updateTextMenu();
        showMultigeneSequence();
        highlightTISs();
    }

    public void highlightTISs() {
        if (multigene == null) {
            return;
        }
        Integer line_char_count = SettingsHandle.getDefault().getLineCharCount();
        StyledDocument document = jTextPane1.getStyledDocument();
        SimpleAttributeSet attrs = new SimpleAttributeSet();
        StyleConstants.setFontFamily(attrs, "Monospaced");
        StyleConstants.setFontSize(attrs, 10);
        for (Integer len : multigene.getAllTISs()) {
            if (len.equals(multigene.getAnnTIS())) {
                StyleConstants.setForeground(attrs, Color.RED);
            } else if (multigene.getTISs().contains(len)) {
                StyleConstants.setForeground(attrs, Color.GREEN);
            } else {
                if (jToggleButtonHigh.isSelected()) {
                    if (multigene.getTISs().contains(len)) {
                    } else {
                        StyleConstants.setForeground(attrs, Color.BLUE);
                    }
                } else {
                    StyleConstants.setForeground(attrs, Color.BLACK);
                }
            }

            if (jRadioButtonDNA.isSelected()) {
                Integer pos = multigene.getLength() - len;
                Integer line_nr = (pos) / line_char_count;
                document.setCharacterAttributes(pos + line_nr, 3, attrs, false);
            } else {
                Integer pos = (multigene.getLength() - len) / 3;
                Integer line_nr = (pos) / line_char_count;
                document.setCharacterAttributes(pos + line_nr, 1, attrs, false);
            }
        }
    }

    public void showMultigene(Multigene multigene) {
        this.multigene = multigene;
        //drawMultigene();
        showMultigeneSequence();
        highlightTISs();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jButton2 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextAreaDetails = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel2 = new javax.swing.JPanel();
        jTextPane1 = new javax.swing.JTextPane();
        jLabelCaret = new javax.swing.JLabel();
        jRadioButtonDNA = new javax.swing.JRadioButton();
        jRadioButtonAA = new javax.swing.JRadioButton();
        jToggleButtonHigh = new javax.swing.JToggleButton();

        org.openide.awt.Mnemonics.setLocalizedText(jButton2, org.openide.util.NbBundle.getMessage(MultigeneZoomTopComponent.class, "MultigeneZoomTopComponent.jButton2.text")); // NOI18N
        jButton2.setToolTipText(org.openide.util.NbBundle.getMessage(MultigeneZoomTopComponent.class, "MultigeneZoomTopComponent.jButton2.toolTipText")); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jTextAreaDetails.setColumns(20);
        jTextAreaDetails.setEditable(false);
        jTextAreaDetails.setRows(3);
        jScrollPane1.setViewportView(jTextAreaDetails);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 313, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jPanel2.setLayout(new java.awt.BorderLayout());

        jTextPane1.setFont(new java.awt.Font("Monospaced", 0, 10)); // NOI18N
        jTextPane1.setToolTipText(org.openide.util.NbBundle.getMessage(MultigeneZoomTopComponent.class, "MultigeneZoomTopComponent.jTextPane1.toolTipText")); // NOI18N
        jPanel2.add(jTextPane1, java.awt.BorderLayout.CENTER);

        jScrollPane2.setViewportView(jPanel2);

        org.openide.awt.Mnemonics.setLocalizedText(jLabelCaret, org.openide.util.NbBundle.getMessage(MultigeneZoomTopComponent.class, "MultigeneZoomTopComponent.jLabelCaret.text")); // NOI18N

        buttonGroup1.add(jRadioButtonDNA);
        jRadioButtonDNA.setSelected(true);
        org.openide.awt.Mnemonics.setLocalizedText(jRadioButtonDNA, org.openide.util.NbBundle.getMessage(MultigeneZoomTopComponent.class, "MultigeneZoomTopComponent.jRadioButtonDNA.text")); // NOI18N
        jRadioButtonDNA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonDNAActionPerformed(evt);
            }
        });

        buttonGroup1.add(jRadioButtonAA);
        org.openide.awt.Mnemonics.setLocalizedText(jRadioButtonAA, org.openide.util.NbBundle.getMessage(MultigeneZoomTopComponent.class, "MultigeneZoomTopComponent.jRadioButtonAA.text")); // NOI18N
        jRadioButtonAA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonAAActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(jToggleButtonHigh, org.openide.util.NbBundle.getMessage(MultigeneZoomTopComponent.class, "MultigeneZoomTopComponent.jToggleButtonHigh.text")); // NOI18N
        jToggleButtonHigh.setToolTipText(org.openide.util.NbBundle.getMessage(MultigeneZoomTopComponent.class, "MultigeneZoomTopComponent.jToggleButtonHigh.toolTipText")); // NOI18N
        jToggleButtonHigh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButtonHighActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelCaret)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 140, Short.MAX_VALUE)
                .addComponent(jRadioButtonAA)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jRadioButtonDNA)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jToggleButtonHigh)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 97, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addContainerGap())
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 313, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton2)
                    .addComponent(jToggleButtonHigh))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelCaret)
                    .addComponent(jRadioButtonDNA)
                    .addComponent(jRadioButtonAA))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

        if (multigene != null) {
            String seq = multigene.sequence();
            if (!seq.equals("")) {
                NCBIAPIFrame frame = new NCBIAPIFrame(multigene);
                frame.pack();
                frame.setLocationRelativeTo(WindowManager.getDefault().getMainWindow());
                //frame.setAlwaysOnTop(true);
                frame.setVisible(true);
            }
        } else {
            Messenger.getDefault().showMessage("First, selecet a multigene.");
        }

    }//GEN-LAST:event_jButton2ActionPerformed

    private void jToggleButtonHighActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButtonHighActionPerformed
        if (jToggleButtonHigh.isSelected()) {
            if (multigene == null) {
                return;
            }
            highlightTISs();
        } else {
            if (multigene == null) {
                Messenger.getDefault().showMessage("First, select a multigene");
                return;
            }
            highlightTISs();
        }
    }//GEN-LAST:event_jToggleButtonHighActionPerformed

    private void jRadioButtonDNAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonDNAActionPerformed
        showMultigeneSequence();
        highlightTISs();
    }//GEN-LAST:event_jRadioButtonDNAActionPerformed

    private void jRadioButtonAAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonAAActionPerformed
        showMultigeneSequence();
        highlightTISs();
    }//GEN-LAST:event_jRadioButtonAAActionPerformed

    public void showMultigeneSequence() {

        if (multigene == null) {
            return;
        }
        Integer line_char_count = SettingsHandle.getDefault().getLineCharCount();
        jTextPane1.setText("");
        StyledDocument document = jTextPane1.getStyledDocument();
        //Document document = jEditorPane1.getDocument();
        SimpleAttributeSet attrs = new SimpleAttributeSet();
        StyleConstants.setFontFamily(attrs, "Monospaced");
        StyleConstants.setFontSize(attrs, 10);

        StyleConstants.setBackground(attrs, Color.WHITE);

        String mg_seq;
        if (jRadioButtonDNA.isSelected()) {
            mg_seq = multigene.sequence();
        } else {
            mg_seq = SequenceUtils.translate(multigene.sequence());
        }
        Integer lines_count = (mg_seq.length() - 1) / line_char_count;
        for (int i = 0; i < lines_count + 1; i++) {
            String sub_mg_seq = mg_seq.substring(i * line_char_count, Math.min(mg_seq.length(), (i + 1) * line_char_count));
            try {
                document.insertString(document.getLength(), sub_mg_seq + "\n", attrs);
            } catch (BadLocationException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
        jTextPane1.setCaretPosition(0);
        jTextPane1.setDocument(document);
    }

    private void prepareTextPane() {
        jTextPane1.addMouseListener(new MouseAdapter() {

            @Override
            public void mousePressed(MouseEvent evt) {
                showCaretPosition();
            }

            @Override
            public void mouseReleased(MouseEvent evt) {
                showCaretPosition();
            }
        });
    }

    private void showCaretPosition() {
        Integer pos = jTextPane1.getCaretPosition();
        Integer line_char_count = SettingsHandle.getDefault().getLineCharCount();
        if (pos >= 0) {
            Integer line_nr = pos / (line_char_count + 1);
            Integer col = pos % (line_char_count + 1);
            jLabelCaret.setText("Caret position: (" + line_nr.toString() + "," + col.toString() + "), " + (pos - line_nr));
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabelCaret;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JRadioButton jRadioButtonAA;
    private javax.swing.JRadioButton jRadioButtonDNA;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTextAreaDetails;
    private javax.swing.JTextPane jTextPane1;
    private javax.swing.JToggleButton jToggleButtonHigh;
    // End of variables declaration//GEN-END:variables

    /**
     * Gets default instance. Do not use directly: reserved for *.settings files only,
     * i.e. deserialization routines; otherwise you could get a non-deserialized instance.
     * To obtain the singleton instance, use {@link #findInstance}.
     */
    public static synchronized MultigeneZoomTopComponent getDefault() {
        if (instance == null) {
            instance = new MultigeneZoomTopComponent();
        }
        return instance;
    }

    /**
     * Obtain the MultigeneZoomTopComponent instance. Never call {@link #getDefault} directly!
     */
    public static synchronized MultigeneZoomTopComponent findInstance() {
        TopComponent win = WindowManager.getDefault().findTopComponent(PREFERRED_ID);
        if (win == null) {
            Logger.getLogger(MultigeneZoomTopComponent.class.getName()).warning(
                    "Cannot find " + PREFERRED_ID + " component. It will not be located properly in the window system.");
            return getDefault();
        }
        if (win instanceof MultigeneZoomTopComponent) {
            return (MultigeneZoomTopComponent) win;
        }
        Logger.getLogger(MultigeneZoomTopComponent.class.getName()).warning(
                "There seem to be multiple components with the '" + PREFERRED_ID
                + "' ID. That is a potential source of errors and unexpected behavior.");
        return getDefault();
    }

    @Override
    public int getPersistenceType() {
        return TopComponent.PERSISTENCE_ALWAYS;
    }

    @Override
    public void componentOpened() {
        // TODO add custom code on component opening
    }

    @Override
    public void componentClosed() {
        // TODO add custom code on component closing
    }

    void writeProperties(java.util.Properties p) {
        // better to version settings since initial version as advocated at
        // http://wiki.apidesign.org/wiki/PropertyFiles
        p.setProperty("version", "1.0");
        // TODO store your settings
    }

    Object readProperties(java.util.Properties p) {
        if (instance == null) {
            instance = this;
        }
        instance.readPropertiesImpl(p);
        return instance;
    }

    private void readPropertiesImpl(java.util.Properties p) {
        String version = p.getProperty("version");
        // TODO read your settings according to their version
    }

    @Override
    protected String preferredID() {
        return PREFERRED_ID;
    }
}
