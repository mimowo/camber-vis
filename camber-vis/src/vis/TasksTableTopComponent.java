/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package vis;

import java.awt.Desktop;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;
//import org.openide.util.ImageUtilities;
import org.netbeans.api.settings.ConvertAsProperties;
import structs.ExtClustalWResult;
import structs.ExtNCBIAPIResult;
import structs.ExtToolResult;
import ui.ClustalWResultFrame;
import ui.NCBIAPIResultFrame;
import utils.Messenger;
import utils.SettingsHandle;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(dtd = "-//vis//TasksTable//EN",
autostore = false)
public final class TasksTableTopComponent extends TopComponent {

    private static TasksTableTopComponent instance;
    /** path to the icon used by the component and its open action */
//    static final String ICON_PATH = "SET/PATH/TO/ICON/HERE";
    private static final String PREFERRED_ID = "TasksTableTopComponent";
    private Map<String, ExtToolResult> results = new TreeMap<String, ExtToolResult>();
    private Integer counter;

    public void addTask(ExtToolResult extToolResult) {
        counter += 1;
        results.put(extToolResult.getJobID(), extToolResult);
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        Object[] row = {counter, extToolResult.getJobID(), extToolResult.getDate(), extToolResult.getToolName(), extToolResult.getStatus()};
        model.addRow(row);
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                jTable1.updateUI();
            }
        });

    }

    public void updateStatus(ExtToolResult extToolResult) {
        //  extToolResult.setStatus(newStatus);
        String newStatus = extToolResult.getStatus();
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        Integer col_job_id_index = getColumnIndex("JobID");
        Integer col_status_index = getColumnIndex("Status");

        if (col_job_id_index != -1 && col_status_index != -1) {
            Integer row = getRowIndex(extToolResult.getJobID(), col_job_id_index);
            model.setValueAt(newStatus, row, col_status_index);
            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    jTable1.updateUI();
                }
            });
        }
    }

    public TasksTableTopComponent() {
        initComponents();
        setName(NbBundle.getMessage(TasksTableTopComponent.class, "CTL_TasksTableTopComponent"));
        setToolTipText(NbBundle.getMessage(TasksTableTopComponent.class, "HINT_TasksTableTopComponent"));
//        setIcon(ImageUtilities.loadImage(ICON_PATH, true));
        prepareTable();
        counter = 0;
    }

    public Integer getColumnIndex(String col_id) {
        Integer col = 0;
        while (!jTable1.getColumnName(col).equals(col_id)) {
            col++;
            if (col >= jTable1.getColumnCount()) {
                return -1;
            }
        }
        return col;
    }

    public Integer getRowIndex(String jobID, int col_index) {
        Integer row = 0;
        while (!jTable1.getValueAt(row, col_index).equals(jobID)) {
            row++;
            if (row >= jTable1.getRowCount()) {
                return -1;
            }
        }
        return row;
    }

    public void prepareTable() {
        jTable1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jTable1.setAutoCreateRowSorter(true);
        jTable1.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {

                if (SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 2) {
                    //} else if (SwingUtilities.isRightMouseButton(e)) {
                    Point p = e.getPoint();
                    int rowNumber = jTable1.rowAtPoint(p);
                    ListSelectionModel model = jTable1.getSelectionModel();
                    model.setSelectionInterval(rowNumber, rowNumber);
                    Integer row = jTable1.getSelectedRow();
                    Integer col = getColumnIndex("JobID");

                    String jobID = jTable1.getValueAt(row, col).toString();

                    ExtToolResult result = results.get(jobID);

                    if (result.getStatus().equals(ExtToolResult.STATUS_WORKING)) {
                        Messenger.getDefault().showMessage("The result is still not ready");
                    } else if (result.getStatus().equals(ExtToolResult.STATUS_ERROR)) {
                        Messenger.getDefault().showMessage("The task has been finished with error, check the output window for more details.");
                    } else {
                        if (result instanceof ExtClustalWResult) {
                            ClustalWResultFrame resultFrame = new ClustalWResultFrame((ExtClustalWResult) result);
                            resultFrame.pack();
                            resultFrame.setLocationRelativeTo(WindowManager.getDefault().getMainWindow());
                            resultFrame.setVisible(true);
                            resultFrame.setTitle("ClustalW task ID: " + result.getJobID());
                        } else if (result instanceof ExtNCBIAPIResult) {
                            NCBIAPIResultFrame resultFrame = new NCBIAPIResultFrame((ExtNCBIAPIResult) result);
                            resultFrame.pack();
                            resultFrame.setLocationRelativeTo(WindowManager.getDefault().getMainWindow());
                            resultFrame.setVisible(true);
                            resultFrame.setTitle("NCBI API task ID: " + result.getJobID());
                        }
                    }
                }
            }
        });
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/clean.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(jButton1, org.openide.util.NbBundle.getMessage(TasksTableTopComponent.class, "TasksTableTopComponent.jButton1.text")); // NOI18N
        jButton1.setToolTipText(org.openide.util.NbBundle.getMessage(TasksTableTopComponent.class, "TasksTableTopComponent.jButton1.toolTipText")); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/folder.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(jButton2, org.openide.util.NbBundle.getMessage(TasksTableTopComponent.class, "TasksTableTopComponent.jButton2.text")); // NOI18N
        jButton2.setToolTipText(org.openide.util.NbBundle.getMessage(TasksTableTopComponent.class, "TasksTableTopComponent.jButton2.toolTipText")); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "JobID", "Time", "Tool", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton2)
                .addContainerGap(252, Short.MAX_VALUE))
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 424, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 433, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        Integer selectedRow = jTable1.getSelectedRow();
        if (selectedRow == -1) {
        } else {
            Integer columnJobID = getColumnIndex("JobID");
            String jobID = jTable1.getValueAt(selectedRow, columnJobID).toString();
            results.remove(jobID);
            ((DefaultTableModel) jTable1.getModel()).removeRow(selectedRow);
            jTable1.updateUI();
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        try {
            String tmp_dir = SettingsHandle.getDefault().getTmpDirPath();

            Desktop.getDesktop().open(new File(tmp_dir));
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
}//GEN-LAST:event_jButton2ActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables

    /**
     * Gets default instance. Do not use directly: reserved for *.settings files only,
     * i.e. deserialization routines; otherwise you could get a non-deserialized instance.
     * To obtain the singleton instance, use {@link #findInstance}.
     */
    public static synchronized TasksTableTopComponent getDefault() {
        if (instance == null) {
            instance = new TasksTableTopComponent();
        }
        return instance;
    }

    /**
     * Obtain the TasksTableTopComponent instance. Never call {@link #getDefault} directly!
     */
    public static synchronized TasksTableTopComponent findInstance() {
        TopComponent win = WindowManager.getDefault().findTopComponent(PREFERRED_ID);
        if (win == null) {
            Logger.getLogger(TasksTableTopComponent.class.getName()).warning(
                    "Cannot find " + PREFERRED_ID + " component. It will not be located properly in the window system.");
            return getDefault();
        }
        if (win instanceof TasksTableTopComponent) {
            return (TasksTableTopComponent) win;
        }
        Logger.getLogger(TasksTableTopComponent.class.getName()).warning(
                "There seem to be multiple components with the '" + PREFERRED_ID
                + "' ID. That is a potential source of errors and unexpected behavior.");
        return getDefault();
    }

    @Override
    public int getPersistenceType() {
        return TopComponent.PERSISTENCE_ALWAYS;
    }

    @Override
    public void componentOpened() {
        // TODO add custom code on component opening
    }

    @Override
    public void componentClosed() {
        // TODO add custom code on component closing
    }

    void writeProperties(java.util.Properties p) {
        // better to version settings since initial version as advocated at
        // http://wiki.apidesign.org/wiki/PropertyFiles
        p.setProperty("version", "1.0");
        // TODO store your settings
    }

    Object readProperties(java.util.Properties p) {
        if (instance == null) {
            instance = this;
        }
        instance.readPropertiesImpl(p);
        return instance;
    }

    private void readPropertiesImpl(java.util.Properties p) {
        String version = p.getProperty("version");
        // TODO read your settings according to their version
    }

    @Override
    protected String preferredID() {
        return PREFERRED_ID;
    }
}
