/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package widgets;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import org.netbeans.api.visual.graph.GraphScene;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.api.visual.widget.Widget;
import structs.Multigene;
import structs.MultigeneCluster;
import utils.Messenger;
import utils.SettingsHandle;

public class GeneWidget extends Widget {

    private Multigene multigene;
    private Integer height;
    private Integer width;
    private Rectangle rect;
    private boolean selected = false;
    private Scene scene;
    private Color color;
    private boolean flip;
    private Font geneIDfont = new Font("Courier", Font.BOLD, 10);
    private SettingsHandle sh = SettingsHandle.getDefault();
    //private boolean anchor;

    public GeneWidget(GraphScene scene, Multigene multigene, Integer width, Integer height, boolean flip) {
        super(scene);
        this.scene = scene;
        this.multigene = multigene;
        this.height = height;
        this.width = width;
        this.rect = new Rectangle(-width / 2, -height / 2, width, height);
        this.color = calculateColor();
        this.flip = flip;
        // this.anchor = anchor;
    }

    public void setFlip(boolean flip) {
        this.flip = flip;
    }

    public void setHeight(Integer height) {
        this.height = height;
        this.rect = new Rectangle(-width / 2, -height / 2, width, height);
    }

    public void setWidth(Integer width) {
        this.width = width;
        this.rect = new Rectangle(-width / 2, -height / 2, width, height);
    }

    @Override
    protected Rectangle calculateClientArea() {
        return rect;
    }

    @Override
    protected void paintWidget() {
        Graphics2D g = getGraphics();
        g.setColor(color);
        Font defaultFont = g.getFont();
        g.setFont(geneIDfont);
        MultigeneCluster cluster = multigene.getCluster();
        if (selected == false) {

            if (cluster.isAnchor()) {
                g.fillRect(rect.x, rect.y, rect.width, rect.height);
            } else {
                g.fillOval(rect.x, rect.y, rect.width, rect.height);
            }
        } else {
            Integer tmp_height = rect.height + 4;
            if (cluster.isAnchor()) {
                g.fillRect(rect.x, -tmp_height / 2, rect.width, tmp_height);
            } else {
                g.fillOval(rect.x, -tmp_height / 2, rect.width, tmp_height);
            }

            Integer max_length = multigene.getLength();

            for (Integer length : multigene.getTISs()) {
                Integer pos;
                g.setColor(Color.BLACK);
                String gene_text = multigene.getGeneIDandName();

                if ((multigene.getIntStrand() == 1 && flip == false) || (multigene.getIntStrand() != 1 && flip == true)) {
                    g.drawString(gene_text, rect.x - gene_text.length()*2 + rect.width / 2, rect.y - 3);
                    pos = rect.width - Double.valueOf(width.doubleValue() * length.doubleValue() / max_length.doubleValue()).intValue();
                } else {
                    g.drawString(gene_text, rect.x - gene_text.length()*2 + rect.width / 2, rect.y + 13);
                    pos = Double.valueOf(width.doubleValue() * length.doubleValue() / max_length.doubleValue()).intValue();
                }
                g.setColor(color);
                Integer add_height;
                if (length == multigene.getSelTIS()) {
                    if (length == multigene.getAnnTIS()) {
                        g.setColor(Color.PINK);
                        add_height = 10;
                    } else {
                        g.setColor(Color.BLUE);
                        add_height = 10;
                    }
                } else {
                    if (length == multigene.getAnnTIS()) {
                        g.setColor(Color.RED);
                        add_height = 10;
                    } else {
                        g.setColor(Color.BLACK);
                        add_height = 5;
                    }
                }

                if ((multigene.getIntStrand() == 1 && flip == false) || (multigene.getIntStrand() != 1 && flip == true)) {
                    g.fillRect(pos + rect.x, -tmp_height / 2 - add_height, 3, tmp_height + add_height);
                } else {
                    g.fillRect(pos + rect.x, -tmp_height / 2, 3, tmp_height + add_height);
                }
            }
        }
        g.setFont(defaultFont);
    }

    public void clicked() {
        if (selected == false) {
            selected = true;
        } else {
            selected = false;
        }
        scene.repaint();
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
        scene.repaint();
    }

    public Multigene getMultigene() {
        return multigene;
    }

    private Color calculateColor() {
        
        if (multigene.isRemoved()) {
            return Color.BLACK;
        }
        
        if (sh.getSchemaColor().equals(SettingsHandle.SCHEMA_COLOR_CLUSTERS)) {
            return multigene.getClusterColor();
        } else if (sh.getSchemaColor().equals(SettingsHandle.SCHEMA_COLOR_CONS)) {
            return multigene.getConservationColor();
        } else if (sh.getSchemaColor().equals(SettingsHandle.SCHEMA_COLOR_TYPES)) {
            return multigene.getTypeColor();
        } else {
            return multigene.getConservationColor();
        }
    }
}
