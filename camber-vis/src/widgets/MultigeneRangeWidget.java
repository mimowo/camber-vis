/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */
package widgets;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import org.netbeans.api.visual.graph.GraphScene;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.api.visual.widget.Widget;

public class MultigeneRangeWidget extends Widget {

    private Rectangle rect;
    private Scene scene;
//    private Integer width;
//    private Integer height;

    public MultigeneRangeWidget(GraphScene scene, Integer m, Integer strand, Integer height, boolean flip) {
        super(scene);
        this.scene = scene;
        if ((strand == 1 && flip == false) || (strand != 1 && flip == true)) {
            this.rect = new Rectangle(m - 1, 3, 4, 4);
        } else {
            this.rect = new Rectangle(m - 1, height - 7, 4, 4);
        }
    }

    @Override
    protected Rectangle calculateClientArea() {
        return rect;
    }

    @Override
    protected void paintWidget() {
        Graphics2D g = getGraphics();
        Color color = Color.BLUE;
        g.setColor(color);
        g.fillOval(rect.x, rect.y, rect.width, rect.height);
    }
}
