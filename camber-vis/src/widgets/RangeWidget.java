/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */

package widgets;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import org.netbeans.api.visual.graph.GraphScene;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.api.visual.widget.Widget;

public class RangeWidget extends Widget {

    private Rectangle rect;
    private Scene scene;
    private Integer width;
    private Integer height;

    public RangeWidget(GraphScene scene, Integer ml, Integer mr, Integer height, Integer max_width) {
        super(scene);
        this.scene = scene;
        ml = Math.max(0, ml);
        mr = Math.max(0, mr);
        ml = Math.min(max_width, ml);
        mr = Math.min(max_width, mr);
        this.rect = new Rectangle(ml, 2, mr - ml, height - 7);
    }

    @Override
    protected Rectangle calculateClientArea() {
        return rect;
    }

    @Override
    protected void paintWidget() {
        Graphics2D g = getGraphics();
        Color color = Color.RED;
        g.setColor(color);
        g.drawRect(rect.x, rect.y, rect.width, rect.height);
    }
}
