/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */

package widgets;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.PriorityQueue;
import org.netbeans.api.visual.graph.GraphScene;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.api.visual.widget.Widget;

public class RulerWidget extends Widget {

    private PriorityQueue<Integer> positions;
    //private Integer height;
    //private Integer width;
    private Rectangle rect;
    private boolean selected = false;
    private Scene scene;
    private Color color;
    private Integer range_left;
    private Integer range_right;
    private Integer window_right;
    private Integer y_pos;

    public RulerWidget(GraphScene scene, PriorityQueue<Integer> positions, Integer range_left, Integer range_right, Integer window_right) {
        super(scene);
        this.scene = scene;
        this.positions = positions;
        this.range_left = range_left;
        this.range_right = range_right;
        this.window_right = window_right;
        this.y_pos = 65;
        this.rect = new Rectangle(0, y_pos, window_right, 5);
    }

    @Override
    protected Rectangle calculateClientArea() {
        return rect;
    }

    @Override
    protected void paintWidget() {
        Graphics2D g = getGraphics();
        g.setColor(Color.BLACK);
        g.drawLine(0, y_pos, window_right, y_pos);
        
        Double a = Double.valueOf(window_right.doubleValue() / (range_right.doubleValue() - range_left.doubleValue()));
        Double b = -a * Double.valueOf(range_left.doubleValue());
        Integer prev_pos = null;
        Integer prev_pos_sc = null;
        for (Integer pos:positions) {
            Integer pos_sc = Double.valueOf(a * pos.doubleValue() + b).intValue();
            g.drawLine(pos_sc, y_pos-5, pos_sc, y_pos);
            if (prev_pos != null) {
                Integer dist = pos - prev_pos - 1;
                Integer str_x = (pos_sc + prev_pos_sc)/2 - 3;
                Integer str_y = y_pos - 5;
                g.drawString(dist.toString(), str_x , str_y);
            }
            prev_pos_sc = pos_sc;
            prev_pos = pos;
        }
    }
}
