/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */

package widgets;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.PriorityQueue;
import org.netbeans.api.visual.graph.GraphScene;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.api.visual.widget.Widget;

public class SplitLineWidget extends Widget {

    private Rectangle rect;
    private Scene scene;
    private Integer window_right;
    private Integer y_pos;

    public SplitLineWidget(GraphScene scene, Integer window_right, Integer y_pos) {
        super(scene);
        this.scene = scene;
        this.window_right = window_right;
        this.y_pos = y_pos;
        this.rect = new Rectangle(0, y_pos, window_right, 1);
    }

    @Override
    protected Rectangle calculateClientArea() {
        return rect;
    }

    @Override
    protected void paintWidget() {
        this.revalidate();
        Graphics2D g = getGraphics();
        g.setColor(Color.BLACK);
        g.drawLine(0, y_pos, window_right, y_pos);

    }
}
