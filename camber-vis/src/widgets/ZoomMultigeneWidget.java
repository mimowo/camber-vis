/* ===========================================================
 * CAMBerVis : genome annotations visualization tool to support comparative
 * analysis of multiple bacterial strains
 * ===========================================================
 *
 *
 * Project Info:  http://bioputer.mimuw.edu.pl/ecamber/
 * Sources: http://code.google.com/p/camber2/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * CAMBerVis  Copyright (C) 2010-2014
 * Code author:  Michal Wozniak (m.wozniak@mimuw.edu.pl)
 */

package widgets;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import org.netbeans.api.visual.graph.GraphScene;
import org.netbeans.api.visual.widget.Widget;
import structs.Multigene;
import utils.VisSettingsHandle;

public class ZoomMultigeneWidget extends Widget {

    private Multigene multigene;
    private Integer height;
    private Integer width;
    private Rectangle rect;

    public ZoomMultigeneWidget(GraphScene scene, Multigene multigene, Integer width, Integer height) {
        super(scene);
        this.multigene = multigene;
        this.height = height;
        this.width = width;
        this.rect = new Rectangle(0, -height / 2, width, height);
    }

    @Override
    protected Rectangle calculateClientArea() {
        return rect;
    }

    @Override
    protected void paintWidget() {
        Graphics2D g = getGraphics();
        Color color = VisSettingsHandle.getDefault().checkColor(multigene.getCluster().getCluster_id());
        g.setColor(color);
        g.drawRect(rect.x, rect.y, rect.width, rect.height);
        Integer max_length = multigene.getLength();

        for (Integer length : multigene.getTISs()) {
            Integer pos = Double.valueOf(width.doubleValue() * length.doubleValue() / max_length.doubleValue()).intValue();
            if (length == multigene.getAnnTIS()) {
                g.setColor(Color.RED);
            } else {
                g.setColor(Color.GREEN);
            }
            g.fillRect(pos, rect.y, 3, rect.height);
        }
    }
}
